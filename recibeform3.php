﻿<?php

//header("Location: index.php?form=1");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="es"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="es"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="es"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="es"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<title>audioactive</title>
            
		<meta name="description" content="Shop powered by PrestaShop" />
		<meta name="keywords" content="tienda, prestashop" />
		<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
		<meta http-equiv="content-language" content="es" />
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="index,follow" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="/audioactive/htdocs/img/favicon.ico?1404314658" />
		<link rel="shortcut icon" type="image/x-icon" href="/audioactive/htdocs/img/favicon.ico?1404314658" />
		<script type="text/javascript">
			var baseDir = 'http://adoraserver/audioactive/htdocs/';
			var baseUri = 'http://adoraserver/audioactive/htdocs/';
			var static_token = 'd5e43d507d9ed951dfd78a839dcf3886';
			var token = 'cfef596f98ac6c8add677d4742f260e5';
			var priceDisplayPrecision = 2;
			var priceDisplayMethod = 1;
			var roundMode = 2;
		</script>
                <link rel="stylesheet" type="text/css" href="/audioactive/htdocs/themes/leodig/css/bootstrap.css"/> 
		<link href="/audioactive/htdocs/themes/leodig/css/grid_prestashop.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/global.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/carriercompare/style.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/modules/blockviewed/blockviewed.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/blockcart/blockcart.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/blockcurrencies/blockcurrencies.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/blockuserinfo/blockuserinfo.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/blocklanguages/blocklanguages.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/modules/blocktags/blocktags.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/js/jquery/plugins/autocomplete/jquery.autocomplete.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/product_list.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/blocksearch/blocksearch.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/blocknewsletter/blocknewsletter.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/leocoinslider/assets/style.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/leomanagewidgets/assets/style.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/leobootstrapmenu/themes/default/assets/styles.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/leomanufacturerscroll/assets/style.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/blockleorelatedproducts/blockleorelatedproducts.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/blockwishlist/blockwishlist.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/productcomments/productcomments.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/modules/sendtoafriend/sendtoafriend.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/blockadvfooter/blockadvfooter.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/blockbestsellers/blockbestsellers.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/blockspecials/blockspecials.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/js/jquery/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/audioactive/htdocs/themes/leodig/css/modules/leocustomajax/leocustomajax.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" href="http://adoraserver/audioactive/htdocs/themes/leodig/css/theme-responsive.css"/>


		<script type="text/javascript" src="/audioactive/htdocs/js/jquery/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/js/jquery/jquery-migrate-1.2.1.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/js/jquery/plugins/jquery.easing.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/js/tools.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/modules/carriercompare/carriercompare.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/themes/leodig/js/modules/blockcart/ajax-cart.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/themes/leodig/js/tools/treeManagement.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/js/jquery/plugins/autocomplete/jquery.autocomplete.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/modules/leotempcp/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/js/jquery/plugins/jquery.cooki-plugin.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/modules/leocoinslider/js/script.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/modules/leomanufacturerscroll/assets/jquery.flexisel.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/modules/blockwishlist/js/ajax-wishlist.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/js/jquery/plugins/fancybox/jquery.fancybox.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/modules/leocustomajax/leocustomajax.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/js/jquery/plugins/jquery.scrollTo.js"></script>
		<script type="text/javascript" src="/audioactive/htdocs/js/jquery/plugins/jquery.serialScroll.js"></script>
	
<link rel="stylesheet" type="text/css" href="http://adoraserver/audioactive/htdocs/themes/leodig/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="http://adoraserver/audioactive/htdocs/themes/leodig/css/fonts.css"/>
<script type="text/javascript" src="http://adoraserver/audioactive/htdocs/themes/leodig/js/custom.js"></script>
<script type="text/javascript" src="http://adoraserver/audioactive/htdocs/themes/leodig/js/jquery.cookie.js"></script>


<!--[if lt IE 9]>
<script src="http://adoraserver/audioactive/htdocs/themes/leodig/js/html5.js"></script>
<script src="http://adoraserver/audioactive/htdocs/themes/leodig/js/respond.min.js"></script>
<![endif]-->
		
	</head>

	<body id="index" class=" fs14 index keep-header">
	<p style="display: block;" id="back-top"> <a href="#top"><span></span></a> </p>
					<section id="page" class="clearfix">
			
			<!-- Header -->
			<header id="header" class="navbar-fixed-top clearfix">
				<section id="topbar">
					<div class="container">
						
<!-- Block user information module HEADER -->
<ul id="header_user_info" class="links hidden-xs hidden-sm pull-left">
	<!--<li>	
		Bienvenido a
			<a href="http://adoraserver/audioactive/htdocs/" title="audioactive" class="login" rel="nofollow">audioactive</a>
		tienda online
	</li>-->
</ul>
			<ul id="header_nav_cart" class="links pull-right">
			<li id="shopping_cart">
				<a  href="http://adoraserver/audioactive/htdocs/carrito" title="Ver mi carrito de compra" rel="nofollow">
					<em class="icon-quitar-carrito"></em>				
					<span class="ajax_cart_quantity hidden">0</span>
					<span class="ajax_cart_product_txt hidden">Artículo</span>
					<span class="ajax_cart_product_txt_s hidden">Artículos</span>
					<span class="ajax_cart_total hidden">
											</span>
					<span class="ajax_cart_no_product">
							0 Artículo(s) - 
							0,00 €
					</span>
				</a>
			</li>
		</ul>
	<div id="header_user" class="pull-right">
	<a data-toggle="dropdown" class="groupe-btn dropdown hidden-sm hidden-md hidden-lg" title="Enlace rápido" href="#"><span class="fa fa-user"></span></a>
	<ul id="header_nav" class="links groupe-content">
		<li>Bienvenido,</li>
					<li><a href="http://adoraserver/audioactive/htdocs/mi-cuenta" title="Conectarse a su cuenta de cliente" class="login" rel="nofollow"><span class="fa fa-lock"></span>Entrar</a></li>					
			<li><a href="http://adoraserver/audioactive/htdocs/mi-cuenta" title="Conectarse a su cuenta de cliente" class="login" rel="nofollow"><span class="fa fa-user"></span>Registrar</a></li>			
				<!--<li ><a href="http://adoraserver/audioactive/htdocs/module/blockwishlist/mywishlist" title="Mi lista de deseos"><span class="fa fa-heart"></span>Mi lista de deseos</a></li>-->
	</ul>
			
</div>

<!-- /Block user information module HEADER -->
<!-- Block languages module -->
<!-- /Block languages module -->
<script type="text/javascript">
var CUSTOMIZE_TEXTFIELD = 1;
var img_dir = 'http://adoraserver/audioactive/htdocs/themes/leodig/img/';
var customizationIdMessage = 'Personalización n°';
var removingLinkText = 'eliminar este producto de mi carrito';
var freeShippingTranslation = 'Envío gratuito!';
var freeProductTranslation = '¡Gratis!';
var delete_txt = 'Eliminar';
var generated_date = 1404370462;
</script>
<!-- MODULE Block cart -->
<div id="cart_block" class="block exclusive">
	<p class="title_block">
		<a href="http://adoraserver/audioactive/htdocs/carrito" title="Ver mi carrito de compra" rel="nofollow">carrito
				<span id="block_cart_expand" class="hidden">&nbsp;</span>
		<span id="block_cart_collapse" >&nbsp;</span>
		</a>
	</p>
	<div class="block_content">
	<!-- block summary -->
	<div id="cart_block_summary" class="collapsed">
		<span class="ajax_cart_quantity" style="display:none;">0</span>
		<span class="ajax_cart_product_txt_s" style="display:none">productos</span>
		<span class="ajax_cart_product_txt" >producto</span>
		<span class="ajax_cart_total" style="display:none">
					</span>
		<span class="ajax_cart_no_product" >vacío</span>
	</div>
	<!-- block list of products -->
	<div id="cart_block_list" class="expanded">
			<p class="cart_block_no_products" id="cart_block_no_products">No hay productos</p>
		<table id="vouchers" style="display:none;">
			<tbody>
								<tr class="bloc_cart_voucher">
						<td>&nbsp;</td>
					</tr>
						</tbody>
		</table>
		<p id="cart-prices">
			<span id="cart_block_shipping_cost" class="price ajax_cart_shipping_cost">0,00 €</span>
			<span>Transporte</span>
			<br/>
									<span id="cart_block_total" class="price ajax_block_cart_total">0,00 €</span>
			<span>Total</span>
		</p>
				<p id="cart-buttons">
			<a href="http://adoraserver/audioactive/htdocs/carrito" class="button_small" title="Ver mi carrito de compra" rel="nofollow">carrito</a>			<a href="http://adoraserver/audioactive/htdocs/carrito" id="button_order_cart" class="button exclusive" title="Confirmar" rel="nofollow"><span></span>Confirmar</a>
		</p>
	</div>
	</div>
</div>
<!-- /MODULE Block cart --><!-- block seach mobile -->
<!-- Block search module TOP -->
<div id="search_block_top" class="col-lg-2 col-xs-5 col-sm-3 col-md-2 pull-right">
	<a  class="groupe-btn dropdown hidden-md hidden-lg" title="Search" href="#"><span class="fa fa-search"></span></a>
	<form method="get" action="http://adoraserver/audioactive/htdocs/buscar" id="searchbox" class="row groupe">
		<input type="hidden" name="controller" value="search" />
		<input type="hidden" name="orderby" value="position" />
		<input type="hidden" name="orderway" value="desc" />	
		<input class="search_query form-control" type="text" id="search_query_top" name="search_query" value="" placeholder="Buscar" />
		<input type="submit" name="submit_search" value="Buscar" class="button" />
	</form>
</div>
	<script type="text/javascript">
	// <![CDATA[
		$('document').ready( function() {
			$("#search_query_top")
				.autocomplete(
					'http://adoraserver/audioactive/htdocs/buscar', {
						minChars: 3,
						max: 10,
						width: 500,
						selectFirst: false,
						scroll: false,
						dataType: "json",
						formatItem: function(data, i, max, value, term) {
							return value;
						},
						parse: function(data) {
							var mytab = new Array();
							for (var i = 0; i < data.length; i++)
								mytab[mytab.length] = { data: data[i], value: data[i].cname + ' > ' + data[i].pname };
							return mytab;
						},
						extraParams: {
							ajaxSearch: 1,
							id_lang: 1
						}
					}
				)
				.result(function(event, data, formatted) {
					$('#search_query_top').val(data.pname);
					document.location.href = data.product_link;
				})
		});
	// ]]>
	</script>

<!-- /Block search module TOP -->
	
					</div>
				</section>
				<section id="header-main">
					<div class="container" >
						<div class="header-wrap">
							<div class="pull-left">
								<a id="header_logo" href="http://adoraserver/audioactive/htdocs/" title="audioactive">
									<img class="logo" src="http://adoraserver/audioactive/htdocs/img/logo.jpg?1404314658" alt="audioactive" />
								</a>
							</div>
																						<nav id="topnavigation" class="clearfix">
									<div class="container">
										<div class="row">
											 <div class="navbar-inverse">
	<nav id="cavas_menu" class="navbar-default  pull-right" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Navegación Toggle</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div id="leo-top-menu" class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav megamenu"><li>    <a href="http://adoraserver/audioactive/htdocs/" target=_self><span class="menu-title">inicio</span></a></li><li>    <a href="http://adoraserver/audioactive/htdocs/1_apple-computer-inc" target=_self><span class="menu-title">Nosotros</span></a></li><li class="parent dropdown fullwidth">
					<a class="dropdown-toggle" data-toggle="dropdown" href="http://adoraserver/audioactive/htdocs/2-home"><span class="menu-title">Productos</span><b class="caret"></b></a><div class="dropdown-menu menu-content mega-cols cols4"><div class="row"><div class="mega-col col-md-3 col-1"><ul><li class="parent dropdown-submenu mega-group "><a class="dropdown-toggle" target=_self data-toggle="dropdown" href="http://adoraserver/audioactive/htdocs/index.php?controller=#"><span class="menu-title">Aud&iacute;fonos</span></a><b class="caret"></b><ul class="dropdown-mega level3"><li class=" mega-group " ><div class="dropdown-mega"><div class="menu-content"><p><img class="img-responsive" src="/audioactive/htdocs/modules/leobootstrapmenu/image/audifonos-menu.jpg" alt="" /></p></div></div></li></ul></li></ul></div><div class="mega-col col-md-3 col-2"><ul><li class="parent dropdown-submenu mega-group "><a class="dropdown-toggle" target=_self data-toggle="dropdown" href="http://adoraserver/audioactive/htdocs/index.php?controller=#"><span class="menu-title">Tel&eacute;fonos</span></a><b class="caret"></b><ul class="dropdown-mega level3"><li class=" mega-group " ><div class="dropdown-mega"><div class="menu-content"><p><img class="img-responsive" src="/audioactive/htdocs/modules/leobootstrapmenu/image/telefonos-menu.jpg" alt="" /></p></div></div></li></ul></li></ul></div><div class="mega-col col-md-3 col-3"><ul><li class="parent dropdown-submenu mega-group "><a class="dropdown-toggle" target=_self data-toggle="dropdown" href="http://adoraserver/audioactive/htdocs/index.php?controller=#"><span class="menu-title">Auriculares Televisi&oacute;n</span></a><b class="caret"></b><ul class="dropdown-mega level3"><li class=" mega-group " ><div class="dropdown-mega"><div class="menu-content"><p><img class="img-responsive" src="/audioactive/htdocs/modules/leobootstrapmenu/image/auriculares-tv-menu.jpg" alt="Auriculares para televisión" width="250" height="160" /></p></div></div></li></ul></li></ul></div><div class="mega-col col-md-3 col-4"><ul><li class=" mega-group other-menu" ><a href="http://adoraserver/audioactive/htdocs/index.php?controller=#" target=_self><span class="menu-title">Otros</span></a><div class="dropdown-mega"><div class="menu-content"><ul class="iconos-menu">
<li><a><em class="icon-clock-streamline-time"></em>Despertadores</a></li>
<li><a><em class="icon-sonido-linea"></em>Amplificadores timbres</a></li>
<li><a><em class="icon-add-lista"></em>Pilas audífonos</a></li>
<li><a><em class="icon-first-aid-medecine-shield-streamline"></em>Limpieza audífonos</a></li>
</ul></div></div></li></ul></div></div></div></li><li>    <a href="http://adoraserver/audioactive/htdocs/contactenos" target=_self><span class="menu-title">Contacto</span></a></li><li>    <a href="http://adoraserver/audioactive/htdocs/index.php?controller=#" target=_self><span class="menu-title">965 550 550</span></a></li></ul>
		</div>
	</nav>
</div>
<script type="text/javascript">
// <![CDATA[
(function($) {
    $.fn.OffCavasmenu = function(opts) {
        // default configuration
        var config = $.extend({}, {
            opt1: null,
            text_warning_select: "Por favor seleccione uno de eliminar?",
            text_confirm_remove: "¿Está seguro de eliminar la fila de pie de página?",
            JSON: null
        }, opts);
        // main function


        // initialize every element
        this.each(function() {
            var $btn = $('#cavas_menu .navbar-toggle');
            var $nav = null;


            if (!$btn.length)
                return;
            var $nav = $('<section id="off-canvas-nav"><nav class="offcanvas-mainnav" ><div id="off-canvas-button"><span class="off-canvas-nav"></span>Cerca</div></nav></sections>');
            var $menucontent = $($btn.data('target')).find('.megamenu').clone();
            $("body").append($nav);
            $("#off-canvas-nav .offcanvas-mainnav").append($menucontent);


            $("html").addClass ("off-canvas");
            $("#off-canvas-button").click( function(){
                    $btn.click();	
            } );
            $btn.toggle(function() {
                $("body").removeClass("off-canvas-inactive").addClass("off-canvas-active");
            }, function() {
                $("body").removeClass("off-canvas-active").addClass("off-canvas-inactive");

            });

        });
        return this;
    }

})(jQuery);
$(document).ready(function() {
    jQuery("#cavas_menu").OffCavasmenu();
    $('#cavas_menu .navbar-toggle').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 0);
        return false;
    });



    var currentURL = window.location;
    currentURL = String(currentURL);
    currentURL = currentURL.replace("https://","").replace("http://","").replace("www.","").replace( /#\w*/, "" );
    baseURL = baseUri.replace("https://","").replace("http://","").replace("www.","");
    isHomeMenu = 0;
    if($("body").attr("id")=="index") isHomeMenu = 1;
    $(".megamenu > li > a").each(function() {
        menuURL = $(this).attr("href").replace("https://","").replace("http://","").replace("www.","").replace( /#\w*/, "" );
        if( isHomeMenu && (baseURL == menuURL || baseURL == menuURL.substring(0,menuURL.length-3) || baseURL.replace("index.php","")==menuURL)){
            $(this).parent().addClass("active");
            return false;
        }
        if(currentURL == menuURL){
            $(this).parent().addClass("active");
            return false;
        }
    });
});
// ]]>
</script>
										</div>
									</div>
								</nav>
													</div>
					</div>
				</section>	
			</header>
			
						<section id="slideshow" class="clearfix hidden-xs hidden-sm">
				<div class="container">
					<div class="row">
						 <div class="col-md-9">
	<div class="leo-coinslider-basic block" style="width: 100%">
		<div class="leo-wrapper">
			<!-- MAIN CONTENT -->
			<div id="leo-coinslider" class="nivoSlider clearfix" style="height:417px;width:873px;position:relative">                                       
				                
					<a href="http://www.prestashop.com" target="_blank|" style="height:417px;width:873px;display:block">             
						<img src="http://adoraserver/audioactive/htdocs/cache/cachefs/leocoinslider/873_417_ba5a7b4092993a5e083cf4c7e3d49852.jpg" title="#leo-container-1"/>                    
					</a>            
				                
					<a href="http://www.prestashop.com" target="_blank|" style="height:417px;width:873px;display:block">             
						<img src="http://adoraserver/audioactive/htdocs/cache/cachefs/leocoinslider/873_417_c7e6545d92e384e2922aba2c0c8321f7.jpg" title="#leo-container-2"/>                    
					</a>            
				                
					<a href="#" target="_blank|" style="height:417px;width:873px;display:block">             
						<img src="http://adoraserver/audioactive/htdocs/cache/cachefs/leocoinslider/873_417_26cbf4a03297ae6ae11b0af10df5c5cc.jpg" title="#leo-container-3"/>                    
					</a>            
				                
					<a href="#" target="_blank|" style="height:417px;width:873px;display:block">             
						<img src="http://adoraserver/audioactive/htdocs/cache/cachefs/leocoinslider/873_417_8ae05485978013737e57249926ecc53a.jpg" title="#leo-container-4"/>                    
					</a>            
								
			</div>
							<div id="leo-container-1" style="display: none;">
															<!-- <a class="leo-readmore" href="http://www.prestashop.com">Read more</a> -->
				</div>
							<div id="leo-container-2" style="display: none;">
															<!-- <a class="leo-readmore" href="http://www.prestashop.com">Read more</a> -->
				</div>
							<div id="leo-container-3" style="display: none;">
															<!-- <a class="leo-readmore" href="#">Read more</a> -->
				</div>
							<div id="leo-container-4" style="display: none;">
															<!-- <a class="leo-readmore" href="#">Read more</a> -->
				</div>
			    
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready( function(){	
	var currentLink = "";
	var nivoLink = "";
	$('#leo-coinslider').nivoSlider({
		effect:'fade',
		animSpeed:700,
		pauseTime:5000,
		slices:3,
		startSlide:0,
		//directionNavHide:1,
		//enableCaption :'1',		
		//captionOpacity:'0.8',
		//captionBackground:'#C01F25',
		//group:'product',
		directionNav:true,
                controlNav:true,
		manualAdvance:false,
                pauseOnHover:true,
		boxCols: 8, // For box animations
                boxRows: 4, // For box animations
		afterChange:function(){
			nivoLink = $( this ).find(".nivo-caption .lof-title").find("a");
			if(nivoLink.attr("rel")){
				currentLink = nivoLink.attr("rel");
			}
		},
                afterLoad:function(){
                                        
                    $(".lof-title").attr("class","lof-title");
                    $(".lof-title").addClass("stretchRight");

                    $(".lof-info").attr("class","lof-info");
                    $(".lof-info").addClass("stretchRight");

                    $(".lof-readmore").attr("class","lof-readmore");
                    $(".lof-readmore").addClass("stretchRight");
                }
	});
});
</script><div id="homecontent-displaySlideshow" class="col-md-3">
						<div class="leo-customclearfix">
								<div>
<h3>Plantéenos sus dudas</h3>
<form id="contactoportada" action="recibeform.php" method="POST" name="contactoportada"><label>Nombre y apellidos</label> <input type="text" name="nombre" /> <br /><label>Email</label> <input type="email" name="email" /><br /> <label>Consulta</label> <textarea></textarea><br /> <input type="submit" value="Enviar" /></form></div>
			</div>
			</div>
<script>
$(document).ready(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            pause: true,
            interval: false
        });
    });
	$(".blockleoproducttabs").each( function(){
		$(".nav-tabs li", this).first().addClass("active");
		$(".tab-content .tab-pane", this).first().addClass("active");
	} );
});
</script>
					</div>
				</div>
			</section>
									<section id="promotetop" class="clearfix">
				<div class="container">
					<div class="row">
						 <div id="homecontent-displayPromoteTop">
						<div class="leo-custom block no-blockshadown hidden-sm hidden-xs clearfix">
								<div class="media-list">
<div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
<div class="media line-icons"><a class="pull-left" title="" href="#"><img class="media-object" src="/audioactive/htdocs/modules/leomanagewidgets/data/icon-free.png" alt="" /> </a>
<div class="media-body">
<h4 class="media-heading"><a title="" href="#">Envío Gratuito</a></h4>
<p>En pedidos superiores a €.</p>
</div>
</div>
</div>
<div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
<div class="media line-icons"><a class="pull-left" title="" href="#"><img class="media-object" src="/audioactive/htdocs/modules/leomanagewidgets/data/icon-money.png" alt="" /> </a>
<div class="media-body">
<h4 class="media-heading"><a title="" href="#">Devoluciones</a></h4>
<p>Hasta 30 días</p>
</div>
</div>
</div>
<div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
<div class="media line-icons">
<div class="media-body"><a class="pull-left" title="" href="#"><img class="media-object" src="/audioactive/htdocs/modules/leomanagewidgets/data/icon-discounts.png" alt="" /> </a>
<h4 class="media-heading"><a title="" href="#">Descuentos</a></h4>
<p>Házte cliente</p>
</div>
</div>
</div>
<div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
<div class="media line-icons"><a class="pull-left" title="" href="#"><img class="media-object" src="/audioactive/htdocs/modules/leomanagewidgets/data/icon-support.png" alt="" /> </a>
<div class="media-body">
<h4 class="media-heading"><a title="" href="#">Atención al cliente</a></h4>
<p>Soporte especializado</p>
</div>
</div>
</div>
</div>
			</div>
								<div class="leo-tab block products_block exclusive blockleoproducttabs clearfix">
								<div class="block_content">	
					<ul id="productTabs-13" class="nav nav-tabs">
																		<li><a href="#leotab-13-category_6" data-toggle="tab"><img src="/audioactive/htdocs/modules/leomanagewidgets/img/icons/icon-iphone.png" alt=""/>Audífonos</a></li>
																								<li><a href="#leotab-13-category_8" data-toggle="tab"><img src="/audioactive/htdocs/modules/leomanagewidgets/img/icons/icon-ipad.png" alt=""/>Auriculares TV</a></li>
																								<li><a href="#leotab-13-category_7" data-toggle="tab"><img src="/audioactive/htdocs/modules/leomanagewidgets/img/icons/icon-ipod.png" alt=""/>Teléfonos</a></li>
																</ul>
				</div>
				<div id="productTabsContent-13" class="tab-content">
																		<div class="tab-pane" id="leotab-13-category_6">
																<div class=" carousel slide" id="leotab-13-category_6carousel">
	 	
	<a class="carousel-control left" href="#leotab-13-category_6carousel" data-slide="prev">&lsaquo;</a>
	<a class="carousel-control right" href="#leotab-13-category_6carousel" data-slide="next">&rsaquo;</a>
		<div class="carousel-inner">
			
		<div class="item active">
											 	 <div class="row  products-item clearfix">
																<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/12-audifono-modelo-1.html" class="product_image" title="Aud&iacute;fono modelo 1">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/32-home_default/audifono-modelo-1.jpg" alt="Aud&iacute;fono modelo 1" />
								<span class="product-additional" rel="12"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/12-audifono-modelo-1.html" title="Aud&iacute;fono modelo 1">Aud&iacute;fono modelo 1</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">120,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-12" href="#" rel="12" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button12" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '12', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/12-audifono-modelo-1.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
								
																			<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/13-audifono-modelo-2.html" class="product_image" title="Aud&iacute;fono modelo 2">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/33-home_default/audifono-modelo-2.jpg" alt="Aud&iacute;fono modelo 2" />
								<span class="product-additional" rel="13"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/13-audifono-modelo-2.html" title="Aud&iacute;fono modelo 2">Aud&iacute;fono modelo 2</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">210,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-13" href="#" rel="13" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button13" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '13', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/13-audifono-modelo-2.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
								
																			<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/14-audifono-modelo-3.html" class="product_image" title="Aud&iacute;fono modelo 3">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/34-home_default/audifono-modelo-3.jpg" alt="Aud&iacute;fono modelo 3" />
								<span class="product-additional" rel="14"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/14-audifono-modelo-3.html" title="Aud&iacute;fono modelo 3">Aud&iacute;fono modelo 3</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">110,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-14" href="#" rel="14" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button14" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '14', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/14-audifono-modelo-3.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
									</div>
								
					</div>		
			
		<div class="item ">
											 	 <div class="row  products-item clearfix">
																<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/15-audifono-modelo-4.html" class="product_image" title="Aud&iacute;fono modelo 4">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/35-home_default/audifono-modelo-4.jpg" alt="Aud&iacute;fono modelo 4" />
								<span class="product-additional" rel="15"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/15-audifono-modelo-4.html" title="Aud&iacute;fono modelo 4">Aud&iacute;fono modelo 4</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">89,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-15" href="#" rel="15" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button15" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '15', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/15-audifono-modelo-4.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
									</div>
								
					</div>		
		</div>
</div>

							</div>
																								<div class="tab-pane" id="leotab-13-category_8">
																<div class=" carousel slide" id="leotab-13-category_8carousel">
	 	
	<a class="carousel-control left" href="#leotab-13-category_8carousel" data-slide="prev">&lsaquo;</a>
	<a class="carousel-control right" href="#leotab-13-category_8carousel" data-slide="next">&rsaquo;</a>
		<div class="carousel-inner">
			
		<div class="item active">
											 	 <div class="row  products-item clearfix">
																<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/16-auriculares-tv-modelo-1.html" class="product_image" title="Auriculares TV modelo 1">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/36-home_default/auriculares-tv-modelo-1.jpg" alt="Auriculares TV modelo 1" />
								<span class="product-additional" rel="16"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/16-auriculares-tv-modelo-1.html" title="Auriculares TV modelo 1">Auriculares TV modelo 1</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">48,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-16" href="#" rel="16" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button16" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '16', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/16-auriculares-tv-modelo-1.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
								
																			<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/17-auriculares-tv-modelo-2.html" class="product_image" title="Auriculares TV modelo 2">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/37-home_default/auriculares-tv-modelo-2.jpg" alt="Auriculares TV modelo 2" />
								<span class="product-additional" rel="17"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/17-auriculares-tv-modelo-2.html" title="Auriculares TV modelo 2">Auriculares TV modelo 2</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">62,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-17" href="#" rel="17" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button17" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '17', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/17-auriculares-tv-modelo-2.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
								
																			<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/18-auriculares-tv-modelo-3.html" class="product_image" title="Auriculares TV modelo 3">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/38-home_default/auriculares-tv-modelo-3.jpg" alt="Auriculares TV modelo 3" />
								<span class="product-additional" rel="18"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/18-auriculares-tv-modelo-3.html" title="Auriculares TV modelo 3">Auriculares TV modelo 3</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">71,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-18" href="#" rel="18" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button18" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '18', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/18-auriculares-tv-modelo-3.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
									</div>
								
					</div>		
			
		<div class="item ">
											 	 <div class="row  products-item clearfix">
																<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/19-auriculares-tv-modelo-4.html" class="product_image" title="Auriculares TV modelo 4">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/39-home_default/auriculares-tv-modelo-4.jpg" alt="Auriculares TV modelo 4" />
								<span class="product-additional" rel="19"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/19-auriculares-tv-modelo-4.html" title="Auriculares TV modelo 4">Auriculares TV modelo 4</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">44,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-19" href="#" rel="19" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button19" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '19', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/19-auriculares-tv-modelo-4.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
									</div>
								
					</div>		
		</div>
</div>

							</div>
																								<div class="tab-pane" id="leotab-13-category_7">
																<div class=" carousel slide" id="leotab-13-category_7carousel">
	 	
	<a class="carousel-control left" href="#leotab-13-category_7carousel" data-slide="prev">&lsaquo;</a>
	<a class="carousel-control right" href="#leotab-13-category_7carousel" data-slide="next">&rsaquo;</a>
		<div class="carousel-inner">
			
		<div class="item active">
											 	 <div class="row  products-item clearfix">
																<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/8-telefono-modelo-1.html" class="product_image" title="Tel&eacute;fono Modelo 1">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/27-home_default/telefono-modelo-1.jpg" alt="Tel&eacute;fono Modelo 1" />
								<span class="product-additional" rel="8"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/8-telefono-modelo-1.html" title="Tel&eacute;fono Modelo 1">Tel&eacute;fono Modelo 1</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">56,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-8" href="#" rel="8" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button8" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '8', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/8-telefono-modelo-1.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
								
																			<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/9-telefono-modelo-2.html" class="product_image" title="Tel&eacute;fono Modelo 2">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/29-home_default/telefono-modelo-2.jpg" alt="Tel&eacute;fono Modelo 2" />
								<span class="product-additional" rel="9"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/9-telefono-modelo-2.html" title="Tel&eacute;fono Modelo 2">Tel&eacute;fono Modelo 2</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">82,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-9" href="#" rel="9" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button9" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '9', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/9-telefono-modelo-2.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
								
																			<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/10-telefono-modelo-3.html" class="product_image" title="Tel&eacute;fono Modelo 3">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/30-home_default/telefono-modelo-3.jpg" alt="Tel&eacute;fono Modelo 3" />
								<span class="product-additional" rel="10"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/10-telefono-modelo-3.html" title="Tel&eacute;fono Modelo 3">Tel&eacute;fono Modelo 3</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">67,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-10" href="#" rel="10" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button10" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '10', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/10-telefono-modelo-3.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
									</div>
								
					</div>		
			
		<div class="item ">
											 	 <div class="row  products-item clearfix">
																<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
					<div class="product-container clearfix">
						<div class="center_block">								
							<a href="http://adoraserver/audioactive/htdocs/home/11-telefono-modelo-4.html" class="product_image" title="Tel&eacute;fono modelo 4">
								<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/31-home_default/telefono-modelo-4.jpg" alt="Tel&eacute;fono modelo 4" />
								<span class="product-additional" rel="11"></span>	
								<span class="new">Nuevo</span>															</a>
							
						</div>
						<div class="right_block">
														<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/11-telefono-modelo-4.html" title="Tel&eacute;fono modelo 4">Tel&eacute;fono modelo 4</a></h4>
							
															<div class="content_price price_container">
									<span class="price" style="display: inline;">102,00 €</span>								
								</div>
													
							
							<div class="product_desc"></div>					
							
							<a class="rating_box leo-rating-11" href="#" rel="11" style="display:none">
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>      
							</a>
							
																								<span class="exclusive button pull-left">Añadir a la cesta</span>
																						<a  href="#" id="wishlist_button11" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '11', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
							<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/11-telefono-modelo-4.html?content_only=1"></a>
						</div>								
					</div>
				</div>				
									</div>
								
					</div>		
		</div>
</div>

							</div>
															</div>
			</div>
								<div class="leo-custom block no-blockshadown hidden-sm hidden-xs clearfix">
								<div class="titulares">
<h1>Tienda de audífonos y accesorios de sonido</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
</div>
			</div>
			</div>
<script>
$(document).ready(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            pause: true,
            interval: false
        });
    });
	$(".blockleoproducttabs").each( function(){
		$(".nav-tabs li", this).first().addClass("active");
		$(".tab-content .tab-pane", this).first().addClass("active");
	} );
});
</script>
					</div>
				</div>
			</section>
									<section id="columns" class="clearfix">
				<div class="container">
					<div class="row">
						

<!-- Center -->
<section id="center_column" class="col-md-12">
	<div class="clearfix">
	


					</div>
<!-- end div block_home -->
	</section>

	</div></div></section>

<!-- Footer -->
						<section id="bottom">
				<div class="container">
					<div class="row">
						 <div id="homecontent-displayBottom" class="col-md-12 ">
						<div class="leo-carousel clearfix">
									<div id="blockleohighlightcarousel-featured" class="products_block exclusive blockleohighlightcarousel">
					<div class="block_content ">	
						<div class="highlight-carousel block">
							<div class="highlight-image">
															</div>
							<div class="clearfix">
																										<div class="carousel slide" id="featured-15-carousel">
	 	
	<a class="carousel-control left" href="#featured-15-carousel"   data-slide="prev">&lsaquo;</a>
	<a class="carousel-control right" href="#featured-15-carousel"  data-slide="next">&rsaquo;</a>
		<div class="carousel-inner">
			<div class="item active">
												  <div class="row products-item ">
																	<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
						<div class="product-container clearfix">
							<div class="center_block">								
								<a href="http://adoraserver/audioactive/htdocs/home/8-telefono-modelo-1.html" class="product_image" title="Tel&eacute;fono Modelo 1">
									<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/27-home_default/telefono-modelo-1.jpg" alt="Tel&eacute;fono Modelo 1" />
									<span class="product-additional" rel="8"></span>	
									<span class="new">Nuevo</span>																	</a>
								
							</div>
							<div class="right_block">
																<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/8-telefono-modelo-1.html" title="Tel&eacute;fono Modelo 1">Tel&eacute;fono Modelo 1</a></h4>
								
																	<div class="content_price price_container">
										<span class="price" style="display: inline;">56,00 €</span>								
									</div>
														
								
								<div class="product_desc"></div>					
								
								<a class="rating_box leo-rating-8" href="#" rel="8" style="display:none">
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>      
								</a>
								
																											<span class="exclusive button pull-left">Añadir a la cesta</span>
																									<a  href="#" id="wishlist_button8" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '8', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
								<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/8-telefono-modelo-1.html?content_only=1"></a>
							</div>								
						</div>
					</div>
					
									
																					<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
						<div class="product-container clearfix">
							<div class="center_block">								
								<a href="http://adoraserver/audioactive/htdocs/home/9-telefono-modelo-2.html" class="product_image" title="Tel&eacute;fono Modelo 2">
									<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/29-home_default/telefono-modelo-2.jpg" alt="Tel&eacute;fono Modelo 2" />
									<span class="product-additional" rel="9"></span>	
									<span class="new">Nuevo</span>																	</a>
								
							</div>
							<div class="right_block">
																<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/9-telefono-modelo-2.html" title="Tel&eacute;fono Modelo 2">Tel&eacute;fono Modelo 2</a></h4>
								
																	<div class="content_price price_container">
										<span class="price" style="display: inline;">82,00 €</span>								
									</div>
														
								
								<div class="product_desc"></div>					
								
								<a class="rating_box leo-rating-9" href="#" rel="9" style="display:none">
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>      
								</a>
								
																											<span class="exclusive button pull-left">Añadir a la cesta</span>
																									<a  href="#" id="wishlist_button9" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '9', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
								<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/9-telefono-modelo-2.html?content_only=1"></a>
							</div>								
						</div>
					</div>
					
									
																					<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
						<div class="product-container clearfix">
							<div class="center_block">								
								<a href="http://adoraserver/audioactive/htdocs/home/10-telefono-modelo-3.html" class="product_image" title="Tel&eacute;fono Modelo 3">
									<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/30-home_default/telefono-modelo-3.jpg" alt="Tel&eacute;fono Modelo 3" />
									<span class="product-additional" rel="10"></span>	
									<span class="new">Nuevo</span>																	</a>
								
							</div>
							<div class="right_block">
																<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/10-telefono-modelo-3.html" title="Tel&eacute;fono Modelo 3">Tel&eacute;fono Modelo 3</a></h4>
								
																	<div class="content_price price_container">
										<span class="price" style="display: inline;">67,00 €</span>								
									</div>
														
								
								<div class="product_desc"></div>					
								
								<a class="rating_box leo-rating-10" href="#" rel="10" style="display:none">
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>      
								</a>
								
																											<span class="exclusive button pull-left">Añadir a la cesta</span>
																									<a  href="#" id="wishlist_button10" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '10', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
								<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/10-telefono-modelo-3.html?content_only=1"></a>
							</div>								
						</div>
					</div>
					
									</div>
									
						</div>		
			<div class="item ">
												  <div class="row products-item ">
																	<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
						<div class="product-container clearfix">
							<div class="center_block">								
								<a href="http://adoraserver/audioactive/htdocs/home/11-telefono-modelo-4.html" class="product_image" title="Tel&eacute;fono modelo 4">
									<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/31-home_default/telefono-modelo-4.jpg" alt="Tel&eacute;fono modelo 4" />
									<span class="product-additional" rel="11"></span>	
									<span class="new">Nuevo</span>																	</a>
								
							</div>
							<div class="right_block">
																<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/11-telefono-modelo-4.html" title="Tel&eacute;fono modelo 4">Tel&eacute;fono modelo 4</a></h4>
								
																	<div class="content_price price_container">
										<span class="price" style="display: inline;">102,00 €</span>								
									</div>
														
								
								<div class="product_desc"></div>					
								
								<a class="rating_box leo-rating-11" href="#" rel="11" style="display:none">
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>      
								</a>
								
																											<span class="exclusive button pull-left">Añadir a la cesta</span>
																									<a  href="#" id="wishlist_button11" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '11', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
								<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/11-telefono-modelo-4.html?content_only=1"></a>
							</div>								
						</div>
					</div>
					
									
																					<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
						<div class="product-container clearfix">
							<div class="center_block">								
								<a href="http://adoraserver/audioactive/htdocs/home/12-audifono-modelo-1.html" class="product_image" title="Aud&iacute;fono modelo 1">
									<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/32-home_default/audifono-modelo-1.jpg" alt="Aud&iacute;fono modelo 1" />
									<span class="product-additional" rel="12"></span>	
									<span class="new">Nuevo</span>																	</a>
								
							</div>
							<div class="right_block">
																<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/12-audifono-modelo-1.html" title="Aud&iacute;fono modelo 1">Aud&iacute;fono modelo 1</a></h4>
								
																	<div class="content_price price_container">
										<span class="price" style="display: inline;">120,00 €</span>								
									</div>
														
								
								<div class="product_desc"></div>					
								
								<a class="rating_box leo-rating-12" href="#" rel="12" style="display:none">
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>      
								</a>
								
																											<span class="exclusive button pull-left">Añadir a la cesta</span>
																									<a  href="#" id="wishlist_button12" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '12', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
								<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/12-audifono-modelo-1.html?content_only=1"></a>
							</div>								
						</div>
					</div>
					
									
																					<div class="col-sm-4 col-sm-4 col-xs-12 product_block ajax_block_product">
						<div class="product-container clearfix">
							<div class="center_block">								
								<a href="http://adoraserver/audioactive/htdocs/home/13-audifono-modelo-2.html" class="product_image" title="Aud&iacute;fono modelo 2">
									<img class="img-responsive" src="http://adoraserver/audioactive/htdocs/33-home_default/audifono-modelo-2.jpg" alt="Aud&iacute;fono modelo 2" />
									<span class="product-additional" rel="13"></span>	
									<span class="new">Nuevo</span>																	</a>
								
							</div>
							<div class="right_block">
																<h4 class="name"><a href="http://adoraserver/audioactive/htdocs/home/13-audifono-modelo-2.html" title="Aud&iacute;fono modelo 2">Aud&iacute;fono modelo 2</a></h4>
								
																	<div class="content_price price_container">
										<span class="price" style="display: inline;">210,00 €</span>								
									</div>
														
								
								<div class="product_desc"></div>					
								
								<a class="rating_box leo-rating-13" href="#" rel="13" style="display:none">
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>      
								</a>
								
																											<span class="exclusive button pull-left">Añadir a la cesta</span>
																									<a  href="#" id="wishlist_button13" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '13', $('#idCombination').val(), 1 ); return false;" title="Agregar a la lista de deseos"></a>
								<a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="Vista rápida" href="http://adoraserver/audioactive/htdocs/home/13-audifono-modelo-2.html?content_only=1"></a>
							</div>								
						</div>
					</div>
					
									</div>
									
						</div>		
		</div>
</div>

															</div>
						</div>
					</div>
										</div>
							</div>
								<div class="leo-custom block">
								<p class="hidden-xs"><a title="" href="#"><img class="img-responsive" style="border-radius: 5px;" src="/audioactive/htdocs/modules/leomanagewidgets/data/banner-mass-bottom.jpg" alt="" /></a></p>
			</div>
			</div>
<script>
$(document).ready(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            pause: true,
            interval: false
        });
    });
	$(".blockleoproducttabs").each( function(){
		$(".nav-tabs li", this).first().addClass("active");
		$(".tab-content .tab-pane", this).first().addClass("active");
	} );
});
</script><div id="mycarouselHolder" align="center" class="col-md-12">
	<div class="block">
					

		  <ul id="lofjcarousel" class="jcarousel-skin-tango">
							<li class="lof-item">
					<a href="http://adoraserver/audioactive/htdocs/1_apple-computer-inc">
						<img src="/audioactive/htdocs/img/m/es-default-m_scene_default.jpg" alt="Apple Computer, Inc" vspace="0" border="0" />
						
					</a>
				</li>
							<li class="lof-item">
					<a href="http://adoraserver/audioactive/htdocs/2_shure-incorporated">
						<img src="/audioactive/htdocs/img/m/es-default-m_scene_default.jpg" alt="Shure Incorporated" vspace="0" border="0" />
						
					</a>
				</li>
					  </ul>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#lofjcarousel").flexisel({
			visibleItems: 6,
			animationSpeed: 1000,
			autoPlay: 0,
			autoPlaySpeed: 3000,
			pauseOnHover: 1,
			enableResponsiveBreakpoints: 1,
	    	responsiveBreakpoints: { 
	    		portrait: { 
	    			changePoint:480,
	    			visibleItems: 1
	    		},
	    		landscape: { 
	    			changePoint:640,
	    			visibleItems: 2
	    		},
	    		tablet: {
	    			changePoint:768,
	    			visibleItems: 6
	    		}
	    	}
	    });
	});
</script>
					</div>
				</div>
			</section>
						<footer id="footer" class="clearfix">
				<section id="footer-top" class="footer">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div id="advandfooter" class="adv-footer-wrap">
			<div id="lofadva-pos-1" class="blocks" style="width:100%">
			<div class="blocks-wrap  clearfix">
							<div  style="width:100.00%; float:left;">
					<div class="block-wrap">
												<div class="block_content">
							<ul class="widget-items">
																								<li class="widget-text">
																				<div class="social"><a title="" href="#"><em class="fa fa-pinterest"> </em>Pinterest</a> <a title="" href="#"><em class="fa fa-facebook"> </em>Facebook</a> <a title="" href="#"><em class="fa fa-google-plus"> </em>Google+</a> <a title="" href="#"><em class="fa fa-twitter"> </em>Twitter</a> <a title="" href="#"><em class="fa fa-youtube"> </em>Youtube</a></div>
									</li>
																						</ul>
						</div>	
					</div>
				</div>
						</div>
		</div>
			<div id="lofadva-pos-2" class="blocks" style="width:100%">
			<div class="blocks-wrap  clearfix">
							<div  style="width:25.00%; float:left;">
					<div class="block-wrap">
													<h3 class="title_block">Mi cuenta</h3>
												<div class="block_content">
							<ul class="widget-items">
																								<li class="link"><a href="http://adoraserver/audioactive/htdocs/historial-de-pedidos" title="Mis pedidos" target="" >Mis pedidos</a></li>
																																<li class="link"><a href="http://adoraserver/audioactive/htdocs/vales" title="Mis hojas de crédito" target="_self" >Mis hojas de crédito</a></li>
																																<li class="link"><a href="http://adoraserver/audioactive/htdocs/direcciones" title="Mis direcciones" target="_self" >Mis direcciones</a></li>
																																<li class="link"><a href="http://adoraserver/audioactive/htdocs/identidad" title="Mis datos personales" target="_self" >Mis datos personales</a></li>
																																<li class="link"><a href="http://adoraserver/audioactive/htdocs/descuento" title="Mis vales" target="_self" >Mis vales</a></li>
																						</ul>
						</div>	
					</div>
				</div>
							<div  style="width:25.00%; float:left;">
					<div class="block-wrap">
													<h3 class="title_block">information</h3>
												<div class="block_content">
							<ul class="widget-items">
																								<li class="link"><a href="http://adoraserver/audioactive/htdocs/content/1-entrega" title="Entrega" target="_self" >Entrega</a></li>
																																<li class="link"><a href="http://adoraserver/audioactive/htdocs/content/2-aviso-legal" title="Aviso Legal" target="_self" >Aviso Legal</a></li>
																																<li class="link"><a href="http://adoraserver/audioactive/htdocs/content/3-condiciones-de-uso" title="Términos y condiciones de uso" target="_self" >Términos y condiciones de uso</a></li>
																																<li class="link"><a href="http://adoraserver/audioactive/htdocs/content/4-sobre" title="¿Quiénes somos?" target="_self" >¿Quiénes somos?</a></li>
																																<li class="link"><a href="http://adoraserver/audioactive/htdocs/content/5-pago-seguro" title="Secure payment" target="_self" >Secure payment</a></li>
																						</ul>
						</div>	
					</div>
				</div>
							<div  style="width:25.00%; float:left;">
					<div class="block-wrap">
												<div class="block_content">
							<ul class="widget-items">
																								<li class="widget-module">
																				<!-- MODULE Block contact infos -->
<div id="block_contact_infos">
	<h4 class="title_block">Contacte con nosotros</h4>
		<p>My Company</p>	<ul>
		<li class="link"><span class="fa fa-map-marker"> </span> 42 avenue des Champs Elys&eacute;es
75000 Paris
France</li>		<li class="link"><span class="fa fa-mobile-phone"> </span>Teléfono: 0123-456-789</li>		<li class="link"><span class="fa fa-envelope"> </span>Email: <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;%73%61%6c%65%73@%79%6f%75%72%63%6f%6d%70%61%6e%79.%63%6f%6d" >&#x73;&#x61;&#x6c;&#x65;&#x73;&#x40;&#x79;&#x6f;&#x75;&#x72;&#x63;&#x6f;&#x6d;&#x70;&#x61;&#x6e;&#x79;&#x2e;&#x63;&#x6f;&#x6d;</a></li>	</ul>
</div>
<!-- /MODULE Block contact infos -->

									</li>
																						</ul>
						</div>	
					</div>
				</div>
							<div  style="width:25.00%; float:left;">
					<div class="block-wrap">
													<h3 class="title_block">Newsletter</h3>
												<div class="block_content">
							<ul class="widget-items">
																								<li class="widget-module">
																				
<!-- Block Newsletter module-->

<div id="newsletter_block_left" class="block">
	<p class="title_block">Newsletter</p>
	<div class="block_content">
		<div class="description">
		<p>Get the word out. Share this page with your frien...</p>
	</div>
		<form action="http://adoraserver/audioactive/htdocs/" method="post">
			<p>
				<input class="inputNew" id="newsletter-input" type="text" name="email" size="18" value="su email" />
				<input type="submit" value="ok" class="button_mini btn btn-theme-primary" name="submitNewsletter" />
				<input type="hidden" name="action" value="0" />
			</p>
		</form>
	</div>
</div>
<!-- /Block Newsletter module-->

<script type="text/javascript">
    var placeholder = "su email";
        $(document).ready(function() {
            $('#newsletter-input').on({
                focus: function() {
                    if ($(this).val() == placeholder) {
                        $(this).val('');
                    }
                },
                blur: function() {
                    if ($(this).val() == '') {
                        $(this).val(placeholder);
                    }
                }
            });

                    });
</script>

									</li>
																						</ul>
						</div>	
					</div>
				</div>
						</div>
		</div>
	</div><script type="text/javascript">
	var leoOption = {
		productNumber:1,
		productRating:1,
		productInfo:1,
		productTran:1,
		productQV:1	}
    $(document).ready(function(){	
		var leoCustomAjax = new $.LeoCustomAjax();
        leoCustomAjax.processAjax();
    });
</script>

							</div>
						</div>
					</div>
				</section>	
				<section id="footer-bottom">
					<div class="container">
						<div class="container-inner">
							<div class="row">
								<div class="col-sm-6">
									<div class="copyright">
											Copyright 2013 Powered by PrestaShop. All Rights Reserved. 
											Design by <a href="http://prestashop.com" title="LeoTheme - Prestashop Themes Club" target="_blank">LeoThemes</a>
									</div>
								</div>
																<div class="col-sm-6 hidden-xs"><div class="footnav"><div id="homecontent-displayFootNav">
						<div class="leo-custom">
								<p class="pull-right"><a title="paypal1" href="#"><img src="/audioactive/htdocs/modules/leomanagewidgets/data/paypal1.png" alt="paypal1" /></a> <a title="paypal2" href="#"><img src="/audioactive/htdocs/modules/leomanagewidgets/data/paypal2.png" alt="paypal2" /></a> <a title="paypal3" href="#"><img src="/audioactive/htdocs/modules/leomanagewidgets/data/paypal3.png" alt="paypal3" /></a> <a title="paypal4" href="#"><img src="/audioactive/htdocs/modules/leomanagewidgets/data/paypal4.png" alt="paypal4" /></a></p>
			</div>
			</div>
			<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
        <h4 class="modal-title" id="myModalLabel">Consulta recibida</h4>
      </div>
      <div class="modal-body">
        Muchas gracias por ponerse en contacto con nosotros.<br/>
		Le responderemos en la mayor brevedad.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            pause: true,
            interval: false
        });
    });
	$(".blockleoproducttabs").each( function(){
		$(".nav-tabs li", this).first().addClass("active");
		$(".tab-content .tab-pane", this).first().addClass("active");
	} );
	
	$('#myModal').modal('show');
	$('#myModal').on('hide.bs.modal', function (e) {
	  //console.log("capturado");
	  url = "index.php";
	  $(location).attr('href',url);
	})
	
});
</script></div></div>		
															</div>
						</div>
					</div>	
				</section>
				
			</footer>
		</section>
				<script type="text/javascript">
			var classBody = ".png";
			$("body").addClass( classBody.replace(/\.\w+$/,"")  );
		</script>
	</body>
</html>

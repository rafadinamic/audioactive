{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$lang_iso}">
	<head>
		<title>{$meta_title|escape:'htmlall':'UTF-8'}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:html:'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:html:'UTF-8'}" />
{/if}
		<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
		<meta http-equiv="content-language" content="{$meta_language}" />
        <meta name="author" content="Adoramedia"/>
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		<script type="text/javascript">
			var baseDir = '{$content_dir|addslashes}';
			var baseUri = '{$base_uri|addslashes}';
			var static_token = '{$static_token|addslashes}';
			var token = '{$token|addslashes}';
			var priceDisplayPrecision = {$priceDisplayPrecision*$currency->decimals};
			var priceDisplayMethod = {$priceDisplay};
			var roundMode = {$roundMode};
		</script>
                <link rel="stylesheet" type="text/css" href="{$BOOTSTRAP_CSS_URI}"/> 
{if isset($css_files)}
	{foreach from=$css_files key=css_uri item=media}
	<link href="{$css_uri}" rel="stylesheet" type="text/css" media="{$media}" />
	{/foreach}
{/if}
{if $LEO_SKIN_DEFAULT &&  $LEO_SKIN_DEFAULT !="default"}
	<link rel="stylesheet" type="text/css" href="{$content_dir}themes/{$LEO_THEMENAME}/skins/{$LEO_SKIN_DEFAULT}/css/skin.css"/>
{/if}
	<link rel="stylesheet" type="text/css" href="{$content_dir}themes/{$LEO_THEMENAME}/css/theme-responsive.css"/>


{if isset($js_files)}
	{foreach from=$js_files item=js_uri}
	<script type="text/javascript" src="{$js_uri}"></script>
	{/foreach}
{/if}
{$LEO_CUSTOMWIDTH}
<link rel="stylesheet" type="text/css" href="{$content_dir}themes/{$LEO_THEMENAME}/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="{$content_dir}themes/{$LEO_THEMENAME}/css/fonts.css"/>
<script type="text/javascript" src="{$content_dir}themes/{$LEO_THEMENAME}/js/custom.js"></script>
<script type="text/javascript" src="{$content_dir}themes/{$LEO_THEMENAME}/js/jquery.cookie.js"></script>
<script type="text/javascript" src="{$content_dir}themes/{$LEO_THEMENAME}/js/jquery.form.min.js"></script>
{if $hide_left_column||in_array($page_name,array(index))}{$HOOK_LEFT_COLUMN=null}{/if}
{if $hide_right_column|| in_array($page_name,array(index))}{$HOOK_RIGHT_COLUMN=null}{/if}
{assign var='classfix' value=0}
{if in_array($page_name,array(index,'prices-drop','new-products','category','best-sales','manufacturer','search','supplier','product'))}{$classfix=1}{/if}

<link href='http://fonts.googleapis.com/css?family=Lato:400,100,300,700|Raleway:400,100,300,500,600,700' rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
<script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
	{$HOOK_HEADER}
  {literal}
  <!-- INICIO CÓDIGO ANALYTICS -->
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55324087-1', 'auto');
  ga('send', 'pageview');
  </script>
  <!-- FIN CÓDIGO ANALYTICS -->
  <!-- Facebook Pixel Code -->
	  <script>
		  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
				  document,'script','//connect.facebook.net/en_US/fbevents.js');

		  fbq('init', '1491299767842621');
		  fbq('track', "PageView");</script>
	  <noscript><img height="1" width="1" style="display:none"
					 src="https://www.facebook.com/tr?id=1491299767842621&ev=PageView&noscript=1"
				  /></noscript>
	  <!-- End Facebook Pixel Code -->
  {/literal}


    </head>

	<body {if isset($page_name)}id="{$page_name|escape:'htmlall':'UTF-8'}"{/if} class="{$LEO_BGPATTERN} fs{$FONT_SIZE} {if isset($page_name)}{$page_name|escape:'htmlall':'UTF-8'}{/if}{if $hide_left_column} hide-left-column{/if}{if $hide_right_column} hide-right-column{/if}{if $content_only} content_only{/if} keep-header">
	<p style="display: block;" id="back-top"> <a href="#top"><span></span></a> </p>
	{if !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
		<div id="restricted-country">
			<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>
		</div>
		{/if}
		<section id="page" class="clearfix">
			
			<!-- Header -->
			<header id="header" class="navbar-fixed-top clearfix">
				<section id="topbar">
					<div class="container">
						{$HOOK_TOP}	
					</div>
				</section>
				<section id="header-main">
					<div class="container" >
						<div class="header-wrap">
							<div class="pull-left">
								<a id="header_logo" href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}">
									<img class="logo" src="{$logo_url}" alt="{$shop_name|escape:'htmlall':'UTF-8'}" width="200" height="45"/>
								</a>
							</div>
							{if !empty($HOOK_HEADERRIGHT)}
								<div id="header_right" class="col-sm-6 inner">
									{$HOOK_HEADERRIGHT}	
								</div>
							{/if}
							{if !empty($HOOK_TOPNAVIGATION) }
								<nav id="topnavigation" class="clearfix">
									<div class="container">
										<div class="row">
											 {$HOOK_TOPNAVIGATION}
										</div>
									</div>
								</nav>
							{/if}
						</div>
					</div>
				</section>	
			</header>
			
			{if $HOOK_SLIDESHOW &&  in_array($page_name,array('index'))}
			<section id="slideshow" class="clearfix hidden-xs hidden-sm">
				<div class="container">
					<div class="row">
						 {$HOOK_SLIDESHOW}
					</div>
				</div>
			</section>
			{/if}
			{if $HOOK_PROMOTETOP &&  in_array($page_name,array('index'))}
			<section id="promotetop" class="clearfix">
				<div class="container">
					<div class="row">
						 {$HOOK_PROMOTETOP}
					</div>
				</div>
			</section>
			{/if}
			{if !in_array($page_name,array('index'))}					
				<section id="breadcrumb" class="clearfix">
					<div class="container">
						<div class="row">
							{include file="$tpl_dir./breadcrumb.tpl"}
						</div>
					</div>
				</section>					
			{/if}
			<section id="columns" class="clearfix">
				<div class="container">
					<div class="row">
						{include file="$tpl_dir./layout/{$LEO_LAYOUT_DIRECTION}/header.tpl" hide_left_column=$hide_left_column hide_right_column=$hide_right_column }
	{/if}

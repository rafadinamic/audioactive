{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block Newsletter module-->

<div id="newsletter_block_left" class="block">
	<p class="title_block">{l s='Newsletter' mod='blocknewsletter'}</p>
	<div class="block_content">
	<div class="description">
		<p>Get the word out. Share this page with your frien...</p>
	</div>
		<form action="{$link->getPageLink('index')|escape:'html'}" method="post" id="form_newsletter">
			<p>
				<span>Suscr&iacute;bete al Newsletter </span><input class="inputNew" id="newsletter-input" type="text" name="email" size="18" value="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='blocknewsletter'}{/if}" /><input type="submit" value="Enviar" class="submitNewsletterForm button_mini btn btn-theme-primary" name="submitNewsletter" />
				<input type="hidden" name="action" value="0" />
                <span class="newsletter_warning">
                    <input type="checkbox" name="privacy" id="privacy_confirm">
                    Mediante el envío de mis datos personales confirmo que he leído y acepto la <a target="_blank" href="http://www.audioactive.es/politicas-de-privacidad-10" style="color:rgba(190,160,197,1);">política de privacidad.</a>
                    {if isset($msg) && $msg}
                        <p class="{if $nw_error}warning{else}success{/if}" style="color: #c0a2c7; background:none; border:none; display:block">{$msg}</p>
                    {/if}
                </span>
			</p>
		</form>
	</div>
</div>
<!-- /Block Newsletter module-->

<script type="text/javascript">
    var placeholder = "{l s='your e-mail' mod='blocknewsletter' js=1}";
        $(document).ready(function() {ldelim}
            $('#newsletter-input').on({ldelim}
                focus: function() {ldelim}
                    if ($(this).val() == placeholder) {ldelim}
                        $(this).val('');
                    {rdelim}
                {rdelim},
                blur: function() {ldelim}
                    if ($(this).val() == '') {ldelim}
                        $(this).val(placeholder);
                    {rdelim}
                {rdelim}
            {rdelim});

            {if isset($msg)}
                $('#columns').before('<div class="clearfix"></div><p class="{if $nw_error}warning{else}success{/if}">{l s="Newsletter:" js=1 mod="blocknewsletter"} {$msg}</p>');
            {/if}
        });
        $('#form_newsletter').submit(function(){

            if(! $('#privacy_confirm').is(':checked'))
            {
                $('.newsletter_warning').children('.warning').remove();
                $('.newsletter_warning').append('<p class="warning" style="color: #c0a2c7; background:none; border:none; display:block">Debe aceptar la política de privacidad.</p>');
                return false;
            }

        });
</script>

{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block user information module HEADER -->
<ul id="header_user_info" class="links hidden-xs hidden-sm pull-left">
	<!--<li>	
		{l s='Welcome to' mod='blockuserinfo'}
			<a href="{$link->getPageLink('index', true)|escape:'html'}" title="{$shop_name|escape:'htmlall':'UTF-8'}" class="login" rel="nofollow">{$shop_name|escape:'htmlall':'UTF-8'}</a>
		{l s='online store' mod='blockuserinfo'}
	</li>-->
</ul>
	{if !$PS_CATALOG_MODE}
		<ul id="header_nav_cart" class="links pull-right">
			{if $cart_qties > 0}<li class="ico_cart_amount">{$cart_qties}</li>{else}<li class="ico_cart_amount" style="display: none;">0</li>{/if}
			<li id="shopping_cart">
				<a  href="{$link->getPageLink($order_process, true)|escape:'html'}" title="{l s='View my shopping cart' mod='blockuserinfo'}" rel="nofollow">
					<em class="icon-quitar-carrito"></em>				
					<span class="ajax_cart_quantity{if $cart_qties == 0} hidden{/if}">{$cart_qties}</span>
					<span class="ajax_cart_product_txt{if $cart_qties != 1} hidden{/if}">{l s='Item' mod='blockuserinfo'}</span>
					<span class="ajax_cart_product_txt_s{if $cart_qties < 2} hidden{/if}">{l s='Items' mod='blockuserinfo'}</span>
					<span class="ajax_cart_total{if $cart_qties == 0} hidden{/if}">
						{if $cart_qties > 0}
							{if $priceDisplay == 1}
								{assign var='blockuser_cart_flag' value='Cart::BOTH_WITHOUT_SHIPPING'|constant}
								{convertPrice price=$cart->getOrderTotal(false, $blockuser_cart_flag)}
							{else}
								{assign var='blockuser_cart_flag' value='Cart::BOTH_WITHOUT_SHIPPING'|constant}
								{convertPrice price=$cart->getOrderTotal(true, $blockuser_cart_flag)}
							{/if}
						{/if}
					</span>
					<span class="ajax_cart_no_product{if $cart_qties > 0} hidden{/if}">
							0 {l s='Item' mod='blockuserinfo'}(s) - 
							{convertPrice price=0}
							{assign "enlacecarrito" "{$link->getPageLink($order_process, true)|escape:'html'}"}
					</span>
				</a>
			</li>
		</ul>
	{/if}
<div id="header_user" class="{if $PS_CATALOG_MODE}header_user_catalog {/if}pull-right">
	<a data-toggle="dropdown" class="groupe-btn dropdown hidden-sm hidden-md hidden-lg" title="{l s='Quick link' mod='blockuserinfo'}" href="#"><span class="fa fa-user"></span></a>
	<ul id="header_nav" class="links groupe-content">
		<!--<li>{l s='Welcome' mod='blockuserinfo'},</li>-->
		{if $logged}
			<li><a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow"><span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a></li>
			<li><a href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html'}" title="{l s='Log me out' mod='blockuserinfo'}" class="logout" rel="nofollow"><span class="fa fa-times"></span>{l s='Log out' mod='blockuserinfo'}</a></li>
			<li ><a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='My Account' mod='blockuserinfo'}" ><span class="fa fa-user"></span>{l s='My Account' mod='blockuserinfo'}</a></li>
		{else}
			<!--<li><a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='Login to your customer account' mod='blockuserinfo'}" class="login" rel="nofollow"><span class="fa fa-lock"></span>{l s='Login' mod='blockuserinfo'}</a></li>-->
			<li><a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='Login to your customer account' mod='blockuserinfo'}" class="login" rel="nofollow"><span class="fa fa-user"></span>{l s='Login' mod='blockuserinfo'} / {l s='Register' mod='blockuserinfo'}</a></li>			
		{/if}
		<!--<li ><a href="{$link->getModuleLink('blockwishlist', 'mywishlist')}" title="{l s='My wishlist' mod='blockuserinfo'}"><span class="fa fa-heart"></span>{l s='My wishlist' mod='blockuserinfo'}</a></li>-->
	</ul>
			
</div>

<!-- /Block user information module HEADER -->
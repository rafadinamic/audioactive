{if !empty($products)}
<div class=" carousel slide" id="{$tabname}">
	 {if count($products)>$itemsperpage}	
	<a class="carousel-control left" href="#{$tabname}"   data-slide="prev">&lsaquo;</a>
	<a class="carousel-control right" href="#{$tabname}"  data-slide="next">&rsaquo;</a>
	{/if}
	<div class="carousel-inner">
	{$mproducts=array_chunk($products,$itemsperpage)}
	{foreach from=$mproducts item=products name=mypLoop}
		<div class="item {if $smarty.foreach.mypLoop.first}active{/if}">
			{foreach from=$products item=product name=products}
				{if $product@iteration%$columnspage==1&&$columnspage>1}
				  <div class="row products-item clearfix ">
				{/if}
					<div class="col-sm-6 col-xs-12 col-md-{$scolumn} product_block ajax_block_product">
						<div class="product-container clearfix">
							<div class="center_block">								
								<a href="{$product.link|escape:'htmlall':'UTF-8'}" class="product_image" title="{$product.name|escape:'htmlall':'UTF-8'}">
									<img class="img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html'}" alt="{$product.name|escape:'htmlall':'UTF-8'}" />
									<span class="product-additional" rel="{$product.id_product}"></span>	
									{if isset($product.new) && $product.new == 1}<span class="new">{l s='New' mod='blockleorelatedproducts'}</span>{/if}
								</a>
								
							</div>
							<div class="right_block">
								{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}<span class="on_sale">{l s='On sale!' mod='blockleorelatedproducts'}</span>{elseif isset($product.reduction) && $product.reduction && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}<span class="discount">{l s='Reduced price!' mod='blockleorelatedproducts'}</span>
								{/if}
								<h4 class="name">{if isset($product.pack_quantity) && $product.pack_quantity}{$product.pack_quantity|intval|cat:' x '}{/if}<a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name|truncate:35:'...'|escape:'htmlall':'UTF-8'}</a></h4>
								
								{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
									<div class="content_price price_container">
										{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}<span class="price" style="display: inline;">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span><br />{/if}
										{if isset($product.available_for_order) && $product.available_for_order && !isset($restricted_country_mode)}<span class="availability">{if ($product.allow_oosp || $product.quantity > 0)}{l s='Available' mod='blockleorelatedproducts'}{elseif (isset($product.quantity_all_versions) && $product.quantity_all_versions > 0)}{l s='Product available with different options' mod='blockleorelatedproducts'}{else}<span class="warning_inline">{l s='Out of stock' mod='blockleorelatedproducts'}</span>{/if}</span>{/if}
									</div>
								{/if}

								<div class="rating_trustivity">
									{Product::api_trust_product_print_rating($product.id_product)}
								</div>
								
							</div>								
						</div>
					</div>			
				{if ($product@iteration%$columnspage==0||$smarty.foreach.products.last)&&$columnspage>1}
					</div>
				{/if}				
			{/foreach}
		</div>		
	{/foreach}
	</div>
</div>
{/if}
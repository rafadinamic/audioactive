{if !empty($mproducts)}
{$itemsperpage=$datas.configs.itemspage}
{$columnspage=$datas.configs.columns}
<div class="carousel slide" id="{$tabname}-{$datas.id_leomanagewidgets}-carousel">
	 {if count($mproducts)>100}	
	<a class="carousel-control left" href="#{$tabname}-{$datas.id_leomanagewidgets}-carousel"   data-slide="prev">&lsaquo;</a>
	<a class="carousel-control right" href="#{$tabname}-{$datas.id_leomanagewidgets}-carousel"  data-slide="next">&rsaquo;</a>
	{/if}
	<div class="carouseld-inner">
	<div class="item active">
	<div class="row products-item ">
		{foreach from=$mproducts item=products name=mypLoop}
			
			{foreach from=$products item=product name=products}
				{if $product@iteration%$columnspage==1&&$columnspage>1}
				<!--<div class="row products-item ">-->
				{/if}
				{assign var='proReduce' value=""}
				{if $product.specific_prices.reduction}
					{if $product.specific_prices.reduction_type == 'percentage'}
						{assign var='proReduce' value=$product.specific_prices.reduction*100}
					{elseif $product.specific_prices.reduction_type == 'amount'}
						{assign var='proReduce' value=ceil($product.specific_prices.reduction/$product.price*100)}
					{/if}
				{/if}
					<div class="{if $datas.ncolumn == 5}leo-md-5{else}col-sm-{$datas.scolumn}{/if} col-sm-4 col-xs-12 product_block ajax_block_product">
						<div class="product-container clearfix">
							<div class="center_block">								
								<a href="{$product.link|escape:'htmlall':'UTF-8'}" class="product_image" title="{$product.name|escape:'htmlall':'UTF-8'}">
									<img class="img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html'}" alt="{$product.name|escape:'htmlall':'UTF-8'}" width="200" height="200" />
									<span class="product-additional" rel="{$product.id_product}"></span>	
									{if isset($product.new) && $product.new == 1}<span class="new">{l s='New' mod='leomanagewidgets'}</span>{/if}
                                    {if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}<span class="on_sale">{l s='On sale!' mod='leomanagewidgets'}</span>{elseif isset($product.reduction) && $product.reduction && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}<span class="discount">{l s='Reduced price!' mod='leomanagewidgets'}</span>
								{/if}
								</a>
								
							</div>
							<div class="right_block">

								<h4 class="name">{if isset($product.pack_quantity) && $product.pack_quantity}{$product.pack_quantity|intval|cat:' x '}{/if}<a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name|truncate:35:'...'|escape:'htmlall':'UTF-8'}</a></h4>

								{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
								
								<div class="rating_trustivity">
									{Product::api_trust_product_print_rating($product.id_product)}
								</div>
								
									<div class="content_price price_container">
										{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}<span class="price" style="display: inline;">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span>{/if}								
									</div>
														
								{/if}

							</div>								
						</div>
					</div>
					
				{if ($product@iteration%$columnspage==0||$smarty.foreach.products.last)&&$columnspage>1}
					<!--</div>-->
				{/if}
					
				{/foreach}
			
		{/foreach}
		{if count($mproducts)>1}<a href="#" class='masresultados'><span>M&aacute;s productos</span></a>{/if}
		</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function()
{
	var productos=$(".product-container").length;
	if (productos>10)
	{
		$(".product-container:gt(9)").hide();
		$(".masresultados").click(function(e)
		{
			e.preventDefault();
			$(".product-container:gt(9)").slideToggle();
		});
	}
	else $(".masresultados").hide();
});
</script>
{/if}



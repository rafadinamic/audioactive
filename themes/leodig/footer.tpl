{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

		{if !$content_only}
		{include file="$tpl_dir./layout/{$LEO_LAYOUT_DIRECTION}/footer.tpl"  }
	</div></div></section>

{if in_array($page_name,array('index'))}
    <hgroup class="contacto_mobile"><h2>Centro de Atención al Cliente <span><a href="tel:+966115079">966 115 079</a></span></h2><h3 class="cac-time">Lu. a Vi. de 9:30 a 13:30</h3> </hgroup>
    {FrontController::CustomSWIPDispatcherPosts(true, false, null, null)}
{/if}

<!-- Footer -->
			{if $HOOK_BOTTOM}
			<section id="bottom">
				<div class="container">
					<div class="row">
						 {$HOOK_BOTTOM}
					</div>
				</div>
			</section>
			{/if}
			<footer id="footer" class="clearfix">
				<section id="footer-top" class="footer">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								{$HOOK_FOOTER}
							</div>
						</div>
					</div>
				</section>	
				<!--<section id="footer-bottom">
					<div class="container">
						<div class="container-inner">
							<div class="row">
								<div class="col-sm-{if $HOOK_FOOTNAV}6{else}12{/if}">
									<div class="copyright">
											&copy; Audioactive. Todos los derechos reservados. dfgdf
									</div>
								</div>
								{if $HOOK_FOOTNAV}
								<div class="col-sm-6 hidden-xs"><div class="footnav">{$HOOK_FOOTNAV}</div></div>		
								{/if}
							</div>
						</div>
					</div>	
				</section>-->
                <!--{if !$avisocookie}
                <div id="politicaCookie" style="background-color: #222; bottom: 0; color: #FFFFFF; font-size: 11px; line-height: 15px; opacity: 0.8; padding: 10px 0px; position: fixed; text-align: center; width: 100%; z-index: 1000;">
                    <span style="display: inline-block;  width: 73%; vertical-align: middle;">Audioactive.es utiliza cookies para mejorar tu experiencia de navegación y realizar tareas de análisis.
  Al continuar con tu navegación entendemos que das tu consentimiento a nuestra <a href="{$base_dir}content/2-aviso-legal" title="politica de cookies" style="color: #FFFFFF; text-decoration:underline;" >política de cookies</a>.</span>
                    <span id="btCookie" style="display: inline-block; background: #fff; color: #000; padding: 2px; width: 6%; margin-left: 10px; cursor: pointer; font-size: 11px;">Accept</span>
                </div>
                {/if}-->
			</footer>
		</section>
	{/if}
	{if $LEO_PANELTOOL}
    	{include file="$tpl_dir./info/paneltool.tpl"}
    {/if}
		<script type="text/javascript">
			var classBody = "{$LEO_PATTERN}";
			$("body").addClass( classBody.replace(/\.\w+$/,"")  );
		</script>

<script type="text/javascript">
    //<![CDATA[
    function setCookie(nombre, valor, tiempo){
        var fecha = new Date();
        fecha.setTime(fecha.getTime() + tiempo);
        document.cookie = nombre + ' = ' + escape(valor) + ((tiempo == null) ? '' : '; expires = ' + fecha.toGMTString()) + '; path=/';
    }

    function getCookie(nombre){
        var nombreCookie, valorCookie, cookie = null, cookies = document.cookie.split(';');
        for (i=0; i<cookies.length; i++){
            valorCookie = cookies[i].substr(cookies[i].indexOf('=') + 1);
            nombreCookie = cookies[i].substr(0,cookies[i].indexOf('=')).replace(/^\s+|\s+$/g, '');
            if (nombreCookie == nombre)
                cookie = unescape(valorCookie);
        }
        return cookie;
    }
    //]]>
</script>

        <script type="text/javascript">
            var correcto = false;
            $('#nombre_fp').attr('required', 'required');
            $('#email_fp').attr('required', 'required');
            $('#consulta_fp').attr('required', 'required');

          $(document).ready(function(){

                $('#nombre_fp').focusout(function(){
                    $('#error_form_nombre').html('');
                    correcto = true;
                   if( ($("#nombre_fp").attr('value').length) < 2 )
                   {
                       $('#error_form_nombre').html('El nombre es muy corto');
                       correcto = false;
                   }
                });

                $('#email_fp').focusout(function(){
                    $('#error_form_email').html('');
                    if($("#email_fp").val().indexOf('@', 0) == -1 || $("#email_fp").val().indexOf('.', 0) == -1) {
                        $('#error_form_email').html('E-mail incorrecto');
                    }
                });

          });
        </script>
        <script type="text/javascript">
            var correcto = false;
            $('#guest_email').attr('required', 'required');
            $('#firstname').attr('required', 'required');
            $('#lastname').attr('required', 'required');
            $('#address1').attr('required', 'required');
            $('#postcode').attr('required', 'required');
            $('#city').attr('required', 'required');
            $('#id_state').attr('required', 'required');
            /*$('#dni').attr('required', 'required');*/
            $('#phone_mobile').attr('required', 'required');



            $(document).ready(function(){

                $('#firstname').focusout(function(){
                    $('#error_form_firstname').html('');
                    correcto = true;
                    if( ($("#firstname").attr('value').length) < 2 )
                    {
                        $('#error_form_firstname').html('El nombre es muy corto');
                        correcto = false;
                    }
                });

                $('#lastname').focusout(function(){
                    $('#error_form_lastname').html('');
                    correcto = true;
                    if( ($("#lastname").attr('value').length) < 2 )
                    {
                        $('#error_form_lastname').html('El apellido es muy corto');
                        correcto = false;
                    }
                });

                $('#address1').focusout(function(){
                    $('#error_form_address1').html('');
                    correcto = true;
                    if( ($("#address1").attr('value').length) < 2 )
                    {
                        $('#error_form_address1').html('La dirección es obligatoria');
                        correcto = false;
                    }
                });

                $('#postcode').focusout(function(){
                    $('#error_form_postcode').html('');
                    //if( ($("#postcode").attr('value').length) != 5 || isNaN($('#postcode').attr('value')))
                    if( ($("#postcode").attr('value').length) != 5 || isNaN($('#postcode').attr('value')) )
                    {
                        $('#error_form_postcode').html('El código postal es incorrecto. Formato: 00000');
                    }
                });

                $('#city').focusout(function(){
                    $('#error_form_city').html('');
                    correcto = true;
                    if( ($("#city").attr('value').length) < 2 )
                    {
                        $('#error_form_city').html('Introduzca una población');
                        correcto = false;
                    }
                });

                $('#id_state').focusout(function(){
                    $('#error_form_id_state').html('');
                    correcto = true;
                    if( !($("#id_state").attr('value')))
                    {
                        $('#error_form_id_state').html('Introduzca una provincia');
                        correcto = false;
                    }
                });

                /*$('#dni').focusout(function(){
                    $('#error_form_dni').html('');
                    correcto = true;
                    if( ($("#dni").attr('value').length) < 8 || ($("#dni").attr('value').length) > 14 )
                    {
                        $('#error_form_dni').html('Formato incorrecto');
                        correcto = false;
                    }
                });*/

                $('#phone_mobile').focusout(function(){
                    $('#error_form_phone_mobile').html('');
                    correcto = true;
                    if( ($("#phone_mobile").attr('value').length) != 9 || isNaN($('#phone_mobile').attr('value')) )
                    {
                        $('#error_form_phone_mobile').html('Formato incorrecto (Utilice sólo números, sin espacios)');
                        correcto = false;
                    }
                });

                $('#guest_email').focusout(function(){
                    $('#error_form_guest_email').html('');
                    if($("#guest_email").val().indexOf('@', 0) == -1 || $("#guest_email").val().indexOf('.', 0) == -1) {
                        $('#error_form_guest_email').html('E-mail incorrecto');
                    }
                });

            });
        </script>
        {literal}
        <!--<script type="text/javascript">/* <![CDATA[ */var google_conversion_id=967619414;var google_custom_params=window.google_tag_params;var google_remarketing_only=true;/* ]]> */</script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">/* <![CDATA[ *//* ]]> */</script>
        <noscript>
            <div style="display:inline;">
                <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/967619414/?value=0&amp;guid=ON&amp;script=0"/>
            </div>
        </noscript> -->
        <script async>
            $.ajax({dataType: "jsonp",url: "https://trustivity.es/api/tr/widget_gen/audioactive.es",type: 'GET',jsonp: 'callback',success: function(jsonp) {
               $(".trustivity_widget").html(jsonp.mensaje);
               $('#tab_trust_boton img').attr('src','../themes/leodig/img/tab_buttonW_vertical_invertido.png');
               /* $('#tab_trust_boton').html('<img src="//img/tab_buttonW_vertical_invertido.png">'); */
            }});

        </script>
        <div class="trustivity_widget"></div>

        <!-- Google Code para etiquetas de remarketing -->
        <!--------------------------------------------------
        Es posible que las etiquetas de remarketing todavma no estin asociadas a la informacisn de identificacisn personal o que estin en paginas relacionadas con las categormas delicadas. Para obtener mas informacisn e instrucciones sobre csmo configurar la etiqueta, consulte http://google.com/ads/remarketingsetup.
        --------------------------------------------------->
        <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 967619414;
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
            <div style="display:inline;">
                <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/967619414/?value=0&amp;guid=ON&amp;script=0"/>
            </div>
        </noscript>
        {/literal}
	</body>
</html>

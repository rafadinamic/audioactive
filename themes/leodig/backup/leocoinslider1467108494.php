<?php
/* Data sample for moduleleocoinslider*/
$query = "DROP TABLE IF EXISTS `_DB_PREFIX_leocoinslider`;
CREATE TABLE `ps_leocoinslider` (
  `id_leocoinslider_slides` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_leocoinslider_slides`,`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_leocoinslider` VALUES
('1','LEO_ID_SHOP'),
('2','LEO_ID_SHOP'),
('3','LEO_ID_SHOP'),
('4','LEO_ID_SHOP');
DROP TABLE IF EXISTS `_DB_PREFIX_leocoinslider_slides`;
CREATE TABLE `ps_leocoinslider_slides` (
  `id_leocoinslider_slides` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_leocoinslider_slides`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_leocoinslider_slides` VALUES
('1','2','1'),
('2','3','1'),
('3','1','1'),
('4','0','1');
DROP TABLE IF EXISTS `_DB_PREFIX_leocoinslider_slides_lang`;
CREATE TABLE `ps_leocoinslider_slides_lang` (
  `id_leocoinslider_slides` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `legend` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id_leocoinslider_slides`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
$dataLang = Array("es"=>"INSERT INTO `_DB_PREFIX_leocoinslider_slides_lang` VALUES
('1','LEO_ID_LANGUAGE','Rejuvenvece tu sistema auditivo','Rejuvenvece tu sistema auditivo','Rejuvenvece tu sistema auditivo','#','ba5a7b4092993a5e083cf4c7e3d49852.jpg'),
('2','LEO_ID_LANGUAGE','Promo Lanzamiento 50%','Promoción Lanzamiento','Promoción Lanzamiento','6-audifonos','bcf8c8198b8e293f3341b5443b8f0a3e.jpg'),
('3','LEO_ID_LANGUAGE','Promoción en teléfonos','','Promoción en teléfonos','7-telefonos','b2d74aa1ec8df368457b7abb22d89158.jpg'),
('4','LEO_ID_LANGUAGE','Amplia tus sentidos','Amplia tus sentidos','Amplia tus sentidos','#','8ae05485978013737e57249926ecc53a.jpg');
");
<?php
/* Data sample for moduleblockadvfooter*/
$query = "DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block`;
CREATE TABLE `ps_loffc_block` (
  `id_loffc_block` int(11) NOT NULL AUTO_INCREMENT,
  `width` float(10,2) NOT NULL,
  `show_title` tinyint(1) NOT NULL,
  `id_position` tinyint(2) NOT NULL,
  PRIMARY KEY (`id_loffc_block`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block` VALUES
('5','75.00','0','1'),
('6','25.00','0','1'),
('7','50.00','0','3'),
('8','20.00','0','3'),
('9','30.00','0','3'),
('10','80.00','0','4'),
('11','20.00','0','4');
DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block_item`;
CREATE TABLE `ps_loffc_block_item` (
  `id_loffc_block_item` int(11) NOT NULL AUTO_INCREMENT,
  `id_loffc_block` int(11) NOT NULL,
  `type` varchar(25) NOT NULL,
  `link` varchar(2000) NOT NULL,
  `linktype` varchar(25) NOT NULL,
  `link_content` varchar(2000) NOT NULL,
  `module_name` varchar(100) NOT NULL,
  `hook_name` varchar(100) NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `addthis` tinyint(1) NOT NULL,
  `show_title` tinyint(1) NOT NULL DEFAULT '1',
  `target` varchar(20) NOT NULL DEFAULT '_self',
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id_loffc_block_item`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block_item` VALUES
('34','5','module','','','','blocknewsletter','displayfooter','','','0','0','','0'),
('35','6','custom_html','','','','','','','','0','0','','0'),
('36','8','link','','cms','4','','','','','0','1','_self','0'),
('37','8','link','','cms','1','','','','','0','1','_self','2'),
('38','8','link','','cms','6','','','','','0','1','_self','3'),
('39','8','link','','cms','2','','','','','0','1','_self','4'),
('40','8','link','','cms','3','','','','','0','1','_self','5'),
('41','7','custom_html','','','','','','','','0','0','','0'),
('42','10','custom_html','','','','','','','','0','0','','0'),
('43','8','link','','link','http://www.audioactive.es/contactenos','','','','','0','1','_self','0'),
('44','11','custom_html','','','','','','','','0','0','','0'),
('45','9','custom_html','','','','','','','','0','0','','0');
DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block_item_lang`;
CREATE TABLE `ps_loffc_block_item_lang` (
  `id_loffc_block_item` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id_loffc_block_item`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block_item_shop`;
CREATE TABLE `ps_loffc_block_item_shop` (
  `id_loffc_block_item` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_loffc_block_item`,`id_shop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block_item_shop` VALUES
('34','LEO_ID_SHOP'),
('35','LEO_ID_SHOP'),
('36','LEO_ID_SHOP'),
('37','LEO_ID_SHOP'),
('38','LEO_ID_SHOP'),
('39','LEO_ID_SHOP'),
('40','LEO_ID_SHOP'),
('41','LEO_ID_SHOP'),
('42','LEO_ID_SHOP'),
('43','LEO_ID_SHOP'),
('44','LEO_ID_SHOP'),
('45','LEO_ID_SHOP');
DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block_lang`;
CREATE TABLE `ps_loffc_block_lang` (
  `id_loffc_block` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id_loffc_block`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block_shop`;
CREATE TABLE `ps_loffc_block_shop` (
  `id_loffc_block` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_loffc_block`,`id_shop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block_shop` VALUES
('5','LEO_ID_SHOP'),
('6','LEO_ID_SHOP'),
('7','LEO_ID_SHOP'),
('8','LEO_ID_SHOP'),
('9','LEO_ID_SHOP'),
('10','LEO_ID_SHOP'),
('11','LEO_ID_SHOP');
";
$dataLang = Array("es"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('34','LEO_ID_LANGUAGE','newsletter_nuevo_modulo',''),
('35','LEO_ID_LANGUAGE','gplus','<p class=\"siguenos\"><a href=\"https://plus.google.com/102073527271724233857\" target=\"_blank\">Síguenos <span>en Google Plus</span></a></p>'),
('36','LEO_ID_LANGUAGE','Nosotros',''),
('37','LEO_ID_LANGUAGE','Condiciones de Envío',''),
('38','LEO_ID_LANGUAGE','Condiciones de Devolución',''),
('39','LEO_ID_LANGUAGE','Aviso Legal',''),
('40','LEO_ID_LANGUAGE','Términos y Condiciones de Uso',''),
('41','LEO_ID_LANGUAGE','texto_empresa','<p><strong>AMPLIFICADORES, ACCESORIOS PARA AUDIÍFONOS, TELÉFONOS Y APARATOS PARA SORDOS</strong></p>\n<p><br>En Audioactive disponemos de accesorios de audífonos más baratos, teléfonos para  mayores, despertadores, auriculares, timbres y otros aparatos para personas con sordera o dificultades visuales. Todo lo necesario para oir mejor, amplificar el sonido y su nitidez en nuestra tienda online. Con marcas de prestifio como Senheiser, Rayovac o Doro entre otras.</p>'),
('42','LEO_ID_LANGUAGE','derechos','<p>Audioactive 2016. Todos los derechos reservados.</p>'),
('43','LEO_ID_LANGUAGE','Contacto',''),
('44','LEO_ID_LANGUAGE','dinamiclink','<p><a class=\"dinamic\" href=\"http://www.dinamicbrain.com\" target=\"_blank\">dinamicbrain</a></p>'),
('45','LEO_ID_LANGUAGE','tarjetas','<div class=\"metodos\">\r\n<p>Métodos de Pago</p>\r\n<ul>\r\n<li class=\"tarjv\">VISA</li>\r\n<li class=\"tarjm\">MASTERCARD</li>\r\n<li class=\"tarja\">AMERICAN EXPRESS</li>\r\n<li class=\"tarjp\">PAYPAL</li>\r\n</ul>\r\n</div>');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('5','LEO_ID_LANGUAGE','newsletter_nuevo'),
('6','LEO_ID_LANGUAGE','siguenos_gplus'),
('7','LEO_ID_LANGUAGE','descripcion'),
('8','LEO_ID_LANGUAGE','enlaces'),
('9','LEO_ID_LANGUAGE','metodos'),
('10','LEO_ID_LANGUAGE','copyright'),
('11','LEO_ID_LANGUAGE','dinamic');
");
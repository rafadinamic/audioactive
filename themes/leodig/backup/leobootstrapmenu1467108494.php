<?php
/* Data sample for moduleleobootstrapmenu*/
$query = "DROP TABLE IF EXISTS `_DB_PREFIX_btmegamenu`;
CREATE TABLE `ps_btmegamenu` (
  `id_btmegamenu` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `id_parent` int(11) NOT NULL,
  `is_group` tinyint(1) NOT NULL,
  `width` varchar(255) DEFAULT NULL,
  `submenu_width` varchar(255) DEFAULT NULL,
  `colum_width` varchar(255) DEFAULT NULL,
  `submenu_colum_width` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `colums` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `is_content` tinyint(1) NOT NULL,
  `show_title` tinyint(1) NOT NULL,
  `type_submenu` varchar(10) NOT NULL,
  `level_depth` smallint(6) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `show_sub` tinyint(1) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `target` varchar(25) DEFAULT NULL,
  `privacy` smallint(6) DEFAULT NULL,
  `position_type` varchar(25) DEFAULT NULL,
  `menu_class` varchar(25) DEFAULT NULL,
  `content` text,
  `submenu_content` text,
  `level` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `right` int(11) NOT NULL,
  `submenu_catids` text,
  `is_cattree` tinyint(1) DEFAULT '1',
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_btmegamenu`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_btmegamenu` VALUES
('1','','0','1',NULL,NULL,NULL,NULL,NULL,'1','','0','0','','0','0','0','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0','0','0',NULL,'1',NULL,NULL),
('2','','1','0',NULL,NULL,NULL,NULL,NULL,'1','url','0','1','menu','1','1','0','0','index','_self','0',NULL,NULL,NULL,NULL,'0','0','0',NULL,'1','2013-11-12 02:04:50','2014-08-20 12:36:40'),
('3','','1','0',NULL,NULL,NULL,NULL,'2','4','url','0','1','menu','1','1','2','0','http://www.audioactive.es/aparatos-auditivos-sordos_14','_self','0',NULL,'fullwidth',NULL,NULL,'0','0','0',NULL,'1','2013-11-12 02:11:38','2014-11-20 16:21:08'),
('6','','26','1',NULL,NULL,NULL,NULL,'8','1','category','0','0','html','3','1','0','0','#','_self','0',NULL,NULL,NULL,NULL,'0','0','0',NULL,'1','2013-11-12 02:16:36','2014-11-20 16:35:10'),
('9','','1','0',NULL,NULL,NULL,NULL,NULL,'1','url','0','1','menu','1','1','3','0','contact','_self','0',NULL,NULL,NULL,NULL,'0','0','0',NULL,'1','2013-11-14 21:58:35','2014-07-01 18:38:58'),
('11','','3','1',NULL,NULL,NULL,NULL,'6','1','category','0','1','menu','2','1','0','0','http://adoraserver:8888/audioactive/htdocs/audifonos_6','_self','0',NULL,NULL,NULL,NULL,'0','0','0',NULL,'1','2013-11-29 03:35:11','2016-03-01 13:23:10'),
('25','','3','1',NULL,NULL,NULL,NULL,'7','1','category','0','1','menu','2','1','1','0','#','_self','0',NULL,NULL,NULL,NULL,'0','0','0',NULL,'1','2013-12-03 05:09:28','2014-08-14 09:38:19'),
('26','','3','1',NULL,NULL,NULL,NULL,'8','1','category','0','1','menu','2','1','2','0','#','_self','0',NULL,NULL,NULL,NULL,'0','0','0',NULL,'1','2013-12-03 05:15:02','2014-08-13 09:12:05'),
('35','','11','1',NULL,NULL,NULL,NULL,'6','1','category','0','0','html','3','1','0','0','#','_self','0',NULL,NULL,NULL,NULL,'0','0','0',NULL,'1','2014-07-01 18:46:28','2014-11-20 16:30:17'),
('36','','25','1',NULL,NULL,NULL,NULL,'7','1','category','0','0','html','3','1','0','0','#','_self','0',NULL,NULL,NULL,NULL,'0','0','0',NULL,'1','2014-07-02 08:29:49','2014-11-20 16:32:48'),
('37','','3','1',NULL,NULL,NULL,NULL,NULL,'1','url','0','1','html','2','1','3','0','#','_self','0',NULL,'other-menu',NULL,NULL,'0','0','0','9,10,11,12','1','2014-07-02 08:36:42','2014-11-20 16:34:31'),
('42','','1','0',NULL,NULL,NULL,NULL,NULL,'1','html','0','1','menu','1','1','4','0','#','_self','0',NULL,'tlf-menu',NULL,NULL,'0','0','0',NULL,'1','2014-07-02 09:06:37','2014-08-19 14:42:00'),
('43','','1','0',NULL,NULL,NULL,NULL,'4','1','cms','0','1','menu','1','1','1','0',NULL,'_self','0',NULL,NULL,NULL,NULL,'0','0','0',NULL,'1','2014-08-20 09:58:23','2014-08-20 09:58:23');
DROP TABLE IF EXISTS `_DB_PREFIX_btmegamenu_lang`;
CREATE TABLE `ps_btmegamenu_lang` (
  `id_btmegamenu` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `content_text` text,
  `submenu_content_text` text,
  PRIMARY KEY (`id_btmegamenu`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `_DB_PREFIX_btmegamenu_shop`;
CREATE TABLE `ps_btmegamenu_shop` (
  `id_btmegamenu` int(11) NOT NULL DEFAULT '0',
  `id_shop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_btmegamenu`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_btmegamenu_shop` VALUES
('1','LEO_ID_SHOP'),
('2','LEO_ID_SHOP'),
('3','LEO_ID_SHOP'),
('6','LEO_ID_SHOP'),
('9','LEO_ID_SHOP'),
('11','LEO_ID_SHOP'),
('25','LEO_ID_SHOP'),
('26','LEO_ID_SHOP'),
('35','LEO_ID_SHOP'),
('36','LEO_ID_SHOP'),
('37','LEO_ID_SHOP'),
('42','LEO_ID_SHOP'),
('43','LEO_ID_SHOP');
";
$dataLang = Array("es"=>"INSERT INTO `_DB_PREFIX_btmegamenu_lang` VALUES
('1','LEO_ID_LANGUAGE','Root',NULL,NULL,NULL),
('2','LEO_ID_LANGUAGE','inicio',NULL,'<p><a href=\"/\"><em class=\"icon-clock-streamline-time\"></em>Inicio</a></p>',NULL),
('3','LEO_ID_LANGUAGE','Productos',NULL,NULL,NULL),
('6','LEO_ID_LANGUAGE','Auriculares img',NULL,NULL,'<p><a href=\"http://www.audioactive.es/auriculares-tv_8\"><img class=\"img-responsive\" src=\"/modules/leobootstrapmenu/image/auriculares-tv-menu.jpg\" alt=\"Auriculares para sordos\" width=\"250\" height=\"160\" /></a></p>'),
('9','LEO_ID_LANGUAGE','Contacto',NULL,NULL,NULL),
('11','LEO_ID_LANGUAGE','Soluciones Auditivas',NULL,NULL,'<p><img class=\"img-responsive\" src=\"/audioactive/htdocs/modules/leobootstrapmenu/image/adv-1.png\" alt=\"\" /></p>'),
('25','LEO_ID_LANGUAGE','Teléfonos',NULL,NULL,'<p><img class=\"img-responsive\" src=\"/audioactive/htdocs/modules/leobootstrapmenu/image/adv-1.png\" alt=\"\" /></p>'),
('26','LEO_ID_LANGUAGE','Auriculares TV',NULL,NULL,NULL),
('35','LEO_ID_LANGUAGE','Audífonos img',NULL,NULL,'<p><a title=\"Audifonos digitales para sordos\" href=\"http://www.audioactive.es/audifonos-digitales_6\"><img class=\"img-responsive\" src=\"/modules/leobootstrapmenu/image/audifonos-menu.jpg\" alt=\"Audífonos digitales\" width=\"250\" height=\"160\" /></a></p>'),
('36','LEO_ID_LANGUAGE','Teléfono img',NULL,NULL,'<p><a href=\"http://www.audioactive.es/telefonos-mayores_7\"><img class=\"img-responsive\" src=\"/modules/leobootstrapmenu/image/telefonos-menu.jpg\" alt=\"Teléfonos para sordos\" width=\"250\" height=\"160\" /></a></p>'),
('37','LEO_ID_LANGUAGE','Otros',NULL,NULL,'<ul class=\"iconos-menu\">\r\n<li><a href=\"despertadores_9\"><em class=\"icon-clock-streamline-time\"></em>Despertadores</a></li>\r\n<li><a href=\"pilas-audifonos_11\"><em class=\"icon-add-lista\"></em>Pilas audífonos</a></li>\r\n<li><a href=\"limpieza-audifonos_12\"><em class=\"icon-first-aid-medecine-shield-streamline\"></em>Limpieza audífonos</a></li>\r\n</ul>'),
('42','LEO_ID_LANGUAGE','966 115 079',NULL,NULL,NULL),
('43','LEO_ID_LANGUAGE','Nosotros',NULL,NULL,NULL);
");
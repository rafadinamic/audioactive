<?php
/* Data sample for moduleblockadvfooter*/
$query = "DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block`;
CREATE TABLE `ps_loffc_block` (
  `id_loffc_block` int(11) NOT NULL AUTO_INCREMENT,
  `width` float(10,2) NOT NULL,
  `show_title` tinyint(1) NOT NULL,
  `id_position` tinyint(2) NOT NULL,
  PRIMARY KEY (`id_loffc_block`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block` VALUES
('1','25.00','1','2'),
('2','25.00','1','2'),
('3','25.00','0','2'),
('4','25.00','1','2');
DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block_item`;
CREATE TABLE `ps_loffc_block_item` (
  `id_loffc_block_item` int(11) NOT NULL AUTO_INCREMENT,
  `id_loffc_block` int(11) NOT NULL,
  `type` varchar(25) NOT NULL,
  `link` varchar(2000) NOT NULL,
  `linktype` varchar(25) NOT NULL,
  `link_content` varchar(2000) NOT NULL,
  `module_name` varchar(100) NOT NULL,
  `hook_name` varchar(100) NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `addthis` tinyint(1) NOT NULL,
  `show_title` tinyint(1) NOT NULL DEFAULT '1',
  `target` varchar(20) NOT NULL DEFAULT '_self',
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id_loffc_block_item`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block_item` VALUES
('1','1','link','','link','history','','','','','0','1','','0'),
('14','1','link','','link','order-slip','','','','','0','1','_self','0'),
('15','1','link','','link','addresses','','','','','0','1','_self','0'),
('16','1','link','','link','identity','','','','','0','1','_self','0'),
('17','1','link','','link','discount','','','','','0','1','_self','0'),
('18','2','link','','cms','1','','','','','0','1','_self','0'),
('19','2','link','','cms','2','','','','','0','1','_self','3'),
('20','2','link','','cms','3','','','','','0','1','_self','2'),
('29','3','module','','','','blockcontactinfos','displayfooter','','','0','0','','0'),
('30','4','module','','','','blocknewsletter','displayfooter','','','0','0','','0'),
('31','2','link','','cms','6','','','','','0','1','_self','1'),
('33','3','custom_html','','','','','','','','0','1','','0');
DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block_item_lang`;
CREATE TABLE `ps_loffc_block_item_lang` (
  `id_loffc_block_item` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id_loffc_block_item`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block_item_shop`;
CREATE TABLE `ps_loffc_block_item_shop` (
  `id_loffc_block_item` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_loffc_block_item`,`id_shop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block_item_shop` VALUES
('1','LEO_ID_SHOP'),
('14','LEO_ID_SHOP'),
('15','LEO_ID_SHOP'),
('16','LEO_ID_SHOP'),
('17','LEO_ID_SHOP'),
('18','LEO_ID_SHOP'),
('19','LEO_ID_SHOP'),
('20','LEO_ID_SHOP'),
('29','LEO_ID_SHOP'),
('30','LEO_ID_SHOP'),
('31','LEO_ID_SHOP'),
('33','LEO_ID_SHOP');
DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block_lang`;
CREATE TABLE `ps_loffc_block_lang` (
  `id_loffc_block` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id_loffc_block`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `_DB_PREFIX_loffc_block_shop`;
CREATE TABLE `ps_loffc_block_shop` (
  `id_loffc_block` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_loffc_block`,`id_shop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block_shop` VALUES
('1','LEO_ID_SHOP'),
('2','LEO_ID_SHOP'),
('3','LEO_ID_SHOP'),
('4','LEO_ID_SHOP');
";
$dataLang = Array("es"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('1','LEO_ID_LANGUAGE','Mis pedidos',''),
('14','LEO_ID_LANGUAGE','Mis hojas de crédito',''),
('15','LEO_ID_LANGUAGE','Mis direcciones',''),
('16','LEO_ID_LANGUAGE','Mis datos personales',''),
('17','LEO_ID_LANGUAGE','Mis vales',''),
('18','LEO_ID_LANGUAGE','Condiciones de envío',''),
('19','LEO_ID_LANGUAGE','Aviso Legal',''),
('20','LEO_ID_LANGUAGE','Términos y condiciones de uso',''),
('29','LEO_ID_LANGUAGE','Contacta con nosotros',''),
('30','LEO_ID_LANGUAGE','newsletter',''),
('31','LEO_ID_LANGUAGE','Condiciones de devolución',''),
('33','LEO_ID_LANGUAGE','Síguenos en','<p style=\"margin-top: 10px;\"><a href=\"https://plus.google.com/102073527271724233857\" rel=\"publisher\" target=\"_blank\"><em class=\"icon icon-google-plus\"></em> Google+</a></p>');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('1','LEO_ID_LANGUAGE','Mi cuenta'),
('2','LEO_ID_LANGUAGE','Información'),
('3','LEO_ID_LANGUAGE','Contacta con nosotros'),
('4','LEO_ID_LANGUAGE','Newsletter');
");
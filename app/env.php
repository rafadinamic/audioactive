<?php
if(! defined('APP_SERVER_ENV'))
{
    define('APP_SERVER_ENV', getenv('APP_ENV')
        ? getenv('APP_ENV')
        : (/**/getenv('HTTP_APP_ENV') ? getenv('HTTP_APP_ENV') : 'prod'));
}

switch(APP_SERVER_ENV)
{
    default:
        $_debug = false;
        break;
    case 'prod-dbg':
//    $_env = 'prod';
        $_debug = true;
        break;
    case 'dev-portatil':
        $_env = 'dev';
    case 'dev':
        $_debug = true;
        umask(0000); // This will let the permissions be 0777
}
define('APP_ENV', isset($_env) ? $_env : APP_SERVER_ENV);
define('APP_DEBUG', $_debug);

if($_debug)
{
    if(!in_array(substr(PHP_SAPI, 0, 3), array('cli','cgi')) && (
            isset($_SERVER['HTTP_CLIENT_IP']) || isset($_SERVER['HTTP_X_FORWARDED_FOR']) ||
//            !preg_match('/^(127\.0\.0\.1|::1|192\.168\.[0-9]{1,3}\.[0-9]{1,3}|84\.127\.227\.119|62\.175\.241\.2)$/
            !preg_match('/^(127\.0\.0\.1|::1|192\.168\.[0-9]{1,3}\.[0-9]{1,3}|)95\.39\.225\.181$/', @$_SERVER['REMOTE_ADDR'])))
    {
        header('HTTP/1.0 403 Forbidden', true, 403);
        exit('You are not allowed to access this file.');
    }

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
}
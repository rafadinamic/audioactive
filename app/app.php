<?php
/**
 * User: David Moya de la Ossa <david.moya@adoramedia.com>
 * Date: 26/07/14
 * Time: 17:55
 */
$_appDir =  __DIR__;

// Environment
include $_appDir . '/env.php';

//// Autoloader
//$_autoloader = include $_appDir . '/vendor/autoload.php';
//
// Configuración
$_config = include $_appDir.'/etc/'.APP_ENV.'.php';
return $_config;
//
//// Applicatin
//$app = new \AM\Application($_autoloader, $_config);
//\AM\Application::setInstance($app);
//
//return $app;

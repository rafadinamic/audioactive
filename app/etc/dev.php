<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 19/11/14
 * Time: 11:50
 */
$_config = include __DIR__ . '/prod.php';

$_configDev = array(
    'db.log' => true,
    'debug' => false,
    'db.options' => array(
        'host' => 'localhost',
        'dbname' => 'audioactive',
        'user' => 'user_audioactive',
        'password' => 'JKeRpsd6npwXLuYpk4XA',
    )
);

return array_replace_recursive($_config, $_configDev);

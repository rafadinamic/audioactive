<?php
/**
 * @author: idealo Internet GmbH http://www.idealo.eu
 * @copyright 2017 idealo Internet GmbH
 * @license Apache License 2.0 - see LICENSE file
 *
 * please read DISCLAIMER, LICENSE and README.md
 */
require_once('./classes/PrepareEnvironment.php');

PrepareEnvironment::init();
IdealoController::run();

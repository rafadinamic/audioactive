<?php
/**
 * @author: idealo Internet GmbH http://www.idealo.eu
 * @copyright 2017 idealo Internet GmbH
 * @license Apache License 2.0 - see LICENSE file
 *
 * please read DISCLAIMER, LICENSE and README.md
 */

/**
 * change on your own risk, all options should work well in a standard PrestaShop installation
 */
return array(
    // base path relative fom PrepareEnvironment.php
    'ps_base_path' => '/../../',

    // [CSV_SETTINGS]
    'csv_field_delimiter' => ",",
   /*
   possible line delimiter values in quotes
    \n (preferred)
    \r\n
    \r
   */
    'csv_line_delimiter' => "\n",
    'csv_quote_char' => "\""
);

# idealo Prestashop CSV Export
## introduction
This is an export tool for PrestaShop installations. It delivers CSV's for all active product's in the shop system.

## installation instruction  
- unpack/move the idealo folder from prestashop-export-{version}.zip in the root folder of your PrestaShop installation
- adjust the config.php located in /idealo/conf/config.php for your needs (default values should be good for the most of the installations)
- that's it

## features
- exports plain products including prices with vat's
- exports variants with deep links [use it {your-domain.tld/prestashop-path}/idealo/?variants=on ]

## known issues
- did not export delivery prices  

## requirements
- PHP >= 5.3
- PrestaShop >= 1.6.x
    see prestashop requirements
    http://doc.prestashop.com/display/PS16/What+you+need+to+get+started

## contact information 
If you have any questions how to use this script or you need help for installation please contact tam@idealo.de

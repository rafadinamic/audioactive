<?php
/**
 * @author: idealo Internet GmbH http://www.idealo.eu
 * @copyright 2017 idealo Internet GmbH
 * @license Apache License 2.0 - see LICENSE file
 *
 * please read DISCLAIMER, LICENSE and README.md
 */
class IdealoController
{
    private static $prestashopExportVersion;
    private static $phpversion;

    public static function run()
    {
        self::init();

        if(Options::isSystemInfo()) {
            self::printSystemInfo();
            exit();
        }

        self::generateCsv();
    }

    private static function init() {
        self::$prestashopExportVersion = file_get_contents(dirname(__FILE__)."/../version");
        self::$phpversion = phpversion();
    }

    private static function addVersionInformationToHeader() {
        header("Content-Type: application/csv; charset=utf-8", true);
        header("X-IdealoPrestaShopExport: " . self::$prestashopExportVersion);
        header("X-PhpVersion: " . self::$phpversion);
        header("X-PrestaShopVersion: " . _PS_VERSION_);
    }

    private static function generateCsv()
    {
        try {
            TestRequirements::run();
            self::addVersionInformationToHeader();
            $csvGenerator = new CsvGenerator();
            $csvGenerator->generate();
        } catch (Error $e) {
            if (Options::displayErrors()) {
                echo "<pre>";
                var_dump($e);
                echo "</pre>";
            }
        }
    }

    private static function printSystemInfo()
    {
        echo "PrestaShop-Version: " . _PS_VERSION_ . "<br>";
        echo "IdealoPrestaShopExport: " . self::$prestashopExportVersion . "<br>";
        phpinfo();
    }
}

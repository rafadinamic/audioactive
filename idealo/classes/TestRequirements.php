<?php
/**
 * @author: idealo Internet GmbH http://www.idealo.eu
 * @copyright 2017 idealo Internet GmbH
 * @license Apache License 2.0 - see LICENSE file
 *
 * please read DISCLAIMER, LICENSE and README.md
 */

class TestRequirements
{
    const MIN_PS_VERSION = "1.5.0.0";
    const MIN_PHP_VERSION = '5.3.0';

    static function run() {
        self::checkPhpVersion();
        self::verifyPrestaShopVersion();
    }

    private static function verifyPrestaShopVersion()
    {
        if(version_compare(_PS_VERSION_, self::MIN_PS_VERSION, "<=")) {
            exit("Your PrestaShop Version (" . _PS_VERSION_ . ") is too old and not supported by this script. At least version ". self::MIN_PS_VERSION . " is required.");
        }
    }

    private static function checkPhpVersion()
    {
        if (version_compare(phpversion(), self::MIN_PHP_VERSION, '<=')) {
            exit("Your PHP Version (".phpversion().") is to low and not supported by this script. At least version ".self::MIN_PHP_VERSION."is required.");
        }
    }
}
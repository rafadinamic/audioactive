<?php
/**
 * @author: idealo Internet GmbH http://www.idealo.eu
 * @copyright 2017 idealo Internet GmbH
 * @license Apache License 2.0 - see LICENSE file
 *
 * please read DISCLAIMER, LICENSE and README.md
 */
require_once 'IdealoConfig.php';

class PrepareEnvironment
{
    static function init() {
        self::deactivateApacheGzip();
        self::initPrestaShop();
        self::registerIdealoAutoloader();
    }

    private static function registerIdealoAutoloader()
    {
        spl_autoload_register('idealo_autoload', true, false);
    }

    private static function initPrestaShop()
    {
        $conf = IdealoConfig::getInstance();
        $basepath = dirname(__FILE__) . $conf->getConfig(IdealoConfig::PS_BASE_PATH);
        $filename = $basepath . 'init.php';
        if (!file_exists($filename)) {
            // TODO: error output and exit
            exit("Script is in the wrong path ({$filename})");
        } else {
            include($basepath . '/config/config.inc.php');
            require_once($filename);
        }
    }

    private static function deactivateApacheGzip()
    {
        @ini_set('zlib.output_compression', 'Off');
        @ini_set('output_buffering', 'Off');
        @ini_set('output_handler', '');
        @ini_set('implicit_flush', 1);
    }
}

function idealo_autoload($classname) {

    $file = dirname(__FILE__)."/".$classname.".php";
    if(file_exists($file)) {
        require_once $file;
    } else {
        echo $file . " not exists <br>";
    }
}
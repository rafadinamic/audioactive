<?php
/**
 * @author: idealo Internet GmbH http://www.idealo.eu
 * @copyright 2017 idealo Internet GmbH
 * @license Apache License 2.0 - see LICENSE file
 *
 * please read DISCLAIMER, LICENSE and README.md
 */
class Options
{

    public static function isSystemInfo()
    {
        return self::isOptionOn("systeminfo");
    }

    /**
     * @return bool
     */
    public static function withVariants()
    {
        return self::isOptionOn("variants");
    }

    public static function displayErrors()
    {
        return self::isOptionOn("errors");
    }

    /**
     * @param $string
     * @return bool
     */
    private static function isOptionOn($string)
    {
        return isset($_GET[$string]) && $_GET[$string] === "on";
    }
}
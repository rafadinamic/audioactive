<?php
/**
 * @author: idealo Internet GmbH http://www.idealo.eu
 * @copyright 2017 idealo Internet GmbH
 * @license Apache License 2.0 - see LICENSE file
 *
 * please read DISCLAIMER, LICENSE and README.md
 */
class CsvHeader
{
    /**
     * @var array the csv-header is generated in array-order as it put into the array
     */
    private static $headerArray = array(
        self::SKU, self::PRICE, self::UNIT_PRICE, self::TITLE, self::CATEGORY_PATH, self::URL, self::DESCRIPTION, self::EAN, self::HAN, self::MANUFACTURER,
        self::DELIVERY_TEXT, self::ATTRIBUTES, self::CONDITION, self::IMG_URL_1, self::IMG_URL_2, self::IMG_URL_3
    );

    const SKU = "sku";
    const EAN = "ean";
    const HAN = "han";
    const MANUFACTURER = "manufacturer";
    const TITLE = "title";
    const PRICE = "price";
    const DELIVERY_TEXT = "delivery_text";
    const CATEGORY_PATH = "category_path";
    const DESCRIPTION = "description";
    const ATTRIBUTES = "attributes";
    const CONDITION = "condition";
    const URL = "url";
    const UNIT_PRICE = "unit_price";
    const IMG_URL_1 = "img_url_1";
    const IMG_URL_2 = "img_url_2";
    const IMG_URL_3 = "img_url_3";

    public static function toString() {
        return implode(IdealoConfig::getInstance()->getConfig(IdealoConfig::CSV_FIELD_DELIMITER), self::$headerArray).IdealoConfig::getInstance()->getLineBreak();
    }

    public static function asArray() {
        return self::$headerArray;
    }
}
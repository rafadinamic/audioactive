<?php
/**
 * @author: idealo Internet GmbH http://www.idealo.eu
 * @copyright 2017 idealo Internet GmbH
 * @license Apache License 2.0 - see LICENSE file
 *
 * please read DISCLAIMER, LICENSE and README.md
 */
class CsvGenerator
{
    private static $conf;
    private static $protocol;
    private static $langId;
    private static $currency;

    function __construct()
    {
        $this->init();
    }

    private function init()
    {
        self::$protocol = (ConfigurationCore::get('PS_SSL_ENABLED') ? "https" : "http")."://";
        self::$conf = IdealoConfig::getInstance();
        self::$langId = ((int)ConfigurationCore::get('PS_LANG_DEFAULT')> 0) ? (int)ConfigurationCore::get('PS_LANG_DEFAULT') : 1;
        self::$currency = new CurrencyCore(ConfigurationCore::get('PS_CURRENCY_DEFAULT'));
    }

    function generate()
    {
        $statement = $this->getProductQuery();

        $activeCategories = CategoryCore::getCategories(self::$langId, true);

        echo CsvHeader::toString();

        $counter = 0;
        self::flush($counter, true);

        while ($nextProduct = Db::getInstance()->nextRow($statement)) {
            
            $productId = intval($nextProduct['id_product']);
            $product = new ProductCore($productId, true, self::$langId, null, null);
            $category = new CategoryCore($product->id_category_default, self::$langId);
            $link = new LinkCore();
            
            if (self::isInActiveCategory($product, $activeCategories) === false) {
                continue;
            }

            $id_variant = $this->getIdVariant($nextProduct);
            
            $csvLine = array();
            self::addToLine($csvLine, CsvHeader::SKU, self::generateSku($nextProduct));
            self::addToLine($csvLine, CsvHeader::PRICE, self::getPriceLocal($product->tax_rate, $product->price, $nextProduct));
            self::addToLine($csvLine, CsvHeader::UNIT_PRICE, $this->getUnitPrice($product, $nextProduct));
            self::addToLine($csvLine, CsvHeader::TITLE, ProductCore::getProductName($product->id, $id_variant, self::$langId));
            self::addToLine($csvLine, CsvHeader::CATEGORY_PATH, $this->getCategoryPath($category)); //, $product));
            self::addToLine($csvLine, CsvHeader::DESCRIPTION, $this->getDescription($product));
            self::addToLine($csvLine, CsvHeader::URL, $this->getUrl($product, $id_variant));
            self::addToLine($csvLine, CsvHeader::EAN, self::getEan($product, $nextProduct));
            self::addToLine($csvLine, CsvHeader::HAN, self::getHan($nextProduct));   // supplier reference is not a manufacture reference as we want but better as nothing
            self::addToLine($csvLine, CsvHeader::MANUFACTURER, $product->manufacturer_name);
            self::addToLine($csvLine, CsvHeader::DELIVERY_TEXT, $this->getDeliveryText($product));
            self::addToLine($csvLine, CsvHeader::ATTRIBUTES, $this->getProductAttributes($product, $id_variant));
            self::addToLine($csvLine, CsvHeader::CONDITION, $product->condition);
            self::setImgUrls($csvLine, $product, $link, $productId, $id_variant);

            $line = self::flatMapToCsvLine($csvLine);
            echo implode(self::$conf->getConfig(IdealoConfig::CSV_FIELD_DELIMITER), $line) . self::$conf->getLineBreak();

            self::flush($counter);
            
        }
    }

    private static function getHan($nextProduct)
    {
        if(isset($nextProduct['reference'])) {
            return $nextProduct['reference'];
        }
        return $nextProduct['reference'];
    }

    private static function flush(&$counter, $force = false)
    {
        if ($counter % 10 == 0 || $force === true) {
            ob_flush();
            flush();
        }
        $counter++;
    }

    private static function getEan(ProductCore $product, $nextProduct)
    {
        return empty($nextProduct["ean"]) ? $product->ean13 : $nextProduct["ean"];
    }

    /**
     * if $product->id_category_default is not in an active category this method returns false
     *
     * @param ProductCore $product
     * @param array $activeCategories
     * @return bool
     */
    private static function isInActiveCategory(ProductCore $product, array $activeCategories)
    {
        foreach ($activeCategories as $specificCategory) {
            foreach ($specificCategory as $category) {
                if ($product->id_category_default == $category["infos"]['id_category']) {
                    return true;
                }
            }
        }
        return false;
    }

    private function flatMapToCsvLine($csvLine)
    {
        $line = array();
        foreach (CsvHeader::asArray() as $column) {
            if (isset($csvLine[$column])) {
                $line[] = $this->encloseInQuotes($csvLine[$column]);
            } else {
                $line[] = null;
            }
        }
        return $line;
    }

    function addToLine(&$csv_line, $column, $value)
    {
        $csv_line[$column] = $value;
    }

    function encloseInQuotes($value)
    {
        $quote_char = self::$conf->getConfig(IdealoConfig::CSV_QUOTE_CHAR);
        return $quote_char . $this->escapeQuotingChar($value) . $quote_char;
    }

    function escapeQuotingChar($value)
    {
        $quote_char = self::$conf->getConfig(IdealoConfig::CSV_QUOTE_CHAR);
        return str_replace($quote_char, $quote_char . $quote_char, $value);
    }

    /**
     * @param $product
     * @return string
     */
    private function getDeliveryText($product)
    {
        if (empty($product->available_later)) {
            return $product->available_now;
        } else {
            return $product->available_later;
        }
    }

    /**
     * @param $category
     * @return string
     */
    private function getCategoryPath(CategoryCore $category) //, ProductCore $product)
    {
        $cats = array_reverse($category->getParentsCategories());
        return implode(array_column($cats, "name"), " > ");
    }

    /**
     * @param $product
     * @return string0.00
     */
    private function getDescription(ProductCore $product)
    {
        if (!empty($product->description_short)) {
            $desc = $product->description_short;
        } else {
            $desc = $product->description;
        }

        $desc = strip_tags($desc);
        $desc = str_replace("\n", " ", $desc);
        return $desc;
    }

    /**
     * @param $product
     * @param $id_variant
     * @return string
     */
    private function getProductAttributes(ProductCore $product, $id_variant = null)
    {
        $attributeSeparator = self::$conf->getConfig(IdealoConfig::CSV_FIELD_DELIMITER) == "|" ? " # " : " | ";
        $productFeatures = array();

        if(Options::withVariants() === true) {
            foreach ($product->getAttributeCombinationsById($id_variant, self::$langId) as $item) {
                $productFeatures[] = $item['group_name'] . ": " . $item['attribute_name'];
            }
        }
        foreach ($product->getFrontFeatures(self::$langId) as $item) {
            $productFeatures[] = $item['name'] . ": " . $item['value'];
        }
        return implode($attributeSeparator, $productFeatures);
    }

    private function getUnitPrice(ProductCore $product, $nextProduct)
    {
        if ($product->unit_price > 0 && !empty($product->unity)) {
            $unit_price_impact = ((isset($nextProduct['unit_price_impact']) === true) ? $nextProduct['unit_price_impact'] : 0);
            $unitPrice = ToolsCore::ps_round((($product->unit_price + $unit_price_impact) * ($product->tax_rate / 100 + 1)),2);
            return $unitPrice . " " . self::$currency->iso_code . "/" . $product->unity;
        } else {
            return null;
        }
    }

    /**
     * @param $csvLine
     * @param $product
     * @param $link
     * @param $productId
     * @param $id_variant
     */
    private function setImgUrls(&$csvLine, ProductCore $product, LinkCore $link, $productId, $id_variant)
    {
        $imageIds = array();
        if(Options::withVariants() === true) {
            $productImages = $product->getCombinationImages(self::$langId);
            if($productImages !== false) {
                foreach($productImages as $combinationImage) {
                    foreach($combinationImage as $value) {
                        if($value['id_product_attribute'] == $id_variant) {
                            $imageIds[] = $value['id_image'];
                        }
                    }
                }
            }
        } else {
            $imageIds = array_column($product->getImages(self::$langId), "id_image");
        }


        $images = array();
        foreach ($imageIds as $imageId) {
            $images[] = self::$protocol . $link->getImageLink($product->link_rewrite, $productId . "-" . $imageId);
        }

        $maxImages = 3;
        for ($i = 0; $i < $maxImages; $i++) {
            $img = isset($images[$i]) ? $images[$i] : null;
            switch ($i) {
                case 0:
                    self::addToLine($csvLine, CsvHeader::IMG_URL_1, $img);
                    break;
                case 1:
                    self::addToLine($csvLine, CsvHeader::IMG_URL_2, $img);
                    break;
                case 2:
                    self::addToLine($csvLine, CsvHeader::IMG_URL_3, $img);
                    break;
                default;
            }
        }
    }

    /**
     * @param $nextProduct
     * @return string
     */
    private static function generateSku($nextProduct)
    {
        $sku = $nextProduct['id_product'];
        if(Options::withVariants() === true) {
            $idVariant = self::getIdVariant($nextProduct);
            if(isset($idVariant)) {
                $sku .= "-" . self::getIdVariant($nextProduct);
            }
        }

        return $sku;
    }

    /**
     * @param $tax_rate
     * @param $price
     * @param $nextProduct
     * @return float
     */
    private function getPriceLocal($tax_rate, $price, $nextProduct) {
        $var = ((isset($nextProduct['variant_price']) === true) ? $nextProduct['variant_price'] : 0) + $price;
        $calculatedPrice = Tools::ps_round((($tax_rate * $var / 100) + $var),2);
        return $calculatedPrice;
    }

    /**
     * @param $nextProduct
     * @return mixed
     */
    private function getIdVariant($nextProduct)
    {
        if(isset($nextProduct['id_variant']))
        {
            return $nextProduct['id_variant'];
        }
        return null;
    }

    /**
     * @param ProductCore $product
     * @param $id_variant
     * @return mixed
     */
    private function getUrl(ProductCore $product, $id_variant)
    {
        if(Options::withVariants() === true && $id_variant !== null) {
            return $product->getLink().$product->getAnchor($id_variant, true);
        }
        return $product->getLink();
    }

    /**
     * @return bool|mysqli_result|PDOStatement|resource
     */
    private function getProductQuery()
    {
        Db::getInstance()->execute("SET NAMES 'utf8'", false);

        if (Options::withVariants() === false) {
            $activeProductsQuery = "SELECT p.id_product, p.reference AS reference, p.ean13 AS ean
                                    FROM
                                        " . _DB_PREFIX_ . "product p
                                        JOIN " . _DB_PREFIX_ . "stock_available sa ON (sa.id_product = p.id_product) AND sa.id_product_attribute = 0
                                    WHERE
                                        p.active = 1 AND p.available_for_order = 1 AND sa.quantity > 0";
        } else {
            $activeProductsQuery = "
                SELECT id_product, id_variant, ean, variant_price, unit_price_impact, 
	                   (case when variant_reference = \"\" then reference else variant_reference end) reference, 
		               group_concat(concat(attr_name, ':', attr_value)) variant_features
                FROM
                (
                    SELECT p.id_product, p.reference as reference, pa.reference as variant_reference, p.available_for_order, pa.id_product_attribute id_variant,
                           pa.price as variant_price, pa.unit_price_impact as unit_price_impact, pa.ean13 as ean, pac.id_product_attribute, agl.name attr_name, 
                           al.name attr_value, sap.quantity as sap_quantity, sav.quantity as sav_quantity
                        FROM " . _DB_PREFIX_ . "product p
                        JOIN " . _DB_PREFIX_ . "stock_available sap ON (sap.id_product = p.id_product) AND sap.id_product_attribute = 0
                        LEFT JOIN " . _DB_PREFIX_ . "product_attribute pa ON (p.id_product = pa.id_product) 
                        LEFT JOIN " . _DB_PREFIX_ . "product_attribute_combination pac ON (pa.id_product_attribute = pac.id_product_attribute)
                        LEFT JOIN " . _DB_PREFIX_ . "attribute a ON (a.id_attribute = pac.id_attribute)
                        LEFT JOIN " . _DB_PREFIX_ . "attribute_group_lang agl ON (a.id_attribute_group = agl.id_attribute_group AND agl.id_lang = 1)
                        LEFT JOIN " . _DB_PREFIX_ . "attribute_lang al ON (al.id_attribute = a.id_attribute)
                        LEFT JOIN " . _DB_PREFIX_ . "stock_available sav ON (sav.id_product = p.id_product) AND (sav.id_product_attribute = pa.id_product_attribute)
                    WHERE p.active = 1 AND p.available_for_order = 1
                ) temp WHERE (case when sav_quantity is null then sap_quantity else sav_quantity end) > 0
                GROUP BY id_product, id_variant, sap_quantity, sav_quantity";
        }

        $statement =  Db::getInstance()->query($activeProductsQuery);
        if (!$statement) {
            exit('Error on query execution: ' . $activeProductsQuery . '. Error: ' . Db::getInstance()->getMsgError() . ' (' . Db::getInstance()->getNumberError() . ')');
        }

        return $statement;
    }
}

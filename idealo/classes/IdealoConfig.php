<?php
/**
 * @author: idealo Internet GmbH http://www.idealo.eu
 * @copyright 2017 idealo Internet GmbH
 * @license Apache License 2.0 - see LICENSE file
 *
 * please read DISCLAIMER, LICENSE and README.md
 */
class IdealoConfig
{
    protected static $_instance = null;

    private static $config = array();

    const PS_BASE_PATH = "ps_base_path";

    const CSV_FIELD_DELIMITER = "csv_field_delimiter";
    const CSV_LINE_DELIMITER = "csv_line_delimiter";
    const CSV_QUOTE_CHAR = "csv_quote_char";

    protected function __clone() {}
    protected function __construct() {}

    public static function getInstance()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self;
            self::init();
        }
        return self::$_instance;
    }

    private static function init() {
        self::$config = require_once(dirname(__FILE__)."/../conf/config.php");
    }

    public function getConfig($name) {
        return self::$config[$name];
    }

    public function getLineBreak() {
        switch ($this->getConfig(IdealoConfig::CSV_LINE_DELIMITER)) {
            case "\n": return "\n";
            case "\n\r": return "\n\r";
            case "\r": return "\r";
            default: return "\n";
        }
    }
}
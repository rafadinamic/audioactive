<?php /* Smarty version Smarty-3.1.14, created on 2016-09-25 17:25:25
         compiled from "/var/www/vhosts/audioactive.es/httpdocs/modules/twenga/views/templates/admin/layout/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:176438235557e7ec65bf6557-42816538%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a42566ee181832451decfdf7229e1f102cff6c76' => 
    array (
      0 => '/var/www/vhosts/audioactive.es/httpdocs/modules/twenga/views/templates/admin/layout/header.tpl',
      1 => 1474817124,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '176438235557e7ec65bf6557-42816538',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'urlLogoHeader' => 0,
    '_basepath' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_57e7ec65c26708_39896543',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57e7ec65c26708_39896543')) {function content_57e7ec65c26708_39896543($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/var/www/vhosts/audioactive.es/httpdocs/tools/smarty/plugins/modifier.escape.php';
?>
<div class="tw-banner">
    <div>
        <a class="tw-logo" target="_blank"
           href="<?php $_smarty_tpl->smarty->_tag_stack[] = array('addUtm', array()); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['addUtm'][0][0]->addUtm(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['urlLogoHeader']->value, 'htmlall', 'UTF-8');?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['addUtm'][0][0]->addUtm(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
">
            <img src="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['_basepath']->value, 'htmlall', 'UTF-8');?>
/views/img/logo.png" alt="Twenga Solutions" class="img-responsive" width="259"
                 height="25"/>
        </a>

        <p class="banner-title"><?php $_smarty_tpl->smarty->_tag_stack[] = array('tr', array('_id'=>69507)); $_block_repeat=true; echo Twenga_Services_Lang::trans(array('_id'=>69507), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Référencez votre site Twenga<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Twenga_Services_Lang::trans(array('_id'=>69507), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
    </div>
    <ul class="tw-list row list-unstyled">
        <li class="col-sm-4">
            <div class="tw-def tw-def1">
                <strong><?php $_smarty_tpl->smarty->_tag_stack[] = array('tr', array('_id'=>69517)); $_block_repeat=true; echo Twenga_Services_Lang::trans(array('_id'=>69517), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Attirez de nouveau clients<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Twenga_Services_Lang::trans(array('_id'=>69517), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</strong><br/><?php $_smarty_tpl->smarty->_tag_stack[] = array('tr', array('_id'=>71967)); $_block_repeat=true; echo Twenga_Services_Lang::trans(array('_id'=>71967), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plus de 30 millions d'e-consommateurs chaque mois<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Twenga_Services_Lang::trans(array('_id'=>71967), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            </div>
        </li>
        <li class="col-sm-4">
            <div class="tw-def tw-def2">
                <strong><?php $_smarty_tpl->smarty->_tag_stack[] = array('tr', array('_id'=>71977)); $_block_repeat=true; echo Twenga_Services_Lang::trans(array('_id'=>71977), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Augmentez vos Ventes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Twenga_Services_Lang::trans(array('_id'=>71977), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</strong><br/><?php $_smarty_tpl->smarty->_tag_stack[] = array('tr', array('_id'=>71987)); $_block_repeat=true; echo Twenga_Services_Lang::trans(array('_id'=>71987), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Un trafic performant piloté pour convertir et maximiser votre revenu<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Twenga_Services_Lang::trans(array('_id'=>71987), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            </div>
        </li>
        <li class="col-sm-4">
            <div class="tw-def tw-def3">
                <strong><?php $_smarty_tpl->smarty->_tag_stack[] = array('tr', array('_id'=>71997)); $_block_repeat=true; echo Twenga_Services_Lang::trans(array('_id'=>71997), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Optimisez vos Coûts<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Twenga_Services_Lang::trans(array('_id'=>71997), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</strong><br/><?php $_smarty_tpl->smarty->_tag_stack[] = array('tr', array('_id'=>72007)); $_block_repeat=true; echo Twenga_Services_Lang::trans(array('_id'=>72007), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Des CPC dynamiques ajustés en temps réel selon le potentiel de conversion<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Twenga_Services_Lang::trans(array('_id'=>72007), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            </div>
        </li>
    </ul>
</div>
<?php }} ?>
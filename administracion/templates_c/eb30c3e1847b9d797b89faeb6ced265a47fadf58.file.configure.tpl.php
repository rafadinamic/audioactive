<?php /* Smarty version Smarty-3.1.14, created on 2016-09-25 17:25:25
         compiled from "/var/www/vhosts/audioactive.es/httpdocs/modules/twenga/views/templates/admin/configure.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11070604957e7ec65980a86-38299013%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'eb30c3e1847b9d797b89faeb6ced265a47fadf58' => 
    array (
      0 => '/var/www/vhosts/audioactive.es/httpdocs/modules/twenga/views/templates/admin/configure.tpl',
      1 => 1474817124,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11070604957e7ec65980a86-38299013',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'stepClass' => 0,
    'productStatus' => 0,
    'formSignUpUrl' => 0,
    'formLoginUrl' => 0,
    'lostPasswordUrl' => 0,
    'currentStepDone' => 0,
    'currentAccountType' => 0,
    'merchantInfo' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_57e7ec65b302d4_60312242',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57e7ec65b302d4_60312242')) {function content_57e7ec65b302d4_60312242($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/var/www/vhosts/audioactive.es/httpdocs/tools/smarty/plugins/modifier.escape.php';
?>
<div class="tw-install tw-box <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['stepClass']->value, 'htmlall', 'UTF-8');?>
 tw-onboarding-<?php echo mb_strtolower(smarty_modifier_escape($_smarty_tpl->tpl_vars['productStatus']->value, 'htmlall', 'UTF-8'), 'UTF-8');?>
" id="tw-step-container">
    <div class="tw-box-title tw-box-content">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('tr', array('_id'=>84537)); $_block_repeat=true; echo Twenga_Services_Lang::trans(array('_id'=>84537), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Installer Twenga Solutions<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Twenga_Services_Lang::trans(array('_id'=>84537), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>

    <?php echo $_smarty_tpl->getSubTemplate ("./configure/step1.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


    <?php echo $_smarty_tpl->getSubTemplate ("./configure/step2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


</div>

<!-- POPIN MOT DE PASSE OUBLIE -->
<?php echo $_smarty_tpl->getSubTemplate ("./configure/popin.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
    var tw_formSignUpUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['formSignUpUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var tw_formLoginUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['formLoginUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var tw_lostPasswordUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['lostPasswordUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var tw_currentStepDone = <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['currentStepDone']->value, 'htmlall', 'UTF-8');?>
;
    var tw_currentAccountType = "<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['currentAccountType']->value, 'htmlall', 'UTF-8');?>
";

    <?php if (isset($_smarty_tpl->tpl_vars['merchantInfo']->value)){?>
    var tw_merchantInfo = <?php echo json_encode($_smarty_tpl->tpl_vars['merchantInfo']->value);?>
;
    <?php }else{ ?>
    var tw_merchantInfo = null;
    <?php }?>
</script>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.14, created on 2016-09-25 17:25:25
         compiled from "/var/www/vhosts/audioactive.es/httpdocs/modules/twenga/views/templates/admin/configure/step1.tpl" */ ?>
<?php /*%%SmartyHeaderCode:34918796357e7ec65b40234-00888208%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2e00ee0ed664e5eaf8b97d2a7124d019b8502988' => 
    array (
      0 => '/var/www/vhosts/audioactive.es/httpdocs/modules/twenga/views/templates/admin/configure/step1.tpl',
      1 => 1474817124,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '34918796357e7ec65b40234-00888208',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'formSignUp' => 0,
    'formLogin' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_57e7ec65b510f3_93552164',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57e7ec65b510f3_93552164')) {function content_57e7ec65b510f3_93552164($_smarty_tpl) {?>
<div class="tw-step tw-step1 step-wait" data-step="1">
    <p class="tw-title tw-padding">
        <b><?php $_smarty_tpl->smarty->_tag_stack[] = array('tr', array('_id'=>84927,'step'=>1)); $_block_repeat=true; echo Twenga_Services_Lang::trans(array('_id'=>84927,'step'=>1), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Etape %step% :<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Twenga_Services_Lang::trans(array('_id'=>84927,'step'=>1), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('tr', array('_id'=>84557)); $_block_repeat=true; echo Twenga_Services_Lang::trans(array('_id'=>84557), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Configurer votre compte<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Twenga_Services_Lang::trans(array('_id'=>84557), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>

    <!-- ETAPE AVEC PAS DE COMPTE CREE -->
    <div class="tw-step-content tw-step-form-signup">
        <?php echo $_smarty_tpl->tpl_vars['formSignUp']->value;?>

    </div>

    <!-- ETAPE AVEC COMPTE EXISTANT -->
    <div class="tw-step-content tw-step-form-login">
        <?php echo $_smarty_tpl->tpl_vars['formLogin']->value;?>

    </div>
</div>
<?php }} ?>
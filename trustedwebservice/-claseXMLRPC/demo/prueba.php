<html>
<head><title>xmlrpc</title></head>
<body>
<?php
	include("xmlrpc.inc");

	$f = new xmlrpcmsg('examples.getStateName');

	print "<h3>Testing value serialization</h3>\n";

	$v = new xmlrpcval(23, "int");
	print "<PRE>" . htmlentities($v->serialize()) . "</PRE>";
	$v = new xmlrpcval("What are you saying? >> << &&");
	print "<PRE>" . htmlentities($v->serialize()) . "</PRE>";

	$v =
		new xmlrpcval(array(
		new xmlrpcval("ABCDEFHIJ"),
		new xmlrpcval(1234, 'int'),
		new xmlrpcval(1, 'boolean')),
		"array"
	);

	print "<PRE>" . htmlentities($v->serialize()) . "</PRE>";
	
?>
</body>
</html>
<?php
include("claseXMLRPC/lib/xmlrpc.inc");
include("claseXMLRPC/lib/xmlrpcs.inc");
include("funciones.php");

function enviaJSON($datosSelli)
{
	global $NoError;
  	$err="";

        $ParFechaIni=$datosSelli->getParam(0);
        $ParVersion=$datosSelli->getParam(1);
        $ParKey=$datosSelli->getParam(2);
        
	if(isset($ParFechaIni))
            $fechaIni=$ParFechaIni->scalarval();
        
        if(isset($ParKey))
            $trustivity_key=$ParKey->scalarval();
        
        if(isset($ParVersion) && ($ParVersion->scalartyp()=="string")){
            $vers=$ParVersion->scalarval();
            
            if($vers == "PS 1.5")
                $respuesta = json_orders_todayPS($fechaIni, "1.5", $trustivity_key);
	}else {
		// Error
		$err="Fallo al enviar";}

  		if ($err){
			return new xmlrpcresp(0, $NoError, $err);
		}else{
			return new xmlrpcresp(new xmlrpcval($respuesta));
		}
}

	$s=new xmlrpc_server(array("sellisfaction.enviaJSON" =>array("function" => "enviaJSON")));
?>
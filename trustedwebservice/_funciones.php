<?php
include("config.php");

function conectar_bbdd()
{
    $con = (mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_)) or die(mysql_error());
    mysql_select_db(_DB_NAME_,$con) or die(mysql_error());
    
    return $con;
}

function json_orders_todayPS($fechaIniTrust, $version, $pwd)
{
        if($pwd == TRUSTIVITY_KEY)
        {
            $con = conectar_bbdd();

            // Consulta que retorna los pedidos de los ultimos 40 dias
            $consulta = mysql_query("select id_order, id_customer, delivery_date, valid, total_paid from " . _DB_PREFIX_ . "orders
                        where DATE_SUB(CURDATE(),INTERVAL 40 DAY) <= delivery_date and valid = 1 and delivery_date > '$fechaIniTrust' order by id_order desc", $con) or die(mysql_error());

                while($fila = mysql_fetch_assoc($consulta))
                {
                    $consultaProduct = mysql_query("select product_id, product_name from " . _DB_PREFIX_ . "order_detail where id_order={$fila["id_order"]} LIMIT 4", $con) or die(mysql_error());
                    $consultaMail = mysql_query("select firstname, lastname, email from " . _DB_PREFIX_ . "customer where id_customer={$fila["id_customer"]}", $con) or die(mysql_error());

                    $datos["productos"] = array();

                    $customer = mysql_fetch_assoc($consultaMail);


                    while($products = mysql_fetch_assoc($consultaProduct))  // Id, nombre e imagen del producto
                        $datos["productos"][] = array(
                                                        "id"=> $products["product_id"],
                                                        "nom" => utf8_encode($products["product_name"]));

                    $datos["pedidos"][] = 
                                array("id_order" => $fila["id_order"], 
                                      "customer_mail" => $customer["email"], 
                                      "customer_firstname" => utf8_encode($customer["firstname"]), 
                                      "customer_lastname" => utf8_encode($customer["lastname"]), 
                                      "fecha" =>  $fila["delivery_date"],
                                      "total" => $fila["total_paid"],
                                      "productos" =>  $datos["productos"]);
                }

                $json = json_encode($datos["pedidos"]);
        }
        else 
             $json = json_encode("Error en la clave");
        
        return $json;
}
 
function json_easy_start($days, $pwd)
{
        if($pwd == TRUSTIVITY_KEY)
        {
            $con = conectar_bbdd();

            $consulta = mysql_query("select id_order, id_customer, delivery_date, valid, total_paid from " . _DB_PREFIX_ . "orders
                        where DATE_SUB(CURDATE(),INTERVAL $days DAY) <= delivery_date and valid = 1 order by id_order desc", $con) or die(mysql_error());

                while($fila = mysql_fetch_assoc($consulta))
                {
                    $consultaProduct = mysql_query("select product_id, product_name from " . _DB_PREFIX_ . "order_detail where id_order={$fila["id_order"]} LIMIT 2", $con) or die(mysql_error());
                    $consultaMail = mysql_query("select firstname, lastname, email from " . _DB_PREFIX_ . "customer where id_customer={$fila["id_customer"]}", $con) or die(mysql_error());

                    $datos["productos"] = array();

                    $customer = mysql_fetch_assoc($consultaMail);


                    while($products = mysql_fetch_assoc($consultaProduct))
                        $datos["productos"][] = array("id"=> $products["product_id"],"nom" => utf8_encode($products["product_name"]));

                    $datos["pedidos"][] = 
                                array("id_order" => $fila["id_order"], 
                                      "customer_mail" => $customer["email"], 
                                      "customer_firstname" => utf8_encode($customer["firstname"]), 
                                      "customer_lastname" => utf8_encode($customer["lastname"]), 
                                      "fecha" =>  $fila["delivery_date"],
                                      "total" => $fila["total_paid"],
                                      "productos" =>  $datos["productos"]);
                }

                $json = json_encode($datos["pedidos"]);
        }    
        else
             $json = json_encode("Error en la clave");
        
        return $json;
}

function comprobar_codigo_ActivSell()
{
        $con = conectar_bbdd();

        $consulta = mysql_query("select value from " . _DB_PREFIX_ . "configuration where name='reg_sellisfaction'", $con) or die(mysql_error());

        $fila = mysql_fetch_assoc($consulta);
        
        if(!empty($fila["value"]))
            return $fila["value"];
        else
            return NULL;
        
}

function test_install($frmwork)
{
    $con = (mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_));
    
    if (!$con) {
        die('No pudo conectarse: ' . mysql_error());
    }
    else
        echo 'Conectado satisfactoriamente';
    
    mysql_close($con);
}
?>
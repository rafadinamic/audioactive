<?php
/**
 * User: David Moya de la Ossa <david.moya@adoramedia.com>
 * Date: 5/09/14
 * Time: 17:23
 */
class AdminFeaturesController extends AdminFeaturesControllerCore
{
    public function initFormFeatureValue()
    {
        $this->setTypeValue();

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Feature value'),
                'image' => '../img/t/AdminFeatures.gif'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Value:'),
                    'name' => 'value',
                    'lang' => true,
                    'size' => 33,
                    'hint' => $this->l('Invalid characters:').' <>;=#{}',
                    'required' => true
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Feature:'),
                    'name' => 'id_feature',
                    'options' => array(
                        'query' => Feature::getFeatures($this->context->language->id),
                        'id' => 'id_feature',
                        'name' => 'name'
                    ),
                    'required' => true
                )
                /** cambio **/,
                array(
                    'type' => 'text',
                    'label' => $this->l('Reference:'),
                    'name' => 'reference',
                    'lang' => false,
                    'size' => 32,
                    'hint' => $this->l('Valid characters:').' [a-z,A-Z,0-9,-]',
                    'required' => false
                )
                /***/
            ),
            'submit' => array(
                'title' => $this->l('   Save   '),
                'class' => 'button'
            )
        );

        // Create Object FeatureValue
        $feature_value = new FeatureValue(Tools::getValue('id_feature_value'));

        $this->tpl_vars = array(
            'feature_value' => $feature_value,
        );

        $this->getlanguages();
        $helper = new HelperForm();
        $helper->currentIndex = self::$currentIndex;
        $helper->token = $this->token;
        $helper->table = $this->table;
        $helper->identifier = $this->identifier;
        $helper->override_folder = 'feature_value/';
        $helper->id = $feature_value->id;
        $helper->toolbar_scroll = false;
        $helper->tpl_vars = $this->tpl_vars;
        $helper->languages = $this->_languages;
        $helper->default_form_language = $this->default_form_language;
        $helper->allow_employee_form_lang = $this->allow_employee_form_lang;
        $helper->fields_value = $this->getFieldsValue($feature_value);
        $helper->toolbar_btn = $this->toolbar_btn;
        $helper->title = $this->l('Add a new feature value');
        $this->content .= $helper->generateForm($this->fields_form);
    }
}
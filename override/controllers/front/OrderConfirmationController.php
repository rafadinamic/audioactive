<?php
/**
 * Project Audioactive.
 * User: David Moya de la Ossa <david.moya@adoramedia.com>
 * Date: 8/09/14
 * Time: 10:06
 */
class OrderConfirmationController extends OrderConfirmationControllerCore
{
    /**
     * @inheritdoc
     */
    public function displayPaymentReturn()
    {
        if(Validate::isUnsignedId($this->id_order) && Validate::isUnsignedId($this->id_module))
        {
            $params = array();
            $order = new Order($this->id_order);
            $currency = new Currency($order->id_currency);

            if(Validate::isLoadedObject($order))
            {
                $params['total_to_pay'] = $order->getOrdersTotalPaid();
                $params['currency'] = $currency->sign;
                $params['objOrder'] = $order;
                $params['currencyObj'] = $currency;

                /** cambio **/
                $this->context->smarty->assign($params);
                /***/

                return Hook::exec('displayPaymentReturn', $params, $this->id_module);
            }
        }
        return false;
    }

} 
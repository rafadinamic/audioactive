<?php
/**
 * User: David Moya de la Ossa <david.moya@adoramedia.com>
 * Date: 5/09/14
 * Time: 17:06
 */
class FeatureValue extends FeatureValueCore
{
    /** @var string */
    public $reference;

    /**
     *
     */
    public static function init()
    {
        self::$definition['fields']['reference'] = array('type' => self::TYPE_STRING, 'validate' => 'isLinkRewrite', 'size' => 32);
    }
}
FeatureValue::init();
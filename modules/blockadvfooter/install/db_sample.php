<?php
/* Data sample for moduleblockadvfooter*/
$query = "CREATE TABLE IF NOT EXISTS `_DB_PREFIX_loffc_block` (
  `id_loffc_block` int(11) NOT NULL AUTO_INCREMENT,
  `width` float(10,2) NOT NULL,
  `show_title` tinyint(1) NOT NULL,
  `id_position` tinyint(2) NOT NULL,
  PRIMARY KEY (`id_loffc_block`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block` VALUES
('1','25.00','1','2'),
('2','25.00','1','2'),
('3','25.00','0','2'),
('4','25.00','1','2'),
('12','100.00','0','1');
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_loffc_block_item` (
  `id_loffc_block_item` int(11) NOT NULL AUTO_INCREMENT,
  `id_loffc_block` int(11) NOT NULL,
  `type` varchar(25) NOT NULL,
  `link` varchar(2000) NOT NULL,
  `linktype` varchar(25) NOT NULL,
  `link_content` varchar(2000) NOT NULL,
  `module_name` varchar(100) NOT NULL,
  `hook_name` varchar(100) NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `addthis` tinyint(1) NOT NULL,
  `show_title` tinyint(1) NOT NULL DEFAULT '1',
  `target` varchar(20) NOT NULL DEFAULT '_self',
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id_loffc_block_item`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block_item` VALUES
('1','1','link','','link','history','','','','','0','1','','0'),
('14','1','link','','link','order-slip','','','','','0','1','_self','0'),
('15','1','link','','link','addresses','','','','','0','1','_self','0'),
('16','1','link','','link','identity','','','','','0','1','_self','0'),
('17','1','link','','link','discount','','','','','0','1','_self','0'),
('18','2','link','','cms','1','','','','','0','1','_self','0'),
('19','2','link','','cms','2','','','','','0','1','_self','0'),
('20','2','link','','cms','3','','','','','0','1','_self','0'),
('21','2','link','','cms','4','','','','','0','1','_self','0'),
('22','2','link','','cms','5','','','','','0','1','_self','4'),
('28','12','custom_html','','','','','','','','0','0','','0'),
('29','3','module','','','','blockcontactinfos','displayfooter','','','0','0','','0'),
('30','4','module','','','','blocknewsletter','displayfooter','','','0','0','','0');
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_loffc_block_item_lang` (
  `id_loffc_block_item` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id_loffc_block_item`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_loffc_block_item_shop` (
  `id_loffc_block_item` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_loffc_block_item`,`id_shop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block_item_shop` VALUES
('1','LEO_ID_SHOP'),
('14','LEO_ID_SHOP'),
('15','LEO_ID_SHOP'),
('16','LEO_ID_SHOP'),
('17','LEO_ID_SHOP'),
('18','LEO_ID_SHOP'),
('19','LEO_ID_SHOP'),
('20','LEO_ID_SHOP'),
('21','LEO_ID_SHOP'),
('22','LEO_ID_SHOP'),
('28','LEO_ID_SHOP'),
('29','LEO_ID_SHOP'),
('30','LEO_ID_SHOP');
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_loffc_block_lang` (
  `id_loffc_block` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id_loffc_block`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_loffc_block_shop` (
  `id_loffc_block` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_loffc_block`,`id_shop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_loffc_block_shop` VALUES
('1','LEO_ID_SHOP'),
('2','LEO_ID_SHOP'),
('3','LEO_ID_SHOP'),
('4','LEO_ID_SHOP'),
('12','LEO_ID_SHOP');
";
$dataLang = Array("en"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('1','LEO_ID_LANGUAGE','My orders',''),
('14','LEO_ID_LANGUAGE','My credit slips',''),
('15','LEO_ID_LANGUAGE','My addresses',''),
('16','LEO_ID_LANGUAGE','My personal info',''),
('17','LEO_ID_LANGUAGE','My vouchers',''),
('18','LEO_ID_LANGUAGE','Delivery',''),
('19','LEO_ID_LANGUAGE','Legal Notice',''),
('20','LEO_ID_LANGUAGE','Terms and conditions of use',''),
('21','LEO_ID_LANGUAGE','About us',''),
('22','LEO_ID_LANGUAGE','Secure payment',''),
('28','LEO_ID_LANGUAGE','social','<div class=\"social\"><a title=\"\" href=\"#\"><em class=\"fa fa-pinterest\"> </em>Pinterest</a> <a title=\"\" href=\"#\"><em class=\"fa fa-facebook\"> </em>Facebook</a> <a title=\"\" href=\"#\"><em class=\"fa fa-google-plus\"> </em>Google+</a> <a title=\"\" href=\"#\"><em class=\"fa fa-twitter\"> </em>Twitter</a> <a title=\"\" href=\"#\"><em class=\"fa fa-youtube\"> </em>Youtube</a></div>'),
('29','LEO_ID_LANGUAGE','Contac us',''),
('30','LEO_ID_LANGUAGE','newsletter','');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('1','LEO_ID_LANGUAGE','My account'),
('2','LEO_ID_LANGUAGE','information'),
('3','LEO_ID_LANGUAGE','Contact Us'),
('4','LEO_ID_LANGUAGE','Newsletter'),
('12','LEO_ID_LANGUAGE','social');
","br"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('1','LEO_ID_LANGUAGE','Minhas ordens',''),
('14','LEO_ID_LANGUAGE','Meus deslizamentos de crédito',''),
('15','LEO_ID_LANGUAGE','Meus endereços',''),
('16','LEO_ID_LANGUAGE','Minha informação pessoal',''),
('17','LEO_ID_LANGUAGE','Meus vales',''),
('18','LEO_ID_LANGUAGE','Entrega',''),
('19','LEO_ID_LANGUAGE','Aviso Legal',''),
('20','LEO_ID_LANGUAGE','Termos e condições de uso',''),
('21','LEO_ID_LANGUAGE','sobre nós',''),
('22','LEO_ID_LANGUAGE','Secure payment',''),
('28','LEO_ID_LANGUAGE','social','<div class=\"social\"><a title=\"\" href=\"#\"><em class=\"fa fa-pinterest\"> </em>Pinterest</a> <a title=\"\" href=\"#\"><em class=\"fa fa-facebook\"> </em>Facebook</a> <a title=\"\" href=\"#\"><em class=\"fa fa-google-plus\"> </em>Google+</a> <a title=\"\" href=\"#\"><em class=\"fa fa-twitter\"> </em>Twitter</a> <a title=\"\" href=\"#\"><em class=\"fa fa-youtube\"> </em>Youtube</a></div>'),
('29','LEO_ID_LANGUAGE','Contac us',''),
('30','LEO_ID_LANGUAGE','newsletter','');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('1','LEO_ID_LANGUAGE','A minha conta'),
('2','LEO_ID_LANGUAGE','information'),
('3','LEO_ID_LANGUAGE','Contact Us'),
('4','LEO_ID_LANGUAGE','Newsletter'),
('12','LEO_ID_LANGUAGE','social');
","de"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('1','LEO_ID_LANGUAGE','Meine Bestellungen',''),
('14','LEO_ID_LANGUAGE','Meine Kreditabrechnungen',''),
('15','LEO_ID_LANGUAGE','Meine Adresse',''),
('16','LEO_ID_LANGUAGE','Persönliches über mich',''),
('17','LEO_ID_LANGUAGE','Meine Gutscheine',''),
('18','LEO_ID_LANGUAGE','Lieferung',''),
('19','LEO_ID_LANGUAGE','Impressum',''),
('20','LEO_ID_LANGUAGE','Allgemeine Geschäftsbedingungen',''),
('21','LEO_ID_LANGUAGE','Wir über uns',''),
('22','LEO_ID_LANGUAGE','Secure payment',''),
('28','LEO_ID_LANGUAGE','social','<div class=\"social\"><a title=\"\" href=\"#\"><em class=\"fa fa-pinterest\"> </em>Pinterest</a> <a title=\"\" href=\"#\"><em class=\"fa fa-facebook\"> </em>Facebook</a> <a title=\"\" href=\"#\"><em class=\"fa fa-google-plus\"> </em>Google+</a> <a title=\"\" href=\"#\"><em class=\"fa fa-twitter\"> </em>Twitter</a> <a title=\"\" href=\"#\"><em class=\"fa fa-youtube\"> </em>Youtube</a></div>'),
('29','LEO_ID_LANGUAGE','Contac us',''),
('30','LEO_ID_LANGUAGE','newsletter','');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('1','LEO_ID_LANGUAGE','Mein Konto'),
('2','LEO_ID_LANGUAGE','information'),
('3','LEO_ID_LANGUAGE','Contact Us'),
('4','LEO_ID_LANGUAGE','Newsletter'),
('12','LEO_ID_LANGUAGE','social');
","es"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('1','LEO_ID_LANGUAGE','Mis pedidos',''),
('14','LEO_ID_LANGUAGE','Mis hojas de crédito',''),
('15','LEO_ID_LANGUAGE','Mis direcciones',''),
('16','LEO_ID_LANGUAGE','Mis datos personales',''),
('17','LEO_ID_LANGUAGE','Mis vales',''),
('18','LEO_ID_LANGUAGE','Entrega',''),
('19','LEO_ID_LANGUAGE','Aviso Legal',''),
('20','LEO_ID_LANGUAGE','Términos y condiciones de uso',''),
('21','LEO_ID_LANGUAGE','¿Quiénes somos?',''),
('22','LEO_ID_LANGUAGE','Secure payment',''),
('28','LEO_ID_LANGUAGE','social','<div class=\"social\"><a title=\"\" href=\"#\"><em class=\"fa fa-pinterest\"> </em>Pinterest</a> <a title=\"\" href=\"#\"><em class=\"fa fa-facebook\"> </em>Facebook</a> <a title=\"\" href=\"#\"><em class=\"fa fa-google-plus\"> </em>Google+</a> <a title=\"\" href=\"#\"><em class=\"fa fa-twitter\"> </em>Twitter</a> <a title=\"\" href=\"#\"><em class=\"fa fa-youtube\"> </em>Youtube</a></div>'),
('29','LEO_ID_LANGUAGE','Contac us',''),
('30','LEO_ID_LANGUAGE','newsletter','');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('1','LEO_ID_LANGUAGE','Mi cuenta'),
('2','LEO_ID_LANGUAGE','information'),
('3','LEO_ID_LANGUAGE','Contact Us'),
('4','LEO_ID_LANGUAGE','Newsletter'),
('12','LEO_ID_LANGUAGE','social');
","fr"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('1','LEO_ID_LANGUAGE','Mes commandes',''),
('14','LEO_ID_LANGUAGE','Mes notes de crédit',''),
('15','LEO_ID_LANGUAGE','Mes adresses',''),
('16','LEO_ID_LANGUAGE','Informations personnelles',''),
('17','LEO_ID_LANGUAGE','Mes bons',''),
('18','LEO_ID_LANGUAGE','Livraison',''),
('19','LEO_ID_LANGUAGE','Mentions légales',''),
('20','LEO_ID_LANGUAGE','Conditions d\'utilisation',''),
('21','LEO_ID_LANGUAGE','À propos de nous',''),
('22','LEO_ID_LANGUAGE','Secure payment',''),
('28','LEO_ID_LANGUAGE','social','<div class=\"social\"><a title=\"\" href=\"#\"><em class=\"fa fa-pinterest\"> </em>Pinterest</a> <a title=\"\" href=\"#\"><em class=\"fa fa-facebook\"> </em>Facebook</a> <a title=\"\" href=\"#\"><em class=\"fa fa-google-plus\"> </em>Google+</a> <a title=\"\" href=\"#\"><em class=\"fa fa-twitter\"> </em>Twitter</a> <a title=\"\" href=\"#\"><em class=\"fa fa-youtube\"> </em>Youtube</a></div>'),
('29','LEO_ID_LANGUAGE','Contac us',''),
('30','LEO_ID_LANGUAGE','newsletter','');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('1','LEO_ID_LANGUAGE','Mon compte'),
('2','LEO_ID_LANGUAGE','information'),
('3','LEO_ID_LANGUAGE','Contact Us'),
('4','LEO_ID_LANGUAGE','Newsletter'),
('12','LEO_ID_LANGUAGE','social');
","id"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('1','LEO_ID_LANGUAGE','Perintah saya',''),
('14','LEO_ID_LANGUAGE','Slip kredit saya',''),
('15','LEO_ID_LANGUAGE','Alamat saya',''),
('16','LEO_ID_LANGUAGE','Info pribadi saya',''),
('17','LEO_ID_LANGUAGE','Voucher saya',''),
('18','LEO_ID_LANGUAGE','pengiriman',''),
('19','LEO_ID_LANGUAGE','Pemberitahuan Legal',''),
('20','LEO_ID_LANGUAGE','Syarat dan ketentuan penggunaan',''),
('21','LEO_ID_LANGUAGE','Tentang kami',''),
('22','LEO_ID_LANGUAGE','Secure payment',''),
('28','LEO_ID_LANGUAGE','social','<div class=\"social\"><a title=\"\" href=\"#\"><em class=\"fa fa-pinterest\"> </em>Pinterest</a> <a title=\"\" href=\"#\"><em class=\"fa fa-facebook\"> </em>Facebook</a> <a title=\"\" href=\"#\"><em class=\"fa fa-google-plus\"> </em>Google+</a> <a title=\"\" href=\"#\"><em class=\"fa fa-twitter\"> </em>Twitter</a> <a title=\"\" href=\"#\"><em class=\"fa fa-youtube\"> </em>Youtube</a></div>'),
('29','LEO_ID_LANGUAGE','Contac us',''),
('30','LEO_ID_LANGUAGE','newsletter','');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('1','LEO_ID_LANGUAGE','Akun saya'),
('2','LEO_ID_LANGUAGE','information'),
('3','LEO_ID_LANGUAGE','Contact Us'),
('4','LEO_ID_LANGUAGE','Newsletter'),
('12','LEO_ID_LANGUAGE','social');
","it"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('1','LEO_ID_LANGUAGE','I miei ordini',''),
('14','LEO_ID_LANGUAGE','I miei scivola di credito',''),
('15','LEO_ID_LANGUAGE','I miei indirizzi',''),
('16','LEO_ID_LANGUAGE','Le mie informazioni personali',''),
('17','LEO_ID_LANGUAGE','I miei buoni',''),
('18','LEO_ID_LANGUAGE','consegna',''),
('19','LEO_ID_LANGUAGE','Legal Notice',''),
('20','LEO_ID_LANGUAGE','Termini e condizioni d\'uso',''),
('21','LEO_ID_LANGUAGE','Chi siamo',''),
('22','LEO_ID_LANGUAGE','Secure payment',''),
('28','LEO_ID_LANGUAGE','social','<div class=\"social\"><a title=\"\" href=\"#\"><em class=\"fa fa-pinterest\"> </em>Pinterest</a> <a title=\"\" href=\"#\"><em class=\"fa fa-facebook\"> </em>Facebook</a> <a title=\"\" href=\"#\"><em class=\"fa fa-google-plus\"> </em>Google+</a> <a title=\"\" href=\"#\"><em class=\"fa fa-twitter\"> </em>Twitter</a> <a title=\"\" href=\"#\"><em class=\"fa fa-youtube\"> </em>Youtube</a></div>'),
('29','LEO_ID_LANGUAGE','Contac us',''),
('30','LEO_ID_LANGUAGE','newsletter','');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('1','LEO_ID_LANGUAGE','Il mio conto'),
('2','LEO_ID_LANGUAGE','information'),
('3','LEO_ID_LANGUAGE','Contact Us'),
('4','LEO_ID_LANGUAGE','Newsletter'),
('12','LEO_ID_LANGUAGE','social');
","nl"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('1','LEO_ID_LANGUAGE','Mijn bestellingen',''),
('14','LEO_ID_LANGUAGE','Mijn credit nota',''),
('15','LEO_ID_LANGUAGE','Mijn adressen',''),
('16','LEO_ID_LANGUAGE','Mijn persoonlijke info',''),
('17','LEO_ID_LANGUAGE','Mijn waardebonnen',''),
('18','LEO_ID_LANGUAGE','levering',''),
('19','LEO_ID_LANGUAGE','Legal Notice',''),
('20','LEO_ID_LANGUAGE','Bepalingen en voorwaarden van',''),
('21','LEO_ID_LANGUAGE','Over ons',''),
('22','LEO_ID_LANGUAGE','Secure payment',''),
('28','LEO_ID_LANGUAGE','social','<div class=\"social\"><a title=\"\" href=\"#\"><em class=\"fa fa-pinterest\"> </em>Pinterest</a> <a title=\"\" href=\"#\"><em class=\"fa fa-facebook\"> </em>Facebook</a> <a title=\"\" href=\"#\"><em class=\"fa fa-google-plus\"> </em>Google+</a> <a title=\"\" href=\"#\"><em class=\"fa fa-twitter\"> </em>Twitter</a> <a title=\"\" href=\"#\"><em class=\"fa fa-youtube\"> </em>Youtube</a></div>'),
('29','LEO_ID_LANGUAGE','Contac us',''),
('30','LEO_ID_LANGUAGE','newsletter','');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('1','LEO_ID_LANGUAGE','Mijn account'),
('2','LEO_ID_LANGUAGE','information'),
('3','LEO_ID_LANGUAGE','Contact Us'),
('4','LEO_ID_LANGUAGE','Newsletter'),
('12','LEO_ID_LANGUAGE','social');
","pl"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('1','LEO_ID_LANGUAGE','Moje zamówienia',''),
('14','LEO_ID_LANGUAGE','Moje zrazy kredytowe',''),
('15','LEO_ID_LANGUAGE','Moje adresy',''),
('16','LEO_ID_LANGUAGE','Moje informacje osobiste',''),
('17','LEO_ID_LANGUAGE','Moje kupony',''),
('18','LEO_ID_LANGUAGE','Dostawa',''),
('19','LEO_ID_LANGUAGE','Nota prawna',''),
('20','LEO_ID_LANGUAGE','Regulamin użytkowania',''),
('21','LEO_ID_LANGUAGE','O nas',''),
('22','LEO_ID_LANGUAGE','Secure payment',''),
('28','LEO_ID_LANGUAGE','social','<div class=\"social\"><a title=\"\" href=\"#\"><em class=\"fa fa-pinterest\"> </em>Pinterest</a> <a title=\"\" href=\"#\"><em class=\"fa fa-facebook\"> </em>Facebook</a> <a title=\"\" href=\"#\"><em class=\"fa fa-google-plus\"> </em>Google+</a> <a title=\"\" href=\"#\"><em class=\"fa fa-twitter\"> </em>Twitter</a> <a title=\"\" href=\"#\"><em class=\"fa fa-youtube\"> </em>Youtube</a></div>'),
('29','LEO_ID_LANGUAGE','Contac us',''),
('30','LEO_ID_LANGUAGE','newsletter','');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('1','LEO_ID_LANGUAGE','Moje konto'),
('2','LEO_ID_LANGUAGE','information'),
('3','LEO_ID_LANGUAGE','Contact Us'),
('4','LEO_ID_LANGUAGE','Newsletter'),
('12','LEO_ID_LANGUAGE','social');
","ru"=>"INSERT INTO `_DB_PREFIX_loffc_block_item_lang` VALUES
('1','LEO_ID_LANGUAGE','Мои заказы',''),
('14','LEO_ID_LANGUAGE','Мой скользит кредитных',''),
('15','LEO_ID_LANGUAGE','Мои адреса',''),
('16','LEO_ID_LANGUAGE','Мой личный Информация',''),
('17','LEO_ID_LANGUAGE','Мои ваучеры',''),
('18','LEO_ID_LANGUAGE','доставка',''),
('19','LEO_ID_LANGUAGE','Официальное уведомление',''),
('20','LEO_ID_LANGUAGE','Условия использования',''),
('21','LEO_ID_LANGUAGE','О нас',''),
('22','LEO_ID_LANGUAGE','Secure payment',''),
('28','LEO_ID_LANGUAGE','social','<div class=\"social\"><a title=\"\" href=\"#\"><em class=\"fa fa-pinterest\"> </em>Pinterest</a> <a title=\"\" href=\"#\"><em class=\"fa fa-facebook\"> </em>Facebook</a> <a title=\"\" href=\"#\"><em class=\"fa fa-google-plus\"> </em>Google+</a> <a title=\"\" href=\"#\"><em class=\"fa fa-twitter\"> </em>Twitter</a> <a title=\"\" href=\"#\"><em class=\"fa fa-youtube\"> </em>Youtube</a></div>'),
('29','LEO_ID_LANGUAGE','Contac us',''),
('30','LEO_ID_LANGUAGE','newsletter','');
INSERT INTO `_DB_PREFIX_loffc_block_lang` VALUES
('1','LEO_ID_LANGUAGE','Моя учетная запись'),
('2','LEO_ID_LANGUAGE','information'),
('3','LEO_ID_LANGUAGE','Contact Us'),
('4','LEO_ID_LANGUAGE','Newsletter'),
('12','LEO_ID_LANGUAGE','social');
");
<?php
/* Data sample for moduleleocoinslider*/
$query = "CREATE TABLE IF NOT EXISTS `_DB_PREFIX_leocoinslider` (
  `id_leocoinslider_slides` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_leocoinslider_slides`,`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_leocoinslider` VALUES
('1','LEO_ID_SHOP'),
('2','LEO_ID_SHOP');
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_leocoinslider_slides` (
  `id_leocoinslider_slides` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_leocoinslider_slides`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO `_DB_PREFIX_leocoinslider_slides` VALUES
('1','1','1'),
('2','2','1');
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_leocoinslider_slides_lang` (
  `id_leocoinslider_slides` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `legend` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id_leocoinslider_slides`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
$dataLang = Array("en"=>"INSERT INTO `_DB_PREFIX_leocoinslider_slides_lang` VALUES
('1','LEO_ID_LANGUAGE','Sample 1','This is a sample picture','sample-1','http://www.prestashop.com','sample-1.jpg'),
('2','LEO_ID_LANGUAGE','Sample 2','This is a sample picture','sample-2','http://www.prestashop.com','sample-2.jpg');
","br"=>"INSERT INTO `_DB_PREFIX_leocoinslider_slides_lang` VALUES
('1','LEO_ID_LANGUAGE','Sample 1','This is a sample picture','sample-1','http://www.prestashop.com','sample-1.jpg'),
('2','LEO_ID_LANGUAGE','Sample 2','This is a sample picture','sample-2','http://www.prestashop.com','sample-2.jpg');
","de"=>"INSERT INTO `_DB_PREFIX_leocoinslider_slides_lang` VALUES
('1','LEO_ID_LANGUAGE','Sample 1','This is a sample picture','sample-1','http://www.prestashop.com','sample-1.jpg'),
('2','LEO_ID_LANGUAGE','Sample 2','This is a sample picture','sample-2','http://www.prestashop.com','sample-2.jpg');
","es"=>"INSERT INTO `_DB_PREFIX_leocoinslider_slides_lang` VALUES
('1','LEO_ID_LANGUAGE','Sample 1','This is a sample picture','sample-1','http://www.prestashop.com','sample-1.jpg'),
('2','LEO_ID_LANGUAGE','Sample 2','This is a sample picture','sample-2','http://www.prestashop.com','sample-2.jpg');
","fr"=>"INSERT INTO `_DB_PREFIX_leocoinslider_slides_lang` VALUES
('1','LEO_ID_LANGUAGE','Sample 1','This is a sample picture','sample-1','http://www.prestashop.com','sample-1.jpg'),
('2','LEO_ID_LANGUAGE','Sample 2','This is a sample picture','sample-2','http://www.prestashop.com','sample-2.jpg');
","id"=>"INSERT INTO `_DB_PREFIX_leocoinslider_slides_lang` VALUES
('1','LEO_ID_LANGUAGE','Sample 1','This is a sample picture','sample-1','http://www.prestashop.com','sample-1.jpg'),
('2','LEO_ID_LANGUAGE','Sample 2','This is a sample picture','sample-2','http://www.prestashop.com','sample-2.jpg');
","it"=>"INSERT INTO `_DB_PREFIX_leocoinslider_slides_lang` VALUES
('1','LEO_ID_LANGUAGE','Sample 1','This is a sample picture','sample-1','http://www.prestashop.com','sample-1.jpg'),
('2','LEO_ID_LANGUAGE','Sample 2','This is a sample picture','sample-2','http://www.prestashop.com','sample-2.jpg');
","nl"=>"INSERT INTO `_DB_PREFIX_leocoinslider_slides_lang` VALUES
('1','LEO_ID_LANGUAGE','Sample 1','This is a sample picture','sample-1','http://www.prestashop.com','sample-1.jpg'),
('2','LEO_ID_LANGUAGE','Sample 2','This is a sample picture','sample-2','http://www.prestashop.com','sample-2.jpg');
","pl"=>"INSERT INTO `_DB_PREFIX_leocoinslider_slides_lang` VALUES
('1','LEO_ID_LANGUAGE','Sample 1','This is a sample picture','sample-1','http://www.prestashop.com','sample-1.jpg'),
('2','LEO_ID_LANGUAGE','Sample 2','This is a sample picture','sample-2','http://www.prestashop.com','sample-2.jpg');
","ru"=>"INSERT INTO `_DB_PREFIX_leocoinslider_slides_lang` VALUES
('1','LEO_ID_LANGUAGE','Sample 1','This is a sample picture','sample-1','http://www.prestashop.com','sample-1.jpg'),
('2','LEO_ID_LANGUAGE','Sample 2','This is a sample picture','sample-2','http://www.prestashop.com','sample-2.jpg');
");
<?php
/**
 * Leo Slideshow Module
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) September 2012 LeoTheme.Com <@emai:leotheme@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
 
/**
 * @since 1.5.0
 * @version 1.2 (2012-03-14)
 */

if (!defined('_PS_VERSION_'))
	exit;
		
		$this->_html .= '<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">';
		/* Save */
		$this->_html .= '
		<div class="margin-form">
			<input type="submit" class="button" name="submitSlider" value="'.$this->l('Save').'" />
		</div>';
		$this->_html .= '
				<fieldset><legend><img src="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" alt="" /> '.$this->l('General configuration').'</legend>';
				
		
		//$this->_html .= $this->getParams()->getThemesTag( $this->getParams()->get('theme') );		
		$this->_html .= $params->inputTag( 'Module Class:', 'modclass', $this->getParams()->get('modclass'), 'px',' size="50" ' );	
		/*
		$this->_html .= $params->inputTag( 'Module Width:', 'modwidth', $this->getParams()->get('modwidth'), 'px',' size="50" ' );	
		$this->_html .= $params->inputTag( 'Module Height:', 'modheight', $this->getParams()->get('modheight'), 'px',' size="50" ' );	
		*/
		$this->_html .= $params->statusTag( 'Cropping Thumbnail:', 'cropthumb', $this->getParams()->get('cropthumb',1), 'cropthumb' );
		$this->_html .= $params->statusTag( 'Show Title:', 'show_title', $this->getParams()->get('show_title',1), 'show_title' );
		$this->_html .= $params->statusTag( 'Show Description:', 'show_desc', $this->getParams()->get('show_desc',1), 'show_desc' );
		$this->_html .= $params->inputTag( 'Slider Image Width:', 'imgwidth', $this->getParams()->get('imgwidth'), 'px',' size="50" ' );	
		$this->_html .= $params->inputTag( 'Slider Image Height:', 'imgheight', $this->getParams()->get('imgheight'), 'px',' size="50" ' );	
                
                $this->_html .= $params->inputTag( 'Limit Items:', 'limit', $this->getParams()->get('limit',6), '<p>'.$this->l('Enter Limit Items be feched').'</p>' );	
		$this->_html .= $params->inputTag( 'Start Item:', 'dfshow', $this->getParams()->get('dfshow',0), '<p>'.$this->l('The item will be showed first').'</p>' );	
                
		//$this->_html .= $params->inputTag( 'Thumbnail Width:', 'thumbwidth', $this->getParams()->get('thumbwidth'), 'px',' size="50" ' );	
		//$this->_html .= $params->inputTag( 'Thumbnail Height:', 'thumbheight', $this->getParams()->get('thumbheight'), 'px',' size="50" ' );	
		
		$this->_html .= '</fieldset>';
		
		// source configuration
		//$this->_html .= '<br><br><fieldset class="clearfix"><legend><img src="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" alt="" /> '.$this->l('Data Source Configuration').'</legend>';
		//$this->_html .= $this->getParams()->getSourceDataTag( $this->getParams()->get('source') );	
		
		
		
		
		//$this->_html .= '</fieldset>';	


	/* Begin fieldset slides */
		$this->_html .= '<br><br>
		<fieldset class="source-group sourceimages">
			<legend><img src="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" alt="" /> '.$this->l('Slides configuration').'</legend>
			<strong>
				<a href="'.AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&addSlide">
					<img src="'._PS_ADMIN_IMG_.'add.gif" alt="" /> '.$this->l('Add Slide').'
				</a>
			</strong>';

		/* Display notice if there are no slides yet */
		if (!$slides)
			$this->_html .= '<p style="margin-left: 40px;">'.$this->l('You have not added any slides yet.').'</p>';
		else /* Display slides */
		{
			$this->_html .= '
			<div id="slidesContent" style="width: 400px; margin-top: 30px;">
				<ul id="slides">';

			foreach ($slides as $slide)
			{
				$this->_html .= '
					<li id="slides_'.$slide['id_slide'].'">
						<strong>#'.$slide['id_slide'].'</strong> '.$slide['title'].'
						<p style="float: right">'.
							$this->displayStatus($slide['id_slide'], $slide['active']).'
							<a href="'.AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&id_slide='.(int)($slide['id_slide']).'" title="'.$this->l('Edit').'"><img src="'._PS_ADMIN_IMG_.'edit.gif" alt="" /></a>
							<a href="'.AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&delete_id_slide='.(int)($slide['id_slide']).'" title="'.$this->l('Delete').'"><img src="'._PS_ADMIN_IMG_.'delete.gif" alt="" /></a>
						</p>
					</li>';
			}
			$this->_html .= '</ul></div>';
		}
		// End fieldset
		$this->_html .= '</fieldset>';
		

		
		/* Begin fieldset slider */
		$this->_html .= '<br><br><fieldset><legend><img src="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" alt="" /> '.$this->l('Slide configuration').'</legend>';
		/* Begin form */
		
		/* Height field */
		$align = array('topLeft'=> $this->l("Top Left"),'topCenter'=>$this->l("Top Center"),'topRight'=>$this->l("Top Right"),'centerLeft'=>$this->l("Top Left")
			, 'center' => $this->l('Center'), 'centerRight' => $this->l('Center Right'), 'bottomCenter' => $this->l('Bottom Center'), 'bottomRight' => $this->l('Bottom Right'));
		//$this->_html .= $params->selectTag( $align, $this->l('Alignment:'), 'alignment', $this->getParams()->get('alignment','center'));
		/* Width field */
		$this->_html .= $params->statusTag( $this->l('Auto Play:'), 'autoAdvance', $this->getParams()->get('autoAdvance',1), '' );
		
		
		$effectArra = array("sliceDown"=>"Slice Down","sliceDownLeft"=>"Slice Down Left","sliceUp"=>"Slice Up"
	            	 ,"sliceUpLeft"=>"Slice Up Left","sliceUpDown"=>"Slice Up Down","sliceUpDownLeft"=>"Slice Up Down Left"
	            	 ,"fold"=>"Fold","fade"=>"Fade","random"=>"Random"
	            	 ,"slideInRight"=>"SlideInRight","slideInLeft"=>"SlideInLeft","boxRandom"=>"BoxRandom"
	            	 ,"boxRain"=>"BoxRain","boxRainReverse"=>"BoxRainReverse","boxRainGrow"=>"BoxRainGrow"                     
	            	 ,"boxRainGrowReverse"=>"BoxRainGrowReverse"
           	);
           	
		$this->_html .= $params->selectTag( $effectArra, $this->l('Effect:'), 'transition', $this->getParams()->get('transition','random'));
                $this->_html .= $params->statusTag( $this->l('Pause on hover:'), 'pausehover', $this->getParams()->get('pausehover',1), '' );
		$this->_html .= $params->inputTag( 'Box Col:', 'boxCols', $this->getParams()->get('boxCols',8), '<p>'.$this->l('For effect').'</p>' );	
		$this->_html .= $params->inputTag( 'Box Row:', 'boxRows', $this->getParams()->get('boxRows',4), '<p>'.$this->l('For effect').'</p>' );
		//$this->_html .= $params->statusTag( $this->l('Pause on hover:'), 'hover', $this->getParams()->get('hover',1), '' );
		$this->_html .= $params->statusTag( $this->l('Show navigation:'), 'navigation', $this->getParams()->get('navigation',1), 'Prev & Next arrows' );
                $this->_html .= $params->statusTag( $this->l('Enable Control Navigation:'), 'cnavigation', $this->getParams()->get('cnavigation',1), 'eg 1,2,3...' );
		$this->_html .= $params->statusTag( $this->l('Navigation on hover:'), 'navigationHover', $this->getParams()->get('navigationHover',1), '<p>'.$this->l('if "yes" the navigation button (prev, next and play/stop buttons) will be visible on hover state only, if "no" they will be visible always').'</p>' );
                
		//$this->_html .= $params->statusTag( $this->l('Show pagination:'), 'pagination', $this->getParams()->get('pagination',1), '' );
		//$this->_html .= $params->statusTag( $this->l('Show play/Pause button:'), 'playPause', $this->getParams()->get('playPause',1), '' );
		//$this->_html .= $params->statusTag( $this->l('Pause On Click:'), 'pauseOnClick', $this->getParams()->get('pauseOnClick',1), '' );
		//$this->_html .= $params->statusTag( $this->l('Show thumbnails:'), 'thumbnails', $this->getParams()->get('thumbnails',1), '' );
		
		$this->_html .= $params->inputTag( 'Duration:', 'duration', $this->getParams()->get('duration','500'), $this->l('ms') );
		/* Loop field */
		$this->_html .= $params->inputTag( 'Interval:', 'interval', $this->getParams()->get('interval','3000'), $this->l('ms') );
		$target = array("_blank|"=>"Blank window","_self"=>"Same window the anchor","_parent"=>"Parent of this document","_top"=>"Top");
		$this->_html .= $params->selectTag( $target, $this->l('Target:'), 'target', $this->getParams()->get('target','_blank'));
		
		//$this->_html .= $params->inputTag( 'Titles Factor:', 'tfactor', $this->getParams()->get('tfactor',0), '<p>//percentage of speed for the titles animation. Speed will be speed * titlesFactor</p>' );
		
		/* End form */
	
		/* End fieldset slider */
		$this->_html .= '</fieldset>';
		
		
	/* Save */
		$this->_html .= '<br /><br />';
		$this->_html .= '
		<div class="margin-form">
			<input type="submit" class="button" name="submitSlider" value="'.$this->l('Save').'" />
		</div>';
	$this->_html .= '</form><br><br>';

	
?>
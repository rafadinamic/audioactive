{*
 * Copyright (c) 2016 Twenga
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @author    Twenga
 * @copyright 2016 Twenga
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}
<div class="tw-customers tw-box tw-box-content">
    <p class="tw-title "><b>{tr _id=85297}Clients :{/tr}</b> {tr _id=7445}Ils nous font déjà confiance{/tr}</p>

    <div class="row tw-padding">
        <div class="col-lg-5">
            <img src="{$_basepath|escape:'htmlall':'UTF-8'}/views/img/logo-client.jpg" alt="Clients" width="383"
                 height="88" class="img-responsive"/>
        </div>
        <div class="col-lg-7">
            <div class="row priceminister">
                <div class="col-sm-5 col-md-4 col-lg-5">
                    <div class="tw-client-prems">
                        <span><em>COS</em><br/> <b>-50%</b></span>
                        <img src="{$_basepath|escape:'htmlall':'UTF-8'}/views/img/priceminister1.png" alt="Priceminister"
                             width="165" height="104"/>
                    </div>
                </div>
                <div class="col-sm-7 col-md-8 col-lg-7 tw-padding-bottom">
                    <div class="tw-testimony">{tr _id=81937}Since activating Smart LEADS, our campaign’s Cost of Sales has been halved. This result has convinced us of the relevancy of this algorithm and played a role in our decision to invest further with Twenga.{/tr}</div>
                    <b>{tr _id=81947}Shani Nomber Search<br/>Price Comparison & Facebook Manager - PriceMinister{/tr}</b>
                </div>
            </div>
        </div>
    </div>
</div>
{*
 * Copyright (c) 2016 Twenga
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @author    Twenga
 * @copyright 2016 Twenga
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}
<div class="tw-banner">
    <div>
        <a class="tw-logo" target="_blank"
           href="{addUtm}{$urlLogoHeader|escape:'htmlall':'UTF-8'}{/addUtm}">
            <img src="{$_basepath|escape:'htmlall':'UTF-8'}/views/img/logo.png" alt="Twenga Solutions" class="img-responsive" width="259"
                 height="25"/>
        </a>

        <p class="banner-title">{tr _id=69507}Référencez votre site Twenga{/tr}</p>
    </div>
    <ul class="tw-list row list-unstyled">
        <li class="col-sm-4">
            <div class="tw-def tw-def1">
                <strong>{tr _id=69517}Attirez de nouveau clients{/tr}</strong><br/>{tr _id=71967}Plus de 30 millions d'e-consommateurs chaque mois{/tr}
            </div>
        </li>
        <li class="col-sm-4">
            <div class="tw-def tw-def2">
                <strong>{tr _id=71977}Augmentez vos Ventes{/tr}</strong><br/>{tr _id=71987}Un trafic performant piloté pour convertir et maximiser votre revenu{/tr}
            </div>
        </li>
        <li class="col-sm-4">
            <div class="tw-def tw-def3">
                <strong>{tr _id=71997}Optimisez vos Coûts{/tr}</strong><br/>{tr _id=72007}Des CPC dynamiques ajustés en temps réel selon le potentiel de conversion{/tr}
            </div>
        </li>
    </ul>
</div>

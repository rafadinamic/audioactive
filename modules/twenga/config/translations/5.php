<?php
/**
  * Copyright (c) 2016 Twenga
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  * OR OTHER DEALINGS IN THE SOFTWARE.
  * 
  * @author    Twenga
  * @copyright 2016 Twenga
  * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
  */
return array(
    7441 => array('Password dimenticata?'),
    7445 => array('Sono già nostri partner'),
    7516 => array('Password'),
    7519 => array('Inserisci il tuo indirizzo e-mail:'),
    7521 => array('Conferma'),
    10371 => array('Telefono'),
    21101 => array('URL del tuo sito'),
    43632 => array('Nome'),
    43642 => array('Cognome'),
    43662 => array('E-mail'),
    64067 => array('Login Twenga Solutions (indirizzo email):'),
    64077 => array('Password Twenga Solutions'),
    64107 => array('Il tuo catalogo prodotti sarà indicizzato in circa 72 ore.'),
    64117 => array(
        'A partire dal momento in cui i tuoi prodotti saranno online, riceverai un ' .
        'volume regolare di potenziali clienti che ti sarà fatturato al CPC (Costo per ' .
        'Clic).'
    ),
    69507 => array('Indicizza il tuo sito su Twenga'),
    69517 => array('Attira nuovi clienti'),
    71967 => array('Più di 30 milioni di consumatori online ogni mese'),
    71977 => array('Aumenta le tue Vendite'),
    71987 => array('Un traffico performante pilotato per convertire e massimizzare i tuoi profitti'),
    71997 => array('Ottimizza i tuoi Costi'),
    72007 => array('Dei CPC dinamici aggiustati in tempo reale secondo il potenziale di conversione'),
    81937 => array(
        'Dopo l’attivazione di Smart LEADS, il Cost of Sales delle nostre campagne è ' .
        'stato diviso per due. Questo risultato ci ha convinto della pertinenza ' .
        'dell’algoritmo e ci ha incoraggiato ad investire in Twenga.'
    ),
    81947 => array('Grégoire Firome, Acquisition Project Leader - PriceMinister'),
    84537 => array('Installare Twenga Solutions'),
    84547 => array('Crea il tuo account'),
    84557 => array('Configura il tuo account'),
    84577 => array('Completa l\'installazione del modulo  Twenga Solutions'),
    84587 => array('Possiedo già un account Twenga Solutions'),
    84597 => array('Non possiedo un account Twenga Solutions'),
    84607 => array(
        'Possiedi già un account Twenga Solutions. Ti invitiamo a riempire i campi qui ' .
        'sotto.'
    ),
    84617 => array(
        'Non possiedi un account Twenga Solutions. Ti invitiamo a riempire i campi qui ' .
        'sotto per iniziare la tua iscrizione.'
    ),
    84627 => array(
        'Attenzione : Stiamo considerando la tua domanda. Per poter beneficiare dei ' .
        'nostri servizi occorre completare la tua iscrizione.'
    ),
    84637 => array('Completa la tua iscrizione'),
    84647 => array('Congratulazioni, hai installato il Tracking Twenga !'),
    84657 => array('Con il Tracking Twenga'),
    84667 => array(
        'Misuro la qualità del mio traffico seguendo i miei tassi di conversione ed i ' .
        'miei costi acquisizione per categoria.'
    ),
    84677 => array(
        'Ottimizzo il mio budget privilegiando le offerte più performanti grazie alle ' .
        'regole automatiche Twenga.'
    ),
    84687 => array(
        'Assicuro le mie performance grazie all\'analisi proattiva e alle ' .
        'raccomandazioni del team Twenga.'
    ),
    84697 => array('L\'url del tuo feed catalogo è stato generato :'),
    84727 => array('Accedi all\'interfaccia'),
    84737 => array(
        'Grazie al tuo account Twenga Solutions, potrai godere di un\'intera gamma di ' .
        'strumenti marketing e analitici.'
    ),
    84927 => array('Passaggio %step% :'),
    85297 => array('Clienti:'),
    86117 => array('Acquisizione avanzata del traffico su Twenga'),
    86127 => array(
        'Questa soluzione integra il tuo feed prodotto sul motore di ricerca Twenga e su ' .
        'più di 1500 siti partner, comparatori di prezzo, guide d\'acquisto e siti ' .
        'editoriali.'
    ),
);

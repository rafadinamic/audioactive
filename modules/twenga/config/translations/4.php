<?php
/**
  * Copyright (c) 2016 Twenga
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  * OR OTHER DEALINGS IN THE SOFTWARE.
  * 
  * @author    Twenga
  * @copyright 2016 Twenga
  * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
  */
return array(
    7441 => array('¿Has olvidado la contraseña?'),
    7445 => array('Confían en nosotros'),
    7516 => array('Contraseña'),
    7519 => array('Por favor escribe tu correo electrónico:'),
    7521 => array('Aceptar'),
    10371 => array('Teléfono'),
    21101 => array('URL de tu tienda online'),
    43632 => array('Nombre'),
    43642 => array('Apellidos'),
    43662 => array('Correo electrónico'),
    64067 => array('Login Twenga Solutions (tu email)'),
    64077 => array('Contraseña Twenga Solutions'),
    64107 => array('Tu catálogo será indexado en aproximadamente 72 horas.'),
    64117 => array(
        'En cuanto tus productos aparezcan en línea, comenzarás a recibir tráfico ' .
        'calificado proveniente de Twenga. Este será facturado al CPC (Coste por Clic).'
    ),
    69507 => array('Referencia tu tienda online en Twenga'),
    69517 => array('Atrae nuevos clientes'),
    71967 => array('Más de 30 millones de e-consumidores cada mes'),
    71977 => array('Incrementa tus Ventas'),
    71987 => array('Un tráfico gestionado para generar conversiones y maximizar tus ingresos'),
    71997 => array('Optimiza tus Gastos'),
    72007 => array('CPC dinámicos ajustados en tiempo real en función del potencial de conversión'),
    81937 => array(
        'Desde la activación de Smart LEADS, el coste de adquisición de nuestra campaña ' .
        'se ha visto reducido a la mitad. Este resultado nos ha convencido de la ' .
        'relevancia de este algoritmo y ha jugado un rol importante en la decisión de ' .
        'invertir más aún con Twenga.'
    ),
    81947 => array('Grégoire Firome, Acquisition Project Leader - PriceMinister'),
    84537 => array('Instalar Twenga Solutions'),
    84547 => array('Crea tu cuenta'),
    84557 => array('Configura tu cuenta'),
    84577 => array('Termina la instalación del módulo Twenga Solutions'),
    84587 => array('Ya tengo una cuenta en Twenga Solutions'),
    84597 => array('No tengo cuenta en Twenga Solutions'),
    84607 => array(
        'Ya tienes una cuenta en Twenga Solutions. Por favor, rellena los campos que ' .
        'encontrarás a continuación.'
    ),
    84617 => array(
        'No tienes cuenta en Twenga Solutions. Por favor, rellena los campos que ' .
        'encontrarás a continuación para comenzar tu inscripción.'
    ),
    84627 => array(
        'Importante: Tu petición ha sido tomada en cuenta. Para poder beneficiarte de ' .
        'nuestros servicios tienes que finalizar tu inscripción.'
    ),
    84637 => array('Termina tu inscripción'),
    84647 => array('¡Enhorabuena! Has instalado correctamente el Tracking Twenga.'),
    84657 => array('Con el Tracking Twenga'),
    84667 => array(
        'Puedes medir la calidad de tu tráfico siguiendo tu tasa de conversión y tus ' .
        'costes de adquisición por categoría.'
    ),
    84677 => array(
        'Puedes optimizar tu presupuesto, privilegiando las ofertas de mayor rendimiento ' .
        'gracias a las reglas automáticas de Twenga.'
    ),
    84687 => array(
        'Puedes asegurar tu rendimiento gracias a un seguimiento proactivo y a los ' .
        'consejos del equipo de Twenga.'
    ),
    84697 => array('La url de tu feed de productos se ha generado correctamente:'),
    84727 => array('Accede a tu interfaz'),
    84737 => array(
        'Ponemos a tu disposición, en tu cuenta Twenga Solutions, un conjunto completo ' .
        'de herramientas de análisis y marketing.'
    ),
    84927 => array('Paso %step%:'),
    85297 => array('Clientes:'),
    86117 => array('Adquisición de tráfico avanzada en la red Twenga'),
    86127 => array(
        'Esta solución integra tu feed de productos en los motores de búsqueda Twenga, ' .
        'así como en más de 1.500 sitios web asociados, como comparadores de precios, ' .
        'guías de compra o webs editoriales.'
    ),
);

<?php
/**
  * Copyright (c) 2016 Twenga
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  * OR OTHER DEALINGS IN THE SOFTWARE.
  * 
  * @author    Twenga
  * @copyright 2016 Twenga
  * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
  */
return array(
    7441 => array('Zapomniałeś hasła?'),
    7445 => array('Oni nam zaufali'),
    7516 => array('Hasło'),
    7519 => array('Wpisz swój adres e-mail:'),
    7521 => array('Potwierdź'),
    10371 => array('Telefon'),
    21101 => array('URL Twojej strony'),
    43632 => array('Imię'),
    43642 => array('Nazwisko'),
    43662 => array('Adres e-mail'),
    64067 => array('Login Twenga Solutions (Twój e-mail)'),
    64077 => array('Hasło Twenga Solutions'),
    64107 => array('Twój katalog zostanie zintegrowany w ciągu około 72 godzin.'),
    64117 => array(
        'Od momentu, kiedy Twoje produkty zaczną być widoczne, zaczniesz regularnie ' .
        'otrzymywać kwalifikowany ruch, za który naliczona będzie opłata w modelu CPC ' .
        '(Koszt za kliknięcie).'
    ),
    69507 => array('Zintegruj swój sklep z Twenga'),
    69517 => array('przyciągnij nowych klientów'),
    71967 => array('Ponad 30 milionów e-klientów miesięcznie'),
    71977 => array('Zwiększ swoją sprzedaż'),
    71987 => array('Wydajny ruch nakierowany na maksymalną konwersję i zwiększenie zysków'),
    71997 => array('Optymalizuj swoje wydatki'),
    72007 => array(
        'Dynamiczne CPC obliczane w czasie rzeczywistym z uwzględnieniem potencjału ' .
        'konwersji'
    ),
    81937 => array(
        'Od aktywacji Smart LEADS, Cost of Sales naszej kampanii spadł o&nbsp;połowę.' .
        ' Ten wynik przekonał nas o&nbsp;działaniu algorytmu i&nbsp;zachęcił do ' .
        'zainwestowania więcej we współpracę z&nbsp;Twenga.'
    ),
    81947 => array('Grégoire Firome, Acquisition Project Leader - PriceMinister'),
    84537 => array('Zainstaluj Twenga Solutions'),
    84547 => array('Utwórz konto'),
    84557 => array('Skonfiguruj konto'),
    84577 => array('Sfinalizuj instalację modułu Twenga Solutions'),
    84587 => array('Mam już konto Twenga Solutions'),
    84597 => array('Nie mam jeszcze konta Twenga Solutions'),
    84607 => array('Posiadasz już konto Twenga Solutions. Prosimy o wypełnienie poniższych pól.'),
    84617 => array(
        'Nie posiadasz jeszcze konta Twenga Solutions. Prosimy o wypełnienie poniższych ' .
        'pól, aby rozpocząć rejestrację.'
    ),
    84627 => array(
        'Uwaga: Twoja prośba została poprawnie zapisana. Aby korzystać z naszych usług, ' .
        'musisz jeszcze ukończyć proces rejestracji.'
    ),
    84637 => array('Dokończ proces rejestracji'),
    84647 => array('Gratulacje, zainstalowałeś właśnie Tracking Twenga!'),
    84657 => array('Dzięki Trackingowi Twenga'),
    84667 => array(
        'Mierzę jakość mojego ruchu śledząc współczynnik konwersji oraz koszty ' .
        'pozyskiwania na poziomie kategorii.'
    ),
    84677 => array(
        'Optymalizuję mój budżet dzięki automatycznym zasadom Twenga, które faworyzują ' .
        'najefektywniejsze oferty.'
    ),
    84687 => array(
        'Upewniam się o stabilności moich wyników dzięki aktywnemu monitoringowi i ' .
        'rekomendacjom zespołu Twenga.'
    ),
    84697 => array('URL Twojego feedu produktowego został poprawnie wygenerowany:'),
    84727 => array('Przejdź do interfejsu'),
    84737 => array(
        'Twoje konto Twenga Solutions da Ci dostęp do kompletu narzędzi analitycznych i ' .
        'marketingowych.'
    ),
    84927 => array('Etap %step%:'),
    85297 => array('Klienci:'),
    86117 => array('Zaawansowane pozyskiwanie ruchu w sieci Twenga'),
    86127 => array(
        'To rozwiązanie indeksuje Twój katalog produktów w wyszukiwarce Twenga oraz w ' .
        'ponad 1500 stron partnerskich takich jak porównywarki cenowe, poradniki ' .
        'zakupowe oraz strony wydawców.'
    ),
);

<?php
/**
  * Copyright (c) 2016 Twenga
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  * OR OTHER DEALINGS IN THE SOFTWARE.
  * 
  * @author    Twenga
  * @copyright 2016 Twenga
  * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
  */
return array(
    7441 => array('Passwort vergessen?'),
    7445 => array('Unsere Referenzen'),
    7516 => array('Passwort'),
    7519 => array('Bitte geben Sie Ihre E-Mail-Adresse ein'),
    7521 => array('Bestätigen'),
    10371 => array('Telefon'),
    21101 => array('URL Ihrer Website'),
    43632 => array('Vorname'),
    43642 => array('Name'),
    43662 => array('E-Mail-Adresse'),
    64067 => array('Ihr Twenga Solutions Login (Ihre E-Mail Adresse)'),
    64077 => array('Ihr Twenga Solutions Passwort'),
    64107 => array('Ihre Produkte werden in etwa 72 Stunden gelistet sein'),
    64117 => array(
        'Sobald Ihre Produkte online sind, werden Sie von Twenga regelmäßigen und ' .
        'qualifizierten Traffic erhalten, der Ihnen per CPC (Cost per Click) berechnet ' .
        'wird.'
    ),
    69507 => array('Indexieren Sie Ihre Seite auf Twenga'),
    69517 => array('Gewinnen Sie neue Kunden'),
    71967 => array('Mehr als 10 Millionen Online-Shopper pro Monat'),
    71977 => array('Erhöhen Sie Ihre Verkäufe'),
    71987 => array('Ein performance-starker Traffic, der optimal konvertiert für maximale Einnahmen'),
    71997 => array('Optimieren Sie Ihre Kosten'),
    72007 => array(
        'Dynamische CPCs, die je nach aktuellem Konversionspotential ständig angepasst ' .
        'werden'
    ),
    81937 => array(
        'Seit der Aktivierung von Smart LEADS wurde die Kosten-Umsatz-Relation unserer ' .
        'Kampagnen halbiert. Dieses Ergebnis hat uns die Wirksamkeit der Algorithmen ' .
        'gezeigt und uns überzeugt, weiter in Kampagnen mit Twenga zu investieren.'
    ),
    81947 => array('Grégoire Firome, Acquisition Project Leader - PriceMinister'),
    84537 => array('Twenga Solutions installieren'),
    84547 => array('Ihr Konto erstellen'),
    84557 => array('Ihr Konto einrichten'),
    84577 => array('Installation des Twenga Solutions Moduls beenden'),
    84587 => array('Ich habe bereits ein Twenga Solutions Konto'),
    84597 => array('Ich habe kein Twenga Solutions Konto'),
    84607 => array(
        'Sie haben bereits ein Twenga Solutions Konto. Bitte füllen Sie folgende Felder ' .
        'aus.'
    ),
    84617 => array(
        'Sie haben noch kein Twenga Solutions Konto. Bitte füllen Sie folgende Felder ' .
        'aus, um sich anzumelden.'
    ),
    84627 => array(
        'Achtung: Wir haben Ihre Anfrage erhalten. Bitte beenden Sie Ihre Anmeldung, um ' .
        'von unseren Dienstleistungen zu profitieren.'
    ),
    84637 => array('Beenden Sie Ihre Anmeldung'),
    84647 => array('Herzlichen Glückwunsch, Sie haben das Twenga-Tracking erfolgreich installiert!'),
    84657 => array('Mit dem Twenga-Tracking'),
    84667 => array(
        'Messe ich die Qualität meines Traffics durch die Verfolgung meiner ' .
        'Konversionsraten und meiner Akquisitionskosten pro Kategorie.'
    ),
    84677 => array(
        'Optimiere ich mein Budget durch die Bevorzugung der am besten performenden ' .
        'Angebote, dank der automatischen Regeln von Twenga.'
    ),
    84687 => array(
        'Sichere ich meine Performance, dank des proaktiven Trackings und der ' .
        'Empfehlungen von Twenga.'
    ),
    84697 => array('Ihre Feed-URL wurde erfolgreich generiert:'),
    84727 => array('Zu Ihrer Benutzeroberfläche'),
    84737 => array(
        'In Ihrem Twenga Solutions Konto profitieren Sie von einem kompletten Marketing- ' .
        'und Analysetool.'
    ),
    84927 => array('Schritt %step%:'),
    85297 => array('Kunden:'),
    86117 => array('Fortschrittliche Traffic-Akquise mit dem Twenga-Netzwerk'),
    86127 => array(
        'Diese Lösung integriert Ihren Produktfeed auf den Twenga-Suchmaschinen sowie ' .
        'auf über 1.500 Partnerseiten, wie beispielsweise Preisvergleiche, ' .
        'Shopping-Guides und Publisher-Seiten.'
    ),
);

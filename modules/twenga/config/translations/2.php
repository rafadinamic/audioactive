<?php
/**
  * Copyright (c) 2016 Twenga
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  * OR OTHER DEALINGS IN THE SOFTWARE.
  * 
  * @author    Twenga
  * @copyright 2016 Twenga
  * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
  */
return array(
    7441 => array('Mot de passe oublié ?'),
    7445 => array('Ils nous font déjà confiance'),
    7516 => array('Mot de passe'),
    7519 => array('Veuillez saisir votre adresse e-mail :'),
    7521 => array('Valider'),
    10371 => array('Téléphone'),
    21101 => array('URL de votre site'),
    43632 => array('Prénom'),
    43642 => array('Nom'),
    43662 => array('Adresse e-mail'),
    64067 => array('Login Twenga Solutions (votre email)'),
    64077 => array('Mot de passe Twenga Solutions '),
    64107 => array('Votre catalogue sera référencé sous 72H environ. '),
    64117 => array(
        'Une fois vos produits publiés, vous recevrez un apport régulier et qualifié ' .
        'd’acheteurs qui vous sera facturé au CPC (Coût par Clic).'
    ),
    69507 => array('Référencez votre site sur Twenga'),
    69517 => array('Attirez de nouveaux clients'),
    71967 => array('Plus de 30 millions d\'e-consommateurs chaque mois'),
    71977 => array('Augmentez vos Ventes'),
    71987 => array('Un trafic performant piloté pour convertir et maximiser votre revenu'),
    71997 => array('Optimisez vos Coûts'),
    72007 => array('Des CPC dynamiques ajustés en temps réel selon le potentiel de conversion'),
    81937 => array(
        'Depuis l’activation de Smart LEADS, le Cost of Sales de notre campagne a été ' .
        'divisé par deux. Ce résultat nous a convaincu de la pertinence
de l’algorithme ' .
        'et nous encourage à investir davantage chez Twenga.'
    ),
    81947 => array('Grégoire Firome, Acquisition Project Leader - PriceMinister'),
    84537 => array('Installer Twenga Solutions'),
    84547 => array('Créer votre compte'),
    84557 => array('Configurer votre compte'),
    84577 => array('Finaliser l\'installation du module Twenga Solutions'),
    84587 => array('J\'ai déjà un compte Twenga Solutions'),
    84597 => array('Je n\'ai pas de compte Twenga Solutions'),
    84607 => array(
        'Vous avez déjà un compte Twenga Solutions. Nous vous invitons à renseigner les ' .
        'champs ci-dessous.'
    ),
    84617 => array(
        'Vous n\'avez pas de compte Twenga Solutions. Nous vous invitons à renseigner ' .
        'les champs ci-dessous afin de commencer votre inscription.'
    ),
    84627 => array(
        'Attention : Nous avons bien pris en compte votre demande, afin de bénéficier de ' .
        'nos services vous devez finaliser votre inscription.'
    ),
    84637 => array('Finalisez votre inscription'),
    84647 => array('Félicitations, vous avez bien installé le Tracking Twenga !'),
    84657 => array('Avec le Tracking Twenga'),
    84667 => array(
        'Je mesure la qualité de mon trafic en suivant mes taux de conversion et mes ' .
        'coûts d’acquisitions par catégorie.'
    ),
    84677 => array(
        'J’optimise mon budget en privilégiant les offres les plus performantes grâce ' .
        'aux règles automatiques Twenga.'
    ),
    84687 => array(
        'Je sécurise ma performance grâce au suivi proactif et aux recommandations des ' .
        'équipes Twenga.'
    ),
    84697 => array('Votre url de flux catalogue a bien été généré :'),
    84727 => array('Accéder à votre interface'),
    84737 => array(
        'Vous bénéficierez depuis votre compte Twenga solutions d’une suite complète ' .
        'd’outils marketing et analytiques.'
    ),
    84927 => array('Etape %step% :'),
    85297 => array('Clients :'),
    86117 => array('Acquisition avancée de trafic sur le réseau Twenga'),
    86127 => array(
        'Cette solution intègre votre flux produit sur les moteurs de recherche Twenga ' .
        'ainsi que sur plus de 1500 sites partenaires tels que des comparateurs de prix, ' .
        'des guides d\'achat et des sites éditoriaux.'
    ),
);

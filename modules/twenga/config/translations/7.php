<?php
/**
  * Copyright (c) 2016 Twenga
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  * OR OTHER DEALINGS IN THE SOFTWARE.
  * 
  * @author    Twenga
  * @copyright 2016 Twenga
  * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
  */
return array(
    7441 => array('Wachtwoord vergeten?'),
    7445 => array('Onze referenties'),
    7516 => array('Wachtwoord'),
    7519 => array('Gelieve uw e-mailadres in te vullen:'),
    7521 => array('Bevestigen'),
    10371 => array('Telefoon'),
    21101 => array('Je site URL'),
    43632 => array('Voornaam'),
    43642 => array('Achternaam'),
    43662 => array('Email-adres'),
    64067 => array('Twenga Solutions log-in (uw emailadres)'),
    64077 => array('Twenga Solutions wachtwoord'),
    64107 => array('Uw catalogus wordt binnen ongeveer 72 uur online zichtbaar.'),
    64117 => array(
        'Wanneer uw producten op Twenga staan ontvangt uw site een traffic flow die u ' .
        'volgens het CPC-principe (cost per click) zal worden gefactureerd.'
    ),
    69507 => array('Indexeer je webwinkel op Twenga'),
    69517 => array('Krijg nieuwe klanten'),
    71967 => array('Maandelijks meer dan 30 miljoen online consumenten'),
    71977 => array('Verhoog uw verkoop'),
    71987 => array(
        'Kwalitatieve traffic, geoptimaliseerd voor het verhogen van uw conversie en ' .
        'opbrengst'
    ),
    71997 => array('Gebruik uw budget zo slim mogelijk'),
    72007 => array('CPC\'s op basis van real time conversiepotentieel'),
    81937 => array(
        'Sinds we Smart LEADS hebben geactiveerd is de Cost of Sales van onze campagne ' .
        'gehalveerd. Dit resultaat heeft ons overtuigd van de werking van het algoritme ' .
        'en ons doen besluiten om verder te investeren met Twenga.'
    ),
    81947 => array('Grégoire Firome, Acquisition Project Leader - PriceMinister'),
    84537 => array('Twenga Solutions installeren'),
    84547 => array('Maak je account aan'),
    84557 => array('Configureer je account'),
    84577 => array('Voltooi de installatie van de Twenga Solutions module'),
    84587 => array('Ik heb al een Twenga Solutions account'),
    84597 => array('Ik heb geen Twenga Solutions account'),
    84607 => array('Je hebt al een Twenga Solutions account. Vul de velden hieronder in.'),
    84617 => array(
        'Je hebt nog geen Twenga Solutions account. We vragen je om de onderstaande ' .
        'velden in te vullen zodat je je inschrijving kan beginnen.'
    ),
    84627 => array(
        'Opgelet: We hebben je aanvraag ontvangen. Voltooi je inschrijving om gebruik te ' .
        'kunnen maken van onze services.'
    ),
    84637 => array('Voltooi je inschrijving'),
    84647 => array('Gefeliciteerd, je hebt de Twenga Tracking succesvol geïnstalleerd!'),
    84657 => array('Met de Twenga Tracking'),
    84667 => array(
        'Ik meet de kwaliteit van mijn traffic door mijn conversieratio en de Cost of ' .
        'Sales per categorie te volgen.'
    ),
    84677 => array(
        'Ik benut mijn budget optimaal door voorrang te geven aan de best presterende ' .
        'aanbiedingen, dankzij de automatische instellingen van Twenga.'
    ),
    84687 => array(
        'Ik ben verzekerd van een goede performance dankzij de proactieve ondersteuning ' .
        'van Twenga\'s team.'
    ),
    84697 => array('De URL van je productfeed is succesvol aangemaakt:'),
    84727 => array('Ga naar je interface'),
    84737 => array(
        'Via je Twenga Solutions account profiteer je van uitgebreide marketing- en ' .
        'analysemogelijkheden.'
    ),
    84927 => array('Stap %step%:'),
    85297 => array('Klanten:'),
    86117 => array('Ontvang kwaliteitstraffic via het Twenga netwerk'),
    86127 => array(
        'Deze oplossing integreert je productfeeds op Twenga\'s zoekmachines en op meer ' .
        'dan 1500 partnersites zoals prijsvergelijkwebsites, shopping guides en ' .
        'publisher websites.'
    ),
);

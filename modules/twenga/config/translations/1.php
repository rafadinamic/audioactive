<?php
/**
  * Copyright (c) 2016 Twenga
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  * OR OTHER DEALINGS IN THE SOFTWARE.
  * 
  * @author    Twenga
  * @copyright 2016 Twenga
  * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
  */
return array(
    7441 => array('Forgot your password?'),
    7445 => array('Our clients'),
    7516 => array('Password'),
    7519 => array('Please enter your email address:'),
    7521 => array('Submit'),
    10371 => array('Telephone'),
    21101 => array('Your site URL'),
    43632 => array('Name'),
    43642 => array('Surname'),
    43662 => array('Email address'),
    64067 => array('Twenga Solutions username (your email)'),
    64077 => array('Twenga Solutions password'),
    64107 => array('Your catalogue will be listed in approximately 72 hours.'),
    64117 => array(
        'Once your products appear on Twenga, you will receive additional traffic which ' .
        'will by billed using CPC (cost per click).'
    ),
    69507 => array('List your site on Twenga'),
    69517 => array('Attract new clients'),
    71967 => array('More than 30 million e-consumers every month'),
    71977 => array('Increase your Sales'),
    71987 => array('High-performing traffic driven to convert and maximise your revenue'),
    71997 => array('Optimise your costs'),
    72007 => array('Dynamic CPC which is adjusted in real time according to its potential conversion'),
    81937 => array(
        'Since activating Smart LEADS, our campaign Cost of Sales has been halved.' .
        ' This result has convinced us of the efficiency of this algorithm and has ' .
        'played a role in our decision to invest further with Twenga.'
    ),
    81947 => array('Grégoire Firome, Acquisition Project Leader - PriceMinister'),
    84537 => array('Install Twenga Solutions'),
    84547 => array('Create your account'),
    84557 => array('Configure your account'),
    84577 => array('Finalise your Twenga Solutions module installation'),
    84587 => array('I already have a Twenga Solutions account'),
    84597 => array('I don\'t have a Twenga Solutions account'),
    84607 => array('You already have a Twenga Solutions account. Please fill in the fields below.'),
    84617 => array(
        'You do not have a Twenga Solutions account. Please fill in the fields below in ' .
        'order to begin your subscription.'
    ),
    84627 => array(
        'Warning: We have now taken your request into account. In order to benefit from ' .
        'our services, you must finalise your subscription.'
    ),
    84637 => array('Finalise your subscription'),
    84647 => array('Congratulations, you have now installed Twenga Tracking!'),
    84657 => array('With Twenga Tracking'),
    84667 => array(
        'I can measure the quality of my traffic by following my conversion rates and ' .
        'acquisition costs per category.'
    ),
    84677 => array(
        'I can optimise my budget by prioritising the highest performing offers, thanks ' .
        'to Twenga\'s automatic settings.'
    ),
    84687 => array(
        'I can secure my performance thanks to proactive monitoring and recommendations ' .
        'from the Twenga teams.'
    ),
    84697 => array('Your feed URL has now been created:'),
    84727 => array('Continue to your interface'),
    84737 => array(
        'Benefit from a complete range of marketing and analytics tools with your Twenga ' .
        'Solutions account.'
    ),
    84927 => array('Step %step%:'),
    85297 => array('Clients:'),
    86117 => array('Advanced traffic acquisition on the Twenga network'),
    86127 => array(
        'The solution integrates your product feed with Twenga\'s search engines, as ' .
        'well as on more than 1500 partner sites such as price comparators, shopping ' .
        'guides and editorial websites.'
    ),
);

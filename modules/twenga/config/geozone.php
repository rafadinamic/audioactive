<?php
/**
  * Copyright (c) 2016 Twenga
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  * OR OTHER DEALINGS IN THE SOFTWARE.
  * 
  * @author    Twenga
  * @copyright 2016 Twenga
  * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
  */

return array(
    'FR' => array(
        'tw_code' => 'FR',
        'name' => 'France',
        'lang' => 'fr',
        'language_id' => 2
    ),
    'DE' => array(
        'tw_code' => 'DE',
        'name' => 'Deutschland',
        'lang' => 'de',
        'language_id' => 3
    ),
    'ES' => array(
        'tw_code' => 'ES',
        'name' => 'España',
        'lang' => 'es',
        'language_id' => 4
    ),
    'IT' => array(
        'tw_code' => 'IT',
        'name' => 'Italia',
        'lang' => 'it',
        'language_id' => 5
    ),
    'NL' => array(
        'tw_code' => 'NL',
        'name' => 'Nederland',
        'lang' => 'nl',
        'language_id' => 7
    ),
    'PL' => array(
        'tw_code' => 'PL',
        'name' => 'Polska',
        'lang' => 'pl',
        'language_id' => 6
    ),
    'GB' => array(
        'tw_code' => 'UK',
        'name' => 'United Kingdom',
        'lang' => 'en',
        'language_id' => 1
    )
);

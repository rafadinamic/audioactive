<?php
/**
  * Copyright (c) 2016 Twenga
  * 
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  * 
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  * 
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  * OR OTHER DEALINGS IN THE SOFTWARE.
  * 
  * @author    Twenga
  * @copyright 2016 Twenga
  * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
  */

return array(

    /**
     * Config EN
     */
    1 => array(
        'TR_FIRM_LONG_URL' => 'https://www.twenga-solutions.com/en',
        'TR_FIRM_URL' => 'www.twenga-solutions.com/en',
        'TR_FIRM_SHORT_URL' => 'Twenga.co.uk',
        'TR_FIRM_EMAIL' => '@twenga.com',
        'TR_FIRM_NAME' => 'Twenga'
    ),

    /**
     * Config FR
     */
    2 => array(
        'TR_FIRM_LONG_URL' => 'http://www.twenga.fr',
        'TR_FIRM_URL' => 'www.twenga.fr',
        'TR_FIRM_SHORT_URL' => 'Twenga.fr',
        'TR_FIRM_EMAIL' => '@twenga.com',
        'TR_FIRM_NAME' => 'Twenga'
    ),

    /**
     * Config BR
     */
    8 => array(
        'TR_FIRM_LONG_URL' => 'http://www.twenga.com.br',
        'TR_FIRM_URL' => 'www.twenga.com.br',
        'TR_FIRM_SHORT_URL' => 'Twenga.com.br',
        'TR_FIRM_EMAIL' => '@twenga.com',
        'TR_FIRM_NAME' => 'Twenga'
    ),

    /**
     * Config DE
     */
    3 => array(
        'TR_FIRM_LONG_URL' => 'http://www.twenga.de',
        'TR_FIRM_URL' => 'www.twenga.de',
        'TR_FIRM_SHORT_URL' => 'Twenga.de',
        'TR_FIRM_EMAIL' => '@twenga.com',
        'TR_FIRM_NAME' => 'Twenga'
    ),

    /**
     * Config IT
     */
    5 => array(
        'TR_FIRM_LONG_URL' => 'http://www.twenga.it',
        'TR_FIRM_URL' => 'www.twenga.it',
        'TR_FIRM_SHORT_URL' => 'Twenga.it',
        'TR_FIRM_EMAIL' => '@twenga.com',
        'TR_FIRM_NAME' => 'Twenga'
    ),

    /**
     * Config ES
     */
    4 => array(
        'TR_FIRM_LONG_URL' => 'http://www.twenga.es',
        'TR_FIRM_URL' => 'www.twenga.es',
        'TR_FIRM_SHORT_URL' => 'Twenga.es',
        'TR_FIRM_EMAIL' => '@twenga.com',
        'TR_FIRM_NAME' => 'Twenga'
    ),

    /**
     * Config NL
     */
    7 => array(
        'TR_FIRM_LONG_URL' => 'http://www.twenga.nl',
        'TR_FIRM_URL' => 'www.twenga.nl',
        'TR_FIRM_SHORT_URL' => 'Twenga.nl',
        'TR_FIRM_EMAIL' => '@twenga.com',
        'TR_FIRM_NAME' => 'Twenga'
    ),

    /**
     * Config US
     */
    11 => array(
        'TR_FIRM_LONG_URL' => 'http://www.twenga.com',
        'TR_FIRM_URL' => 'www.twenga.com',
        'TR_FIRM_SHORT_URL' => 'Twenga.com',
        'TR_FIRM_EMAIL' => '@twenga.com',
        'TR_FIRM_NAME' => 'Twenga'
    ),

    /**
     * Config RU
     */
    9 => array(
        'TR_FIRM_LONG_URL' => 'http://www.twenga.ru',
        'TR_FIRM_URL' => 'www.twenga.ru',
        'TR_FIRM_SHORT_URL' => 'Twenga.ru',
        'TR_FIRM_EMAIL' => '@twenga.com',
        'TR_FIRM_NAME' => 'Twenga'
    ),

    /**
     * Config PL
     */
    6 => array(
        'TR_FIRM_LONG_URL' => 'http://www.twenga.pl',
        'TR_FIRM_URL' => 'www.twenga.pl',
        'TR_FIRM_SHORT_URL' => 'Twenga.pl',
        'TR_FIRM_EMAIL' => '@twenga.com',
        'TR_FIRM_NAME' => 'Twenga'
    ),

    /**
     * Config AU
     */
    12 => array(
        'TR_FIRM_LONG_URL' => 'http://www.twenga.com.au',
        'TR_FIRM_URL' => 'www.twenga.com.au',
        'TR_FIRM_SHORT_URL' => 'Twenga.com.au',
        'TR_FIRM_EMAIL' => '@twenga.com',
        'TR_FIRM_NAME' => 'Twenga'
    ),
);

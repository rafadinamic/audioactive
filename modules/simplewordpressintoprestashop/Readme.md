Copyright (C) 2014 by NarceaDigital

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

About
=====
+ Simple WordPress Into Prestashop by NarceaDigital
+ Version 1.0.1

System Requirements
===================
+ PrestaShop 1.6.x
  
Configuration Instructions
==========================
    1. Upload files to your PrestaShop installation.

    2. Go to your PrestaShop administration Modules list.
    3. In module list, install Simple WordPress Into Prestashop
    4. Define, in module configuration page, internal path to WordPress installation and max WordPress posts to shown in PrestaShop
    5. Define if date must be shown and paragraph size for date, title and content
    6. Put a Shortcode in a CMS Page like [SWIP_Posts]
    7. Load that CMS Page

Shortcode Examples

Show all posts (limited by max posts value in module configuration page): [SWIP_Posts]
Show a unique post: [SWIP_Post_idpost], by example: [SWIP_Post_23]
Show a unique page: [SWIP_Page_idpage], by example: [SWIP_Page_23]
Show only title unformatted of a unique post: [SWIP_Post_idpage_Title], by example: [SWIP_Post_23_Title]
Show only title unformatted of a unique page: [SWIP_Page_idpage], by example: [SWIP_Page_23_Title]

### Tested with:

+ PrestaShop™ 1.6.0.0 - 1.6.0.14
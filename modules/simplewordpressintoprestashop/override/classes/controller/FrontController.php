<?php
/**
* 2014 NarceaDigital
*
* Simple WordPress Into Prestashop
*
* NOTICE OF LICENSE
*
*  @author    NarceaDigital prestashop@narcea.com
*  @copyright 2016 NarceaDigital
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  @version 1.0.7
*/

class FrontController extends FrontControllerCore
{
	 /**
     * Removed in PrestaShop 1.7
     *
     * @return bool
     */
    protected function useMobileTheme()
    {
        return false;
    }

	public function obtenerPrefijo()
	{
		$filename_cms_wp = Configuration::Get('SWIP_WordPress_Path').'/wp-config.php';

		if (file_exists($filename_cms_wp) && is_file($filename_cms_wp) && is_readable($filename_cms_wp))
		{
			$file = fopen( $filename_cms_wp, 'r' );
			$file_content = fread( $file, filesize( $filename_cms_wp ) );
			fclose( $file );
			preg_match("/table_prefix\s*=\s*'([^'.]*)';/", $file_content, $matches);
			$prefijo = $matches[1];
		}
		return ($prefijo);
	}

	public function conectarWordPress()
	{
		$filename_cms_wp = Configuration::Get('SWIP_WordPress_Path').'/wp-config.php';
		if (file_exists( $filename_cms_wp) && is_file( $filename_cms_wp) && is_readable( $filename_cms_wp))
		{
			$file = fopen($filename_cms_wp, 'r' );
			$file_content = fread($file, filesize($filename_cms_wp));
			fclose($file);
			preg_match_all('/define\s*?\(\s*?([\'"])(DB_NAME|DB_USER|DB_PASSWORD|DB_HOST)\1\s*?,\s*?([\'"])([^\3]*?)\3\s*?\)\s*?;/si',
			$file_content, $defines);
			if ((isset( $defines[2]) && !empty( $defines[2])) && (isset($defines[4]) && !empty( $defines[4])))
			{
				foreach ($defines[2] as $key => $define)
				{
					switch ($define)
					{
						case 'DB_NAME':
							$name = $defines[4][$key];
						break;
						case 'DB_USER':
							$user = $defines[4][$key];
						break;
						case 'DB_PASSWORD':
							$pass = $defines[4][$key];
						break;
						case 'DB_HOST':
							$host = $defines[4][$key];
							if (strpos($host, ':'))
								$host = Tools::substr($host, 0, strpos($host, ':'));
						break;
					}
				}
			}
			return array('host' => $host,'name' => $name,'user' => $user,'pass' => $pass,);
		}
	}

		public function CustomSWIPDispatcherPosts($all_posts, $show_only_title, $pages_or_posts, $swip_post)
	{
		$datos_wordpress = FrontController::conectarWordPress();
		$db_host = $datos_wordpress['host'];
		$db_database = $datos_wordpress['name'];
		$db_username = $datos_wordpress['user'];
		$db_password = $datos_wordpress['pass'];
		$link = mysqli_connect($db_host, $db_username, $db_password, $db_database);
		if (mysqli_connect_errno())
		{
			printf('Connection to WordPress failed. Please disable SWIP module and before activating, configure WordPress Path correctly.');
			die();
		}
		if ($all_posts)
			$query = 'Select * FROM '.FrontController::obtenerPrefijo().
		'posts WHERE post_type = \'post\' AND post_status = \'publish\' ORDER BY ID DESC LIMIT '.Configuration::Get('SWIP_maxPosts');
		else
		{
			if ($pages_or_posts == 'posts')
				$query = 'Select * FROM '.FrontController::obtenerPrefijo().
			'posts WHERE post_type = \'post\' AND post_status = \'publish\' AND ID = \''.$swip_post.'\' LIMIT 1';
			else
				$query = 'Select * FROM '.FrontController::obtenerPrefijo().
			'posts WHERE post_type = \'page\' AND post_status = \'publish\' AND ID = \''.$swip_post.'\' LIMIT 1';
		}
		$result = $link->query($query);
		$salida = '';
		$salida = '<div class="posts"><div class="container"><div class="post-grid">';
		while ($row = mysqli_fetch_array($result))
		{
			$post_id = $row['ID'];
			$post_author = $row['post_author'];
			$post_url = $row['guid'];
			$post_image = "";
			$query_post_image = 'Select meta_value FROM '.FrontController::obtenerPrefijo().'postmeta WHERE meta_key = \'_wp_attached_file\' AND post_id IN 
								(SELECT meta_value FROM wp_postmeta WHERE post_id = \''.$post_id.'\' AND meta_key = \'_thumbnail_id\')';
			$result_post_image = $link->query($query_post_image);
			while ($row_post_image = mysqli_fetch_array($result_post_image))
				$post_image = $row_post_image['meta_value'];
			$query_user_fn = 'Select * FROM '.FrontController::obtenerPrefijo().'usermeta WHERE user_id = \''.$post_author.'\' and meta_key = \'first_name\'';
			$result_user_fn = $link->query($query_user_fn);
			while ($row_user_fn = mysqli_fetch_array($result_user_fn))
				$post_author_completo = $row_user_fn['meta_value'];
			$query_user_ln = 'Select * FROM '.FrontController::obtenerPrefijo().'usermeta WHERE user_id = \''.$post_author.'\' and meta_key = \'last_name\'';
			$result_user_ln = $link->query($query_user_ln);
			while ($row_user_ln = mysqli_fetch_array($result_user_ln))
				$post_author_completo .= ' '.$row_user_ln['meta_value'];
			if ($post_author_completo == ' ')
			{
				$query_user_vacio = 'Select * FROM '.FrontController::obtenerPrefijo().'users WHERE ID = \''.$post_author.'\'';
				$result_user_vacio = $link->query($query_user_vacio);
				while ($row_user_vacio = mysqli_fetch_array($result_user_vacio))
					$post_author_completo = $row_user_vacio['user_login'];
			}
			if (Configuration::Get('SWIP_utf8_encode') != 'Yes')
				$post_author = utf8_encode($post_author_completo);
			else $post_author = $post_author_completo;
			$post_date = $row['post_date'];
			if (Configuration::Get('SWIP_utf8_encode') != 'Yes')
				$post_content = utf8_encode($row['post_content']);
			else $post_content = $row['post_content'];
			if (!Configuration::Get('SWIP_show_LinkTitle'))
			{
				if (Configuration::Get('SWIP_utf8_encode') != 'Yes')
					$post_title = utf8_encode($row['post_title']);
				else $post_title = $row['post_title'];
			}

			//Nuestra propia forma de imprimir los post
			$post_title = utf8_encode($row['post_title']);
			$salida = $salida.
			'<a href="'.$post_url.'" class="post-card">'.
            	'<div class="marco_mobile">
            	<div class="image_blog" style = "
            			background-image: url(http://www.dinamiclifestyle.com/audioactive/blog/wp-content/uploads/'.$post_image.');
    					background-size: cover;
    					min-height: 10em;
					    background-position: center;">
    					'.
            	'</div>
            	</div>'.
            	'<h2 class="Titulo">'.$post_title.'</h2>'.
            	'<p class="Contenido">'.$post_content.'</p>'.
            '</a>';
		}
		$salida = $salida.'</div><a class="more-posts" href="http://www.audioactive.es/blog/">Más artículos</a></div></div>';
		//return $salida;
		return "OK";
	}

	public function SWIPDispatcherPosts($all_posts, $show_only_title, $pages_or_posts, $swip_post)
	{
		$datos_wordpress = $this->conectarWordPress();
		$db_host = $datos_wordpress['host'];
		$db_database = $datos_wordpress['name'];
		$db_username = $datos_wordpress['user'];
		$db_password = $datos_wordpress['pass'];
		$link = mysqli_connect($db_host, $db_username, $db_password, $db_database);
		if (mysqli_connect_errno())
		{
			printf('Connection to WordPress failed. Please disable SWIP module and before activating, configure WordPress Path correctly.');
			die();
		}
		if ($all_posts)
			$query = 'Select * FROM '.$this->obtenerPrefijo().
		'posts WHERE post_type = \'post\' AND post_status = \'publish\' ORDER BY ID DESC LIMIT '.Configuration::Get('SWIP_maxPosts');
		else
		{
			if ($pages_or_posts == 'posts')
				$query = 'Select * FROM '.$this->obtenerPrefijo().
			'posts WHERE post_type = \'post\' AND post_status = \'publish\' AND ID = \''.$swip_post.'\' LIMIT 1';
			else
				$query = 'Select * FROM '.$this->obtenerPrefijo().
			'posts WHERE post_type = \'page\' AND post_status = \'publish\' AND ID = \''.$swip_post.'\' LIMIT 1';
		}
		$result = $link->query($query);
		$salida = '';
		$salida = '<div class="posts"><div class="container"><div class="post-grid">';
		while ($row = mysqli_fetch_array($result))
		{
			$post_id = $row['ID'];
			$post_author = $row['post_author'];
			$post_url = $row['guid'];
			$post_image = "";
			$query_post_image = 'Select meta_value FROM '.$this->obtenerPrefijo().'postmeta WHERE meta_key = \'_wp_attached_file\' AND post_id IN 
								(SELECT meta_value FROM wp_postmeta WHERE post_id = \''.$post_id.'\' AND meta_key = \'_thumbnail_id\')';
			$result_post_image = $link->query($query_post_image);
			while ($row_post_image = mysqli_fetch_array($result_post_image))
				$post_image = $row_post_image['meta_value'];
			$query_user_fn = 'Select * FROM '.$this->obtenerPrefijo().'usermeta WHERE user_id = \''.$post_author.'\' and meta_key = \'first_name\'';
			$result_user_fn = $link->query($query_user_fn);
			while ($row_user_fn = mysqli_fetch_array($result_user_fn))
				$post_author_completo = $row_user_fn['meta_value'];
			$query_user_ln = 'Select * FROM '.$this->obtenerPrefijo().'usermeta WHERE user_id = \''.$post_author.'\' and meta_key = \'last_name\'';
			$result_user_ln = $link->query($query_user_ln);
			while ($row_user_ln = mysqli_fetch_array($result_user_ln))
				$post_author_completo .= ' '.$row_user_ln['meta_value'];
			if ($post_author_completo == ' ')
			{
				$query_user_vacio = 'Select * FROM '.$this->obtenerPrefijo().'users WHERE ID = \''.$post_author.'\'';
				$result_user_vacio = $link->query($query_user_vacio);
				while ($row_user_vacio = mysqli_fetch_array($result_user_vacio))
					$post_author_completo = $row_user_vacio['user_login'];
			}
			if (Configuration::Get('SWIP_utf8_encode') != 'Yes')
				$post_author = utf8_encode($post_author_completo);
			else $post_author = $post_author_completo;
			$post_date = $row['post_date'];
			if (Configuration::Get('SWIP_utf8_encode') != 'Yes')
				$post_content = utf8_encode($row['post_content']);
			else $post_content = $row['post_content'];
			if (!Configuration::Get('SWIP_show_LinkTitle'))
			{
				if (Configuration::Get('SWIP_utf8_encode') != 'Yes')
					$post_title = utf8_encode($row['post_title']);
				else $post_title = $row['post_title'];
			}
			else
				if (!Configuration::Get('SWIP_custom_LinkTitle'))
				{
					if (Configuration::Get('SWIP_utf8_encode') != 'Yes')
						$post_title = '<a href="'.$post_url.'">'.utf8_encode($row['post_title']).'</a>';
					else $post_title = '<a href="'.$post_url.'">'.$row['post_title'].'</a>';
				}
				else
				{
						if (Configuration::Get('SWIP_utf8_encode') != 'Yes')
							$post_title = '<a href="'.Configuration::Get('SWIP_custom_LinkTitle').'">'.utf8_encode($row['post_title']).'</a>';
						else $post_title = '<a href="'.Configuration::Get('SWIP_custom_LinkTitle').'">'.utf8_encode($row['post_title']).'</a>';
				}
			if (Configuration::Get('SWIP_DateTextSize') != 0)
				$post_date = '<h'.Configuration::Get('SWIP_DateTextSize').'>'.$post_date.'</h'.Configuration::Get('SWIP_DateTextSize');
			$post_date = $post_date.'<br/>';
			if (Configuration::Get('SWIP_AuthorTextSize') != 0)
				$post_author = '<h'.Configuration::Get('SWIP_AuthorTextSize').'>'.$post_author.'</h'.Configuration::Get('SWIP_AuthorTextSize');
			$post_author = $post_author.'<br/>';
			if (Configuration::Get('SWIP_TitleTextSize') != 0)
				$post_title = '<h'.Configuration::Get('SWIP_TitleTextSize').'>'.$post_title.'</h'.Configuration::Get('SWIP_TitleTextSize').'>';
			$post_title = $post_title.'<br/>';
			if (Configuration::Get('SWIP_TitleStrong') != 0)
				$post_title = '<strong>'.$post_title.'</strong>';
			$post_content = '<br/>'.$post_content;
			if (Configuration::Get('SWIP_ContentTextSize') != 0)
				$post_content = '<h'.Configuration::Get('SWIP_ContentTextSize').'>'.$post_content.'</h'.Configuration::Get('SWIP_ContentTextSize').'>';
			$post_content = $post_content.'<br/>';
			if (!Configuration::Get('SWIP_showDate'))
				$post_date = '';
			if (!Configuration::Get('SWIP_showAuthor'))
				$post_author = '';
			if (!Configuration::Get('SWIP_showContent'))
				$post_content = '';
			if ($show_only_title)
				$salida .= $post_title;
			else
				$salida .= $post_date.$post_author.$post_title.$post_content;

		}
		$salida = $salida.'</div><a class="more-posts" href="http://www.audioactive.es/blog/">Más artículos</a></div></div>';
		return $salida;
	}

	protected function smartyOutputContent($content)
	{
		$this->context->cookie->write();

		$html = '';
		$js_tag = 'js_def';
		$this->context->smarty->assign($js_tag, $js_tag);
		if (is_array($content))
		{
			foreach ($content as $tpl)
				$html .= $this->context->smarty->fetch($tpl);
		}
		else $html = $this->context->smarty->fetch($content);
		$html_swip = $html;
		preg_match_all('/\[SWIP_Page_[0-9]{1,9}_Title\]/i', $html_swip, $matches);
		foreach ($matches as $match)
		{
			foreach ($match as $submatch)
				$html_swip = str_replace($submatch,
				$this->SWIPDispatcherPosts(false, true, 'pages', Tools::substr($submatch, 11, strpos(']', $submatch) - 1)), $html_swip);
		}
		preg_match_all('/\[SWIP_Post_[0-9]{1,9}_Title\]/i', $html_swip, $matches);
		foreach ($matches as $match)
		{
			foreach ($match as $submatch)
				$html_swip = str_replace($submatch,
				$this->SWIPDispatcherPosts(false, true, 'posts', Tools::substr($submatch, 11, strpos(']', $submatch) - 1)), $html_swip);
		}
		preg_match_all('/\[SWIP_Post_[0-9]{1,9}\]/i', $html_swip, $matches);
		foreach ($matches as $match)
		{
			foreach ($match as $submatch)
				$html_swip = str_replace($submatch,
				$this->SWIPDispatcherPosts(false, false, 'posts', Tools::substr($submatch, 11, strpos(']', $submatch) - 1)), $html_swip);
		}
		preg_match_all('/\[SWIP_Page_[0-9]{1,9}\]/i', $html_swip, $matches);
		foreach ($matches as $match)
		{
			foreach ($match as $submatch)
				$html_swip = str_replace($submatch,
				$this->SWIPDispatcherPosts(false, false, 'pages', Tools::substr($submatch, 11, strpos(']', $submatch) - 1)), $html_swip);
		}
		$html_swip = str_replace('[SWIP_Posts]', $this->SWIPDispatcherPosts(true, false, null, null), $html_swip);
		$html = trim($html_swip);

		echo $html;	
	}

}

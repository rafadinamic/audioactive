{*
**
* 2014 NarceaDigital
*
* Simple WordPress Into Prestashop
*
* NOTICE OF LICENSE
*
*  @author    NarceaDigital prestashop@narcea.com
*  @copyright 2016 NarceaDigital
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  @version 1.0.7
*
*}

{if $errors|@count > 0}
	<div class="error">
		<ul>
			{foreach from=$errors item=error}
				<li>{$error|escape:'htmlall':'UTF-8'}</li>
			{/foreach}
		</ul>
	</div>
{/if}

<form action="{$request_uri|escape:'htmlall':'UTF-8'|replace:'&amp;':'&'}" method="post" class="std">
	<fieldset>
		<legend><img src="{$path|escape:'htmlall':'UTF-8'}logo.png" alt="" title="" />{l s='Settings' mod='simplewordpressintoprestashop'}</legend>
			<label>{l s='WordPress internal path:' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				<input type="text" size="50" name="SWIP_WordPress_Path" value="{$SWIP_WordPress_Path|escape:'htmlall':'UTF-8'}" />
				{if $SWIP_ErrorPath=='Yes'}
					<p style="color:red;">{l s='Error, WordPress not found on this path' mod='simplewordpressintoprestashop'}</p>
					{else}
				<p style="color:green;">{l s='WordPress found!' mod='simplewordpressintoprestashop'}</p>
				{/if}	
				<p class="clear">{l s='Your Wordpress must be on same server that this PrestaShop. by example: /var/www/vhosts/mydomain/httpdocs/' mod='simplewordpressintoprestashop'}</p>
				<p class="clear">{l s='You can download and install in your WordPress our "SWIP" WordPress Plugin, to locate route. Simply install and find in source code of WordPress frontpage (Ctrl+U)
				"SWIP" text, and after, you can see WordPress internal path. Uninstall plugin after use.' mod='simplewordpressintoprestashop'}</p>
				<p class="clear"><a href='{$SWIP_MODULE_DIR|escape:'htmlall':'UTF-8'}simplewordpressintoprestashop/swip.zip'>{l s='Download WordPress plugin. Click here' mod='simplewordpressintoprestashop'}</a></p>
			</div>
			<label>{l s='Detect WordPress automatically (SWIP WordPress Plugin must be installed). Enter WordPress url (by example: http://myblog.com)' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				</p>
				</p>
				<input type="text" size="50" name="SWIP_WordPress_Url" value="{$SWIP_WordPress_Url|escape:'htmlall':'UTF-8'}" />
			</div>
			</p>
			</p>
			<label>{l s='WordPress max posts to show:' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				<input type="number" min="1" max="9999" name="SWIP_maxPosts" value="{$SWIP_maxPosts|escape:'htmlall':'UTF-8'}" />
				<p class="clear">{l s='You can show a unique post or all you need' mod='simplewordpressintoprestashop'}</p>
			</div>
			<label>{l s='Show post/page date' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				{if $SWIP_showDate=='Yes'}
					<input type="radio" name="SWIP_showDate" value="{'Yes'|escape:'htmlall':'UTF-8'}" checked />{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_showDate" value="{'No'|escape:'htmlall':'UTF-8'}" />{l s='No' mod='simplewordpressintoprestashop'}
				{else}
					<input type="radio" name="SWIP_showDate" value="{'Yes'|escape:'htmlall':'UTF-8'}"/>{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_showDate" value="{'No'|escape:'htmlall':'UTF-8'}" checked/>{l s='No' mod='simplewordpressintoprestashop'}
				{/if}
				<p class="clear">{l s='Show above of your page or posts, date of each one.' mod='simplewordpressintoprestashop'}</p>
			</div>
			<label>{l s='Bold page/post title' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				{if $SWIP_TitleStrong=='Yes'}
					<input type="radio" name="SWIP_TitleStrong" value="{'Yes'|escape:'htmlall':'UTF-8'}" checked />{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_TitleStrong" value="{'No'|escape:'htmlall':'UTF-8'}" />{l s='No' mod='simplewordpressintoprestashop'}
				{else}
					<input type="radio" name="SWIP_TitleStrong" value="{'Yes'|escape:'htmlall':'UTF-8'}"/>{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_TitleStrong" value="{'No'|escape:'htmlall':'UTF-8'}" checked/>{l s='No' mod='simplewordpressintoprestashop'}
				{/if}
				<p class="clear">{l s='Show title of posts or pages stronger' mod='simplewordpressintoprestashop'}</p>
			</div>
			<label>{l s='Show post/page Author' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				{if $SWIP_showAuthor=='Yes'}
					<input type="radio" name="SWIP_showAuthor" value="{'Yes'|escape:'htmlall':'UTF-8'}" checked />{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_showAuthor" value="{'No'|escape:'htmlall':'UTF-8'}" />{l s='No' mod='simplewordpressintoprestashop'}
				{else}
					<input type="radio" name="SWIP_showAuthor" value="{'Yes'|escape:'htmlall':'UTF-8'}"/>{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_showAuthor" value="{'No'|escape:'htmlall':'UTF-8'}" checked/>{l s='No' mod='simplewordpressintoprestashop'}
				{/if}
				<p class="clear">{l s='Show above of your page or posts, author of each one.' mod='simplewordpressintoprestashop'}</p>
			</div>
			<label>{l s='Make post/page link' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				{if $SWIP_show_LinkTitle=='Yes'}
					<input type="radio" name="SWIP_show_LinkTitle" value="{'Yes'|escape:'htmlall':'UTF-8'}" checked />{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_show_LinkTitle" value="{'No'|escape:'htmlall':'UTF-8'}" />{l s='No' mod='simplewordpressintoprestashop'}
				{else}
					<input type="radio" name="SWIP_show_LinkTitle" value="{'Yes'|escape:'htmlall':'UTF-8'}"/>{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_show_LinkTitle" value="{'No'|escape:'htmlall':'UTF-8'}" checked/>{l s='No' mod='simplewordpressintoprestashop'}
				{/if}
				<p class="clear">{l s='Make hyperlink on posts and pages title, to blog url.' mod='simplewordpressintoprestashop'}</p>
			</div>
			<label>{l s='Custom title link (leave blank to set default):' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				<input type="text" size="50" name="SWIP_custom_LinkTitle" value="{$SWIP_custom_LinkTitle|escape:'htmlall':'UTF-8'}" />
				<p class="clear">{l s='Customize post and page title hyperlink to a url you need.' mod='simplewordpressintoprestashop'}</p>
			</div>
			<label>{l s='Date text size' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				{if $SWIP_DateTextSize<=1}
					<input type="radio" name="SWIP_DateTextSize" value="{1|escape:'htmlall':'UTF-8'}" checked />h1
					<input type="radio" name="SWIP_DateTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_DateTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_DateTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_DateTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_DateTextSize==2}
					<input type="radio" name="SWIP_DateTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_DateTextSize" value="{2|escape:'htmlall':'UTF-8'}" checked />h2
					<input type="radio" name="SWIP_DateTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_DateTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_DateTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_DateTextSize==3}
					<input type="radio" name="SWIP_DateTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_DateTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_DateTextSize" value="{3|escape:'htmlall':'UTF-8'}" checked />h3
					<input type="radio" name="SWIP_DateTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_DateTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_DateTextSize==4}
					<input type="radio" name="SWIP_DateTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_DateTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_DateTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_DateTextSize" value="{4|escape:'htmlall':'UTF-8'}" checked />h4
					<input type="radio" name="SWIP_DateTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_DateTextSize==5}
					<input type="radio" name="SWIP_DateTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_DateTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_DateTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_DateTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_DateTextSize" value="{5|escape:'htmlall':'UTF-8'}" checked/>h5
				{/if}
				<p class="clear">{l s='Define text size of date paragraphs.' mod='simplewordpressintoprestashop'}</p>
			</div>
			<label>{l s='Autor text size' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				{if $SWIP_AuthorTextSize<=1}
					<input type="radio" name="SWIP_AuthorTextSize" value="{1|escape:'htmlall':'UTF-8'}" checked />h1
					<input type="radio" name="SWIP_AuthorTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_AuthorTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_AuthorTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_AuthorTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_AuthorTextSize==2}
					<input type="radio" name="SWIP_AuthorTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_AuthorTextSize" value="{2|escape:'htmlall':'UTF-8'}" checked />h2
					<input type="radio" name="SWIP_AuthorTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_AuthorTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_AuthorTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_AuthorTextSize==3}
					<input type="radio" name="SWIP_AuthorTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_AuthorTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_AuthorTextSize" value="{3|escape:'htmlall':'UTF-8'}" checked />h3
					<input type="radio" name="SWIP_AuthorTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_AuthorTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_AuthorTextSize==4}
					<input type="radio" name="SWIP_AuthorTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_AuthorTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_AuthorTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_AuthorTextSize" value="{4|escape:'htmlall':'UTF-8'}" checked />h4
					<input type="radio" name="SWIP_AuthorTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_AuthorTextSize==5}
					<input type="radio" name="SWIP_AuthorTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_AuthorTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_AuthorTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_AuthorTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_AuthorTextSize" value="{5|escape:'htmlall':'UTF-8'}" checked/>h5
				{/if}
				<p class="clear">{l s='Define size of Autor paragraph.' mod='simplewordpressintoprestashop'}</p>
			</div>
			<label>{l s='Autor text size' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				{if $SWIP_TitleTextSize<=1}
					<input type="radio" name="SWIP_TitleTextSize" value="{1|escape:'htmlall':'UTF-8'}" checked />h1
					<input type="radio" name="SWIP_TitleTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_TitleTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_TitleTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_TitleTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_TitleTextSize==2}
					<input type="radio" name="SWIP_TitleTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_TitleTextSize" value="{2|escape:'htmlall':'UTF-8'}" checked />h2
					<input type="radio" name="SWIP_TitleTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_TitleTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_TitleTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_TitleTextSize==3}
					<input type="radio" name="SWIP_TitleTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_TitleTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_TitleTextSize" value="{3|escape:'htmlall':'UTF-8'}" checked />h3
					<input type="radio" name="SWIP_TitleTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_TitleTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_TitleTextSize==4}
					<input type="radio" name="SWIP_TitleTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_TitleTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_TitleTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_TitleTextSize" value="{4|escape:'htmlall':'UTF-8'}" checked />h4
					<input type="radio" name="SWIP_TitleTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_TitleTextSize==5}
					<input type="radio" name="SWIP_TitleTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_TitleTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_TitleTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_TitleTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_TitleTextSize" value="{5|escape:'htmlall':'UTF-8'}" checked/>h5
				{/if}
				<p class="clear">{l s='Define size of Title paragraph.' mod='simplewordpressintoprestashop'}</p>
			</div>
			<label>{l s='Post / page Content text size' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				{if $SWIP_ContentTextSize<=1}
					<input type="radio" name="SWIP_ContentTextSize" value="{1|escape:'htmlall':'UTF-8'}" checked />h1
					<input type="radio" name="SWIP_ContentTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_ContentTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_ContentTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_ContentTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_ContentTextSize==2}
					<input type="radio" name="SWIP_ContentTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_ContentTextSize" value="{2|escape:'htmlall':'UTF-8'}" checked />h2
					<input type="radio" name="SWIP_ContentTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_ContentTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_ContentTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_ContentTextSize==3}
					<input type="radio" name="SWIP_ContentTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_ContentTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_ContentTextSize" value="{3|escape:'htmlall':'UTF-8'}" checked />h3
					<input type="radio" name="SWIP_ContentTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_ContentTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_ContentTextSize==4}
					<input type="radio" name="SWIP_ContentTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_ContentTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_ContentTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_ContentTextSize" value="{4|escape:'htmlall':'UTF-8'}" checked />h4
					<input type="radio" name="SWIP_ContentTextSize" value="{5|escape:'htmlall':'UTF-8'}" />h5
				{/if}
				{if $SWIP_ContentTextSize==5}
					<input type="radio" name="SWIP_ContentTextSize" value="{1|escape:'htmlall':'UTF-8'}" />h1
					<input type="radio" name="SWIP_ContentTextSize" value="{2|escape:'htmlall':'UTF-8'}" />h2
					<input type="radio" name="SWIP_ContentTextSize" value="{3|escape:'htmlall':'UTF-8'}" />h3
					<input type="radio" name="SWIP_ContentTextSize" value="{4|escape:'htmlall':'UTF-8'}" />h4
					<input type="radio" name="SWIP_ContentTextSize" value="{5|escape:'htmlall':'UTF-8'}" checked/>h5
				{/if}
				<p class="clear">{l s='Define size of page and posts content paragraph.' mod='simplewordpressintoprestashop'}</p>
			</div>
			<label>{l s='Show post/page content' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				{if $SWIP_showContent=='Yes'}
					<input type="radio" name="SWIP_showContent" value="{'Yes'|escape:'htmlall':'UTF-8'}" checked />{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_showContent" value="{'No'|escape:'htmlall':'UTF-8'}" />{l s='No' mod='simplewordpressintoprestashop'}
				{else}
					<input type="radio" name="SWIP_showContent" value="{'Yes'|escape:'htmlall':'UTF-8'}"/>{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_showContent" value="{'No'|escape:'htmlall':'UTF-8'}" checked/>{l s='No' mod='simplewordpressintoprestashop'}
				{/if}
				<p class="clear">{l s='Show / Hide content of posts and pages' mod='simplewordpressintoprestashop'}</p>
			</div>
			<label>{l s='Disable utf8 encode (select when you don\'t see well special characters)' mod='simplewordpressintoprestashop'}</label>
			<div class="margin-form">
				{if $SWIP_utf8_encode=='Yes'}
					<input type="radio" name="SWIP_utf8_encode" value="{'Yes'|escape:'htmlall':'UTF-8'}" checked />{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_utf8_encode" value="{'No'|escape:'htmlall':'UTF-8'}" />{l s='No' mod='simplewordpressintoprestashop'}
				{else}
					<input type="radio" name="SWIP_utf8_encode" value="{'Yes'|escape:'htmlall':'UTF-8'}"/>{l s='Yes' mod='simplewordpressintoprestashop'}
					<input type="radio" name="SWIP_utf8_encode" value="{'No'|escape:'htmlall':'UTF-8'}" checked/>{l s='No' mod='simplewordpressintoprestashop'}
				{/if}
				<p class="clear">{l s='Some Wordpress themes or plugins may require activate this option' mod='simplewordpressintoprestashop'}</p>
			</div><strong>>{l s='HOW TO USE' mod='simplewordpressintoprestashop'}</strong><br/>{l s='WordPress content will be inserted in any PrestaShop page,
		such Home Page, CMS Page, lateral top or footer hooks, etc.' mod='simplewordpressintoprestashop'}<br/>
		{l s='You can insert posts or pages adding simply shortcodes directly
		into any BackOffice module textbox or text editor. Thats All!' mod='simplewordpressintoprestashop'}
		<br/><br/><strong>{l s='Shortcode Examples' mod='simplewordpressintoprestashop'}</strong><br/>
		{l s='Show all posts (limited by max posts value in module configuration page): [SWIP_Posts]' mod='simplewordpressintoprestashop'}<br/>
		{l s='Show a unique post: [SWIP_Post_idpost], by example: [SWIP_Post_23]' mod='simplewordpressintoprestashop'}<br/>
		{l s='Show a unique page: [SWIP_Page_idpage], by example: [SWIP_Page_23]' mod='simplewordpressintoprestashop'}<br/>
		{l s='Show only title unformatted of a unique post: [SWIP_Post_idpage_Title], by example: [SWIP_Post_23_Title]' mod='simplewordpressintoprestashop'}<br/>
		{l s='Show only title unformatted of a unique page: [SWIP_Page_idpage],	by example: [SWIP_Page_23_Title]' mod='simplewordpressintoprestashop'}<br/><br/>
		<center><input type="submit" name="{$submitName|escape:'htmlall':'UTF-8'}" value="{l s='Save' mod='simplewordpressintoprestashop'}" class="button" /></center>
	</fieldset>
</form>
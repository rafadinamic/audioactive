<?php
/**
* 2014 NarceaDigital
*
* Simple WordPress Into Prestashop
*
* NOTICE OF LICENSE
*
*  @author    NarceaDigital prestashop@narcea.com
*  @copyright 2016 NarceaDigital
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  @version 1.0.8
*/

class SimpleWordpressIntoPrestashop extends Module
{
	public function __construct()
	{
		$this->name = 'simplewordpressintoprestashop';
		$this->tab = 'administration';
		$this->author = 'NarceaDigital';
		$this->version = '1.0.8';
		$this->module_key = '58c322a2cccb725d3b440db61f923629';
			$this->ps_versions_compliancy = array('min' => '1.6.0.9', 'max' => '5.6.1.99');
		if (!defined('_PS_ADMIN_DIR_'))
		{
			if (defined('PS_ADMIN_DIR'))
				define('_PS_ADMIN_DIR_', PS_ADMIN_DIR);
			else
				$this->_errors[] = $this->l('Error. Prestashop version is not compatible with this module');
		}

		parent::__construct();

		$this->displayName = $this->l('Simple WordPress Into Prestashop');
		$this->description = $this->l('Shows WordPress content in PrestaShop through simply shortcodes');
	}

	public function install()
	{
		if (file_exists(_PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'override/classes/controller/FrontController.php'))
			unlink(_PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'override/classes/controller/FrontController.php');
		// Check module name validation
		if (!Validate::isModuleName($this->name))
		die(Tools::displayError());
		// Check PS version compliancy
		if (version_compare(_PS_VERSION_, $this->ps_versions_compliancy['min']) < 0 ||
			version_compare(_PS_VERSION_, $this->ps_versions_compliancy['max']) >= 0)
		{
			$this->_errors[] = $this->l('The version of your module is not compliant with your PrestaShop version.
			Please contact developer by a support ticket in Addons, and problem will be fixed quickly');
			return false;
		}
		// Check module dependencies
		if (count($this->dependencies) > 0)
		foreach ($this->dependencies as $dependency)
			if (!Db::getInstance()->getRow('SELECT `id_module` FROM `'._DB_PREFIX_.'module` WHERE `name` = \''.pSQL($dependency).'\''))
			{
				$error = $this->l('Before installing this module, you have to installed these/this module(s) first :').'<br />';
				foreach ($this->dependencies as $d)
					$error .= '- '.$d.'<br />';
				$this->_errors[] = $error;
				return false;
			}
		// Check if module is installed
		$result = Db::getInstance()->getRow('SELECT `id_module` FROM `'._DB_PREFIX_.'module` WHERE `name` = \''.pSQL($this->name).'\'');
		if ($result)
		{
			$this->_errors[] = $this->l('This module has already been installed.');
			return false;
		}
		// Install overrides
		try
		{
			$this->installOverrides();
		} catch (Exception $e)
		{
			$this->_errors[] = sprintf(Tools::displayError('Unable to install override: %s'), $e->getMessage());
			return false;
		}
		// Install module and retrieve the installation id
		$result = Db::getInstance()->insert($this->table, array('name' => $this->name, 'active' => 1, 'version' => $this->version));
		if (!$result)
		{
			$this->_errors[] = $this->l('Technical error : PrestaShop could not installed this module.');
			return false;
		}
		$this->id = Db::getInstance()->Insert_ID();
		Cache::clean('Module::isInstalled'.$this->name);
		// Enable the module for current shops in context
		$this->enable();
		// Permissions management
		Db::getInstance()->execute('
		INSERT INTO `'._DB_PREFIX_.'module_access` (`id_profile`, `id_module`, `view`, `configure`) (
			SELECT id_profile, '.(int)$this->id.', 1, 1
			FROM '._DB_PREFIX_.'access a
			WHERE id_tab = (
				SELECT `id_tab` FROM '._DB_PREFIX_.'tab
				WHERE class_name = \'AdminModules\' LIMIT 1)
			AND a.`view` = 1)');

		Db::getInstance()->execute('
		INSERT INTO `'._DB_PREFIX_.'module_access` (`id_profile`, `id_module`, `view`, `configure`) (
			SELECT id_profile, '.(int)$this->id.', 1, 0
			FROM '._DB_PREFIX_.'access a
			WHERE id_tab = (
				SELECT `id_tab` FROM '._DB_PREFIX_.'tab
				WHERE class_name = \'AdminModules\' LIMIT 1)
			AND a.`view` = 0)');
		// Adding Restrictions for client groups
		Group::addRestrictionsForModule($this->id, Shop::getShops(true, null, true));
		mkdir(_PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'override/classes');
		mkdir(_PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'override/classes/controller');
		return true;
	}

	public function uninstall()
	{
		parent::uninstall();
		$this->uninstallOverrides();
		if (file_exists(_PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'override/classes/controller/FrontController.php'))
			unlink(_PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'override/classes/controller/FrontController.php');
		return parent::uninstall();
	}

	public function enable($force_all = false)
	{
		parent::enable();
		try
		{
			if (parent::enable($force_all))
			parent::installOverrides();
		}
		catch (Exception $e)
		{
			$this->_errors[] = sprintf(Tools::displayError('Unable to install override: %s'), $e->getMessage());
			return false;
		}
		return parent::enable();
	}
	public function disable($force_all = false)
	{
		// Disable module for all shops
		try
		{
			parent::disable($force_all);
			parent::uninstallOverrides();
		}
		catch (Exception $e)
		{
			$this->_errors[] = sprintf(Tools::displayError('Unable to install override: %s'), $e->getMessage());
			return false;
		}
		return parent::disable();
	}
	public function getContent()
	{
		$output = '<h2>'.$this->displayName.'</h2>';
		if (Tools::isSubmit('submit'.Tools::ucfirst($this->name)))
		{
			$ruta_wordpress = Tools::getValue('SWIP_WordPress_Path');
			if (!$ruta_wordpress
			|| empty($ruta_wordpress)
			|| !Validate::isGenericName($ruta_wordpress)
			|| !file_exists($ruta_wordpress.'/wp-blog-header.php'))
					Configuration::updateValue('SWIP_ErrorPath', 'Yes');
		else
		{
			Configuration::updateValue('SWIP_ErrorPath', 'No');
			Configuration::updateValue('SWIP_WordPress_Path', $ruta_wordpress);
		}
		$max_wordpress_posts = Tools::getValue('SWIP_maxPosts');
		if (!$max_wordpress_posts
		|| empty($max_wordpress_posts)
		|| !Validate::isGenericName($max_wordpress_posts)
		|| !is_numeric($max_wordpress_posts))
			$output .= $this->displayError($this->l('Invalid posts number'));
			else Configuration::updateValue('SWIP_maxPosts', $max_wordpress_posts);
			Configuration::updateValue('SWIP_showDate', Tools::getValue('SWIP_showDate'));
			Configuration::updateValue('SWIP_showAuthor', Tools::getValue('SWIP_showAuthor'));
			Configuration::updateValue('SWIP_DateTextSize', Tools::getValue('SWIP_DateTextSize'));
			Configuration::updateValue('SWIP_AuthorTextSize', Tools::getValue('SWIP_AuthorTextSize'));
			Configuration::updateValue('SWIP_TitleTextSize', Tools::getValue('SWIP_TitleTextSize'));
			Configuration::updateValue('SWIP_TitleStrong', Tools::getValue('SWIP_TitleStrong'));
			Configuration::updateValue('SWIP_ContentTextSize', Tools::getValue('SWIP_ContentTextSize'));
			Configuration::updateValue('SWIP_showContent', Tools::getValue('SWIP_showContent'));
			Configuration::updateValue('SWIP_show_LinkTitle', Tools::getValue('SWIP_show_LinkTitle'));
			Configuration::updateValue('SWIP_custom_LinkTitle', Tools::getValue('SWIP_custom_LinkTitle'));
			Configuration::updateValue('SWIP_WordPress_Url', Tools::getValue('SWIP_WordPress_Url'));
			Configuration::updateValue('SWIP_utf8_encode', Tools::getValue('SWIP_utf8_encode'));
			if (isset($this->errors) && count($this->errors))
				$output .= $this->displayError(implode('<br />', $this->errors));
			else
				$output .= $this->displayConfirmation($this->l('Settings updated'));
			if (Tools::getValue('SWIP_WordPress_Url') != '')
			{
				if (validate::isUrl(Tools::getValue('SWIP_WordPress_Url')))
				{
					$webpage_content = Tools::file_get_contents(Tools::getValue('SWIP_WordPress_Url'));
					$substring_webpage_content = Tools::substr($webpage_content, Tools::strpos($webpage_content, 'WordPress Internal Path for SWIP'));
					if (Tools::strpos($substring_webpage_content, 'WordPress Internal Path for SWIP') === false)
						$output .= $this->displayError($this->l('Error, SWIP WordPress plugin not found. Automatic WordPress Search is not possible.'));
					else
					{
						$internal_path_url = Tools::substr($substring_webpage_content, 35);
						$pos_comillas_internal_path = strpos($internal_path_url, chr(34));
						$final_internal_path_url = Tools::substr($internal_path_url, 0, $pos_comillas_internal_path);
						Configuration::updateValue('SWIP_WordPress_Path', '/'.$final_internal_path_url);
					}
				}
			}
		}
		return $output.$this->displayForm();
	}

	public function displayForm()
	{
		$this->context->smarty->assign('request_uri', Tools::safeOutput($_SERVER['REQUEST_URI']));
		$this->context->smarty->assign('path', $this->_path);
		$this->context->smarty->assign('SWIP_WordPress_Path', pSQL(Tools::getValue('SWIP_WordPress_Path', Configuration::get('SWIP_WordPress_Path'))));
		$this->context->smarty->assign('SWIP_maxPosts', pSQL(Tools::getValue('SWIP_maxPosts', Configuration::get('SWIP_maxPosts'))));
		$this->context->smarty->assign('SWIP_utf8_encode', pSQL(Tools::getValue('SWIP_utf8_encode', Configuration::get('SWIP_utf8_encode'))));
		$this->context->smarty->assign('SWIP_showDate', pSQL(Tools::getValue('SWIP_showDate', Configuration::get('SWIP_showDate'))));
		$this->context->smarty->assign('SWIP_showAuthor', pSQL(Tools::getValue('SWIP_showAuthor', Configuration::get('SWIP_showAuthor'))));
		$this->context->smarty->assign('SWIP_DateTextSize', pSQL(Tools::getValue('SWIP_DateTextSize', Configuration::get('SWIP_DateTextSize'))));
		$this->context->smarty->assign('SWIP_AuthorTextSize', pSQL(Tools::getValue('SWIP_AuthorTextSize', Configuration::get('SWIP_AuthorTextSize'))));
		$this->context->smarty->assign('SWIP_TitleTextSize', pSQL(Tools::getValue('SWIP_TitleTextSize', Configuration::get('SWIP_TitleTextSize'))));
		$this->context->smarty->assign('SWIP_showContent', pSQL(Tools::getValue('SWIP_showContent', Configuration::get('SWIP_showContent'))));
		$this->context->smarty->assign('SWIP_TitleStrong', pSQL(Tools::getValue('SWIP_TitleStrong', Configuration::get('SWIP_TitleStrong'))));
		$this->context->smarty->assign('SWIP_ContentTextSize', pSQL(Tools::getValue('SWIP_ContentTextSize', Configuration::get('SWIP_ContentTextSize'))));
		$this->context->smarty->assign('SWIP_show_LinkTitle', pSQL(Tools::getValue('SWIP_show_LinkTitle', Configuration::get('SWIP_show_LinkTitle'))));
		$this->context->smarty->assign('SWIP_custom_LinkTitle',
			pSQL(Tools::getValue('SWIP_custom_LinkTitle', Configuration::get('SWIP_custom_LinkTitle'))));
		$this->context->smarty->assign('SWIP_ErrorPath', pSQL(Tools::getValue('SWIP_ErrorPath', Configuration::get('SWIP_ErrorPath'))));
		$this->context->smarty->assign('SWIP_WordPress_Url', pSQL(Tools::getValue('SWIP_WordPress_Url', Configuration::get('SWIP_WordPress_Url'))));
		$this->context->smarty->assign('SWIP_MODULE_DIR', _MODULE_DIR_);
		$this->context->smarty->assign('submitName', 'submit'.Tools::ucfirst($this->name));
		@$this->context->smarty->assign('errors', $this->errors);
		return $this->display(__FILE__, 'views/templates/admin/configure.tpl');
	}
}
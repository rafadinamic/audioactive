<div class="navbar">
	<nav id="cavas_menu" class="navbar-default  pull-left" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<!--<button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">{l s='Toggle navigation' mod='leobootstrapmenu'}</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>-->
			<button type="button" class="navbar-toggle">
				<span class="sr-only">{l s='Toggle navigation' mod='leobootstrapmenu'}</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div id="leo-top-menu" class="collapse navbar-collapse navbar-ex1-collapse divtogglemenu">
			<!-- NUEVO PARA METER BOTONES EN MENU RWD -->
			<div id="header_user" class="{if $PS_CATALOG_MODE}header_user_catalog {/if}pull-right showrwd">
				<ul>
					<!--<li>{l s='Welcome' mod='blockuserinfo'},</li>-->
					{if $logged}
						<li><a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow"><span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a></li>
						<li><a href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html'}" title="{l s='Log me out' mod='blockuserinfo'}" class="logout" rel="nofollow"><span class="fa fa-times"></span>{l s='Log out' mod='blockuserinfo'}</a></li>
						<li ><a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='My Account' mod='blockuserinfo'}" ><span class="fa fa-user"></span>{l s='My Account' mod='blockuserinfo'}</a></li>
					{else}
						<!--<li><a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='Login to your customer account' mod='blockuserinfo'}" class="login" rel="nofollow"><span class="fa fa-lock"></span>{l s='Login' mod='blockuserinfo'}</a></li>-->
						<li><a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='Login to your customer account' mod='blockuserinfo'}" class="login" rel="nofollow"><span class="fa fa-user"></span> ENTRAR / REGISTRO</a></li>			
					{/if}
					<!--<li ><a href="{$link->getModuleLink('blockwishlist', 'mywishlist')}" title="{l s='My wishlist' mod='blockuserinfo'}"><span class="fa fa-heart"></span>{l s='My wishlist' mod='blockuserinfo'}</a></li>-->
				</ul>
						
			</div>
			<!-- NUEVO PARA METER BOTONES EN MENU RWD -->
			
			{$leobootstrapmenu_menu_tree}
			
			<!-- NUEVO PARA METER BOTONES EN MENU RWD -->
			<hgroup class="showrwd">
				<h2>Centro de Atenci&oacute;n al Cliente <span><a href="tel:+966115079">966 115 079</a></h2>
				<h3 class="cac-time">Lu. a Vi. de 9:30 a 13:30</h3>
			</hgroup>
			<div class="white_background"></div>
			<!-- NUEVO PARA METER BOTONES EN MENU RWD -->
		</div>
	</nav>
</div>
<script>
$(document).ready(function()
{
	$("button.navbar-toggle").click(function(e)
	{
		e.preventDefault();
		$(".divtogglemenu").slideToggle("fast");
	});
});
</script>
<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.c om for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class blocktrustivitywidget extends Module
{
	private $_html = '';
	private $_postErrors = array();

	public function __construct()
	{
		$this->name = 'blocktrustivitywidget';
		$this->tab = 'front_office_features';
		$this->version = 2.0;
		$this->author = 'Jesus Martin - Trustivity';
		$this->need_instance = 0;
		parent::__construct();

		$this->displayName = 'Trustivity widget valoraciones';
		$this->description = utf8_encode("A�ade el widget de valoraciones de trusttivity. Para configurar los parametros lo debes hacer desde <a href='http://www.trustivity.es/backoffice/'>tu backoffice</a>");
                $this->secure_key = Tools::encrypt($this->name);
                $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
                
                $this->confirmUninstall = 'Estas seguro que quieres desinstalar el widget de trustivity?';

                if (!Configuration::get('blocktrustivitywidget'))      
                  $this->warning = $this->l('No name provided');
	}

       public function uninstall()
       {
          return parent::uninstall() && Configuration::deleteByName('blocktrustivitywidget');
       }
        
       public function install()
        {
          if (Shop::isFeatureActive())
                Shop::setContext(Shop::CONTEXT_ALL);

          return parent::install() &&
                $this->registerHook('leftColumn') &&
                Configuration::updateValue('blocktrustivitywidget', 'my friend');
        }
        
        public function hookDisplayLeftColumn($params)
        {
            $cod_pass = Configuration::get('COD_PASS_TRUSTED');

            if(empty($cod_pass) || $cod_pass == "")
                $cod_widget = '';

            // WS
            include(dirname(__FILE__) . "/claseXMLRPC/lib/xmlrpc.inc");
            $servidor = explode("www.", $_SERVER["SERVER_NAME"]);
            
            if(isset($servidor[1]))
                $servidor = $servidor[1];
            else 
                $servidor = $_SERVER["SERVER_NAME"];

            $f=new xmlrpcmsg('widget.mostrar',array(new xmlrpcval($servidor, "string"), new xmlrpcval($cod_pass,"string")));
            $c=new xmlrpc_client("http://trustivity.es/trustedXMLRPC/widget.php");

            $c->setDebug(0);
            $r=$c->send($f);
            if(!$r){ 
                    die("error SEND"); 
            }

            $v=$r->value();

            if(!$r->faultCode()){   
                    $cod_widget = "<div id='widget'>". json_decode($v->scalarval()) ."</div>";
            }else{   
                    //$cod_widget = "N�mero de error: " .$r->faultCode()." Descripci�n del error '".$r->faultString()."'<BR>";
            }
            // END WS
          
            $this->context->smarty->assign(
                    array(
                            'codigo_widget' => $cod_widget,
                    )
            );

            return $this->display(__FILE__, 'blocktrustivitywidget.tpl');
        }

        public function hookDisplayRightColumn($params)
        {
          return $this->hookDisplayLeftColumn($params);
        }

        public function hookDisplayHeader()
        {
          $this->context->controller->addCSS($this->_path.'css/style_audition.css', 'all');
        }
        
        public function getContent()
	{
		$output = '<h2>'.$this->displayName.'</h2>';
                
		if (Tools::isSubmit('submitBlockWidg'))
		{
			if (!($txtWidget = Tools::getValue('pass_sell')) || empty($txtWidget))
				$output .= '<div class="alert error">'.$this->l('You must fill in the \'Products displayed\' field.').'</div>';
			else
			{
				Configuration::updateValue('TRUSTIVITY_PASS', $txtWidget);
				$output .= '<div class="conf confirm">'.$this->l('Settings updated').'</div>';
			}
		}
		return $output.$this->displayForm();
	}

	public function displayForm()
	{
            $pass = Configuration::get('TRUSTIVITY_PASS');
            
            if(empty($pass) || $pass == "")
                $pass = '';
            
		$output = '
		<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.png" alt="" title="" />'.$this->l('Settings').'</legend>
				<label>Introduce tu password de Trustivity: </label>
				<div class="margin-form">
					<input type="text" name="pass_sell" value="' . $pass . '">
					<p class="clear">'.$this->l('Puede ver este password en la secci&oacute;n "Mis datos" de su backoffice').'</p>
				</div>
				<center><input type="submit" name="submitBlockWidg" value="Guardar" class="button" /></center>
			</fieldset>
		</form>';
		return $output;
	}
}

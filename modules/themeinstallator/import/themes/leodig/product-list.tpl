{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if isset($products)}
	<!-- Products list -->
<div id="product_list" class="products_block  block {if Configuration::get('leoview')==0}view-list{/if}">
	{foreach from=$products item=product name=products}
		{if $product@iteration%Configuration::get('productlistcols')==1 || Configuration::get('productlistcols')==1}
			<div class="row  products-item clearfix">
        {/if}
		{assign var='proReduce' value=""}
		{if isset($product.specific_prices.reduction)}
			{if $product.specific_prices.reduction_type == 'percentage'}
				{assign var='proReduce' value=$product.specific_prices.reduction*100}
			{elseif $product.specific_prices.reduction_type == 'amount'}
				{assign var='proReduce' value=ceil($product.specific_prices.reduction/$product.price*100)}
			{/if}
		{/if}
		
			<div class="col-xs-12 col-sm-{(12/Configuration::get('productlistcols'))} product_block ajax_block_product ">		
				<div class="product-container clearfix">
					<div class="center_block">								
						<a href="{$product.link|escape:'htmlall':'UTF-8'}" class="product_image" title="{$product.name|escape:'htmlall':'UTF-8'}">
							<img class="img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html'}" alt="{if !empty($product.legend)}{$product.legend|escape:'htmlall':'UTF-8'}{else}{$product.name|escape:'htmlall':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'htmlall':'UTF-8'}{else}{$product.name|escape:'htmlall':'UTF-8'}{/if}" />
							<span class="product-additional" rel="{$product.id_product}"></span>	
							{if isset($product.new) && $product.new == 1}<span class="new">{l s='New'}</span>{/if}
							{if $proReduce}<span class="hot">-{$proReduce}%</span>{/if}
						</a>
						
					</div>
					<div class="right_block">
						{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}<span class="on_sale">{l s='On sale!'}</span>{elseif isset($product.reduction) && $product.reduction && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}<span class="discount">{l s='Reduced price!'}</span>
						{/if}
						<h4 class="name">{if isset($product.pack_quantity) && $product.pack_quantity}{$product.pack_quantity|intval|cat:' x '}{/if}<a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name|truncate:35:'...'|escape:'htmlall':'UTF-8'}</a></h4>
						
						{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
							<div class="content_price price_container">
								{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}<span class="price" style="display: inline;">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span><br />{/if}
								{if isset($product.available_for_order) && $product.available_for_order && !isset($restricted_country_mode)}<span class="availability">{if ($product.allow_oosp || $product.quantity > 0)}{l s='Available'}{elseif (isset($product.quantity_all_versions) && $product.quantity_all_versions > 0)}{l s='Product available with different options'}{else}<span class="warning_inline">{l s='Out of stock'}</span>{/if}</span>{/if}
							</div>
					
								{if isset($product.online_only) && $product.online_only}<span class="online_only">{l s='Online only'}</span>{/if}
						{/if}

						<div class="product_desc">{$product.description_short|strip_tags:'UTF-8'|truncate:120:'...'}</div>

						{if isset($comparator_max_item) && $comparator_max_item}
							<p class="compare">
								<input type="checkbox" class="comparator" id="comparator_item_{$product.id_product}" value="comparator_item_{$product.id_product}" {if isset($compareProducts) && in_array($product.id_product, $compareProducts)}checked="checked"{/if} autocomplete="off"/> 
								<label for="comparator_item_{$product.id_product}">{l s='Select to compare'}</label>
							</p>
						{/if}
						
						
						<a class="rating_box leo-rating-{$product.id_product}" href="#" rel="{$product.id_product}" style="display:none">
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>      
						</a>
						
						{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.minimal_quantity <= 1 && $product.customizable != 2 && !$PS_CATALOG_MODE}
							{if ($product.allow_oosp || $product.quantity > 0)}
								{if isset($static_token)}
									<a class="button ajax_add_to_cart_button exclusive pull-left" rel="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)|escape:'html'}" title="{l s='Add to cart'}"><span>{l s='Add to cart'}</span></a>
								{else}
									<a class="button ajax_add_to_cart_button exclusive pull-left" rel="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}", false)|escape:'html'}" title="{l s='Add to cart'} ">{l s='Add to cart'}</a>
								{/if}						
							{else}
								<span class="exclusive button pull-left">{l s='Add to cart'}</span>
							{/if}
						{/if}
						
						  <a  href="#" id="wishlist_button{$product.id_product}" class="btn-tooltip button wishlist-compare fa fa-heart" onclick="LeoWishlistCart('wishlist_block_list', 'add', '{$product.id_product}', $('#idCombination').val(), 1 ); return false;" title="{l s='Add to wishlist'}"></a>
						  <a class="quick-view btn-tooltip button wishlist-compare fa fa-arrows-alt" title="{l s='Quick View'}" href="{if $product.link|strpos:"?"}{$product.link|cat:"&content_only=1"|escape:'htmlall':'UTF-8'}{else}{$product.link|cat:"?content_only=1"|escape:'htmlall':'UTF-8'}{/if}"></a>
						
					</div>								
				</div>
			</div>
		{if $product@iteration%Configuration::get('productlistcols')==0||$smarty.foreach.products.last}
			</div>
		{/if}
	{/foreach}
	</div>
	<!-- /Products list -->
{/if}

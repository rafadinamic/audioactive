<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php if ( is_active_sidebar( 'sidebar-1' )  ) : ?>
	<aside id="secondary" class="sidebar widget-area" role="complementary">

		<section id="recent-posts-2" class="widget widget_recent_entries">
			<h2 class="widget-title">Entradas recientes</h2>
			<ul>
				<?php wp_get_archives('type=postbypost&limit=5'); ?>
			</ul>
		</section>	
		<section id="archives-2" class="widget widget_archive">
			<h2 class="widget-title">Productos</h2>
				<ul>
						<li><a href="https://www.audioactive.es/soluciones-auditivas-_6/">Soluciones Auditivas</a></li>
						<li><a href="https://www.audioactive.es/auriculares-tv_8/">Auriculares TV</a></li>
						<li><a href="https://www.audioactive.es/telefonos-mayores_7/">Teléfonos</a></li>
						<li><a href="https://www.audioactive.es/despertador-sordos_9/">Despertadores</a></li>
						<li><a href="https://www.audioactive.es/limpieza-audifonos_12/">Limpieza Audífonos</a></li>
						<li><a href="https://www.audioactive.es/pilas-audifonos_11/">Pilas Audífonos</a></li>
				</ul>
		</section>
		<section id="recent-posts-2" class="widget widget_recent_entries">
			<h2 class="widget-title">Buscar</h2>
				<ul><li>
					<form role="search" method="get" class="search-form" action="https://www.audioactive.es/blog/">
						<label>
							<span class="screen-reader-text">Buscar por:</span>
							<input type="search" class="search-field" placeholder="Buscar …" value="" name="s">
						</label>
						<button type="submit" class="search-submit"><span class="screen-reader-text">Buscar</span></button>
					</form>
				</li></ul>
		</section>
</li>
		</ul>
</section>	</aside>
<?php endif; ?>

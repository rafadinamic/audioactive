$(document).ready( function(){
	
	if ($(".navbar-default .tlf-menu span.menu-title").length > 0) {
		var txt_menu = "966 115 079";
		txt_menu += "<span class=\"hidden-xs hidden-sm menu-tlf-span\" style=\"font-size: 9px;\">Atención telefónica</span>"
		txt_menu += "<span class=\"hidden-xs hidden-sm menu-tlf-span\" style=\"font-size: 11px;\">10:00-13:30</span>";
		txt_menu += "<span class=\"hidden-xs hidden-sm menu-tlf-span\" style=\"font-size: 11px;\">16:30-18:30</span>";
		$(".navbar-default .tlf-menu span.menu-title").html(txt_menu);
	}
	
	if ($("#categories_block_left").length > 0) {
		var txt_buttons = "";
		txt_buttons += "<div class=\"media-list\" style=\"display: inline-block;\">";
			txt_buttons += "<div class=\"col-xs-12 col-sm-6 col-md-12 col-lg-12\" style=\"margin-bottom: 10px;\">";
				txt_buttons += "<img class=\"media-object infopromos\" src=\"http://www.audioactive.es/img/cms/envio-icon.jpg\" alt=\"Envío gratis en pedidos superiores a 60€\" style=\"max-width: 220px; width: 100%; max-height: 90px; margin-left: 30px;\">";
			txt_buttons += "</div>";
			txt_buttons += "<div class=\"col-xs-12 col-sm-6 col-md-12 col-lg-12\" style=\"margin-bottom: 10px;\">";
				txt_buttons += "<img class=\"media-object infopromos\" src=\"http://www.audioactive.es/img/cms/devoluciones-icon.jpg\" alt=\"15 días para devoluciones\" style=\"max-width: 220px; width: 100%; max-height: 90px; margin-left: 30px;\">";
			txt_buttons += "</div>";
			txt_buttons += "<div class=\"col-xs-12 col-sm-6 col-md-12 col-lg-12\" style=\"margin-bottom: 10px;\">";
				txt_buttons += "<img class=\"media-object infopromos\" src=\"http://www.audioactive.es/img/cms/fabrica-casa-icon.jpg\" alt=\"Directo de fábrica a tu casa\" style=\"max-width: 220px; width: 100%; max-height: 90px; margin-left: 30px;\">";
			txt_buttons += "</div>";
			txt_buttons += "<div class=\"col-xs-12 col-sm-6 col-md-12 col-lg-12\" style=\"margin-bottom: 10px;\">";
				txt_buttons += "<img class=\"media-object infopromos\" src=\"http://www.audioactive.es/img/cms/catalogo-icon.jpg\" alt=\"Entra al catálogo de productos completo\" style=\"max-width: 220px; width: 100%; max-height: 90px; margin-left: 30px;\">";
			txt_buttons += "</div>";
		txt_buttons += "</div>";
		$("#categories_block_left").append(txt_buttons);
	}
	{
		var txt_buttons = "";
		txt_buttons += "<div class=\"media-list\" style=\"display: inline-block;\">";
			txt_buttons += "<div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4\" style=\"margin-bottom: 10px;\">";
				txt_buttons += "<img class=\"media-object infopromos\" src=\"http://www.audioactive.es/img/cms/ico-envio-gratis.png\" alt=\"Envío gratis en pedidos superiores a 60€\" style=\"max-width: 278px; width: 100%; \">";
			txt_buttons += "</div>";
			txt_buttons += "<div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4\" style=\"margin-bottom: 10px;\">";
				txt_buttons += "<img class=\"media-object infopromos\" src=\"http://www.audioactive.es/img/cms/ico-devoluciones.png\" alt=\"15 días para devoluciones\" style=\"max-width: 278px; width: 100%; \">";
			txt_buttons += "</div>";
			txt_buttons += "<div class=\"col-xs-12 col-sm-4 col-md-4 col-lg-4\" style=\"margin-bottom: 10px;\">";
				txt_buttons += "<img class=\"media-object infopromos\" src=\"http://www.audioactive.es/img/cms/ico-dos-anyos.png\" alt=\"2 Años de garantia\" style=\"max-width: 278px; width: 100%; \">";
			txt_buttons += "</div>";
		txt_buttons += "</div>";
		if ($("#product #more_info_block").length > 0) {
			$("#product #more_info_block").append(txt_buttons);
			$("#product #more_info_block .media-list").addClass("hidden-lg");
			$("#product #more_info_block .media-list").addClass("hidden-md");
			$("#product #more_info_block .media-list").addClass("hidden-sm");
		}
		if ($("#product #buy_block .content_prices").length > 0) {
			$("#product #buy_block .content_prices").append(txt_buttons);
			$("#product #buy_block .content_prices .media-list").addClass("hidden-xs");
		}
	}



	$("a.quick-view").each(function() {
		try {
			var p_detail = $(this).attr("href").split("content_only=");
			$(this).addClass("new-quick-view");
			$(this).removeClass("quick-view");
			$(this).attr("href", p_detail[0]);
		}
		catch (ex) {}
	});

	if( $.cookie('listing_products_mode') ){
            $("#product_list").removeClass('view-grid').removeClass('view-list').addClass( $.cookie('listing_products_mode') );
        }

	$("#productsview a").each( function() {
		if( $.cookie('listing_products_mode') && $(this).attr('rel') == $.cookie('listing_products_mode') ){
			$('#productsview a i').removeClass( 'active' );
			$( 'i', this).addClass('active'); 
		}
		$(this).click( function(){
			$('#productsview a i').removeClass( 'active' );
			$( 'i', this).addClass('active');
			$("#product_list").removeClass('view-grid').removeClass('view-list').addClass( $(this).attr('rel') );
			$.cookie( 'listing_products_mode', $(this).attr('rel') );
			return false;
		} ); 
	} );
	 
	 
	
	//userinfo	 
	 $("#header_user").each( function(){
		var content = $(".groupe-content");
		$(".groupe-btn", this ).click( function(){
			content.toggleClass("show");
		}) ;
	} );
	 
	//search 
		$("#search_block_top").each( function(){
		var content = $(".groupe");
		$(".groupe-btn", this ).click( function(){
				content.toggleClass("show");
			}) ;
		} );
	 
	// scroll top
	$('#nav_up').click(function () {
	   $('body,html').animate({
	    scrollTop: 0
	   }, 800);
	   return false;
	 });

	// canvas menu 	
	$(document.body).on('click', '[data-toggle="dropdown"]' ,function(){
	  if(!$(this).parent().hasClass('open') && this.href && this.href != '#'){
	   window.location.href = this.href;
	  }

	 });
 
	//tooltip
	$('.btn-tooltip').tooltip('show');
	$('.btn-tooltip').tooltip('hide');
	
	// gototop
	   // hide #back-top first
		 $("#back-top").hide(); 
		 // fade in #back-top
		 $(function () {
			  $(window).scroll(function () {
			   if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			   } else {
				$('#back-top').fadeOut();
			   }
			  });
			  // scroll body to 0px on click
			  $('#back-top a').click(function () {
			   $('body,html').animate({
				scrollTop: 0
			   }, 800);
			   return false;
		  });
		 });	
} );
$(window).ready( function(){
    /* automatic keep header always displaying on top */
    /* automatic keep header always displaying on top */
    if( $("body").hasClass("keep-header") ){
        $("#header").addClass( "navbar-fixed-top" );
        var hideheight =  $("#header").height()+120;
        $("#page").css( "padding-top", $("#header").height() );
        $(window).scroll(function() {
            var pos = $(window).scrollTop();
            if( pos >= hideheight ){
                $(".hide-bar").css( "margin-top", - $("#topbar").height() );
            }else {
                $("#topbar").removeClass('hide-bar');
                $("#topbar").css( "margin-top", 0 );
            }

        });
    }

} );
// wishlihst
function LeoWishlistCart(id, action, id_product, id_product_attribute, quantity)
{ 
	$.ajax({
		type: 'GET',
		url:	baseDir + 'modules/blockwishlist/cart.php',
		async: true,
		cache: false,
		data: 'action=' + action + '&id_product=' + id_product + '&quantity=' + quantity + '&token=' + static_token + '&id_product_attribute=' + id_product_attribute,
		success: function(data)
		{ 
			if (action == 'add') {
				
				if( $("#wishlistwraning").length <= 0 ) {
				   var html = '';
				   html +=  '<div id="wishlistwraning"><div class="container">';
				   html +=  ' ';
				   html +=  '</div></div>';
				   $("body").append( html );	
				} 
				$("#wishlistwraning .container").html( ' <div class="alert-content"> <div class="alert"><button type="button" class="close" data-dismiss="alert">&times;</button>' + data + '</div></div>' );		
				if( $("#wishlistwraning .cart_block_product_name").length > 0 ) {
					$("#wishlistwraning").html('<div class="container"><div class="alert">Done</div></div>').show().delay(1000).fadeOut(300);
				}else {
					$("#wishlistwraning").show().delay(3000).fadeOut(600);
				}
				
   			}
		
			if($('#' + id).length != 0)
			{ 
				$('#' + id).slideUp('normal');
				document.getElementById(id).innerHTML = data;
				$('#' + id).slideDown('normal');
				
			}
		}
	});
}


<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri().'/favicon.ico'; ?>" /> 
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
    <script type="text/javascript">/* <![CDATA[ */var baseDir='https://www.audioactive.es/';var baseUri='https://www.audioactive.es/';var static_token='60e35cbf3de167a4cd740b1097a49c80';var token='fe5e860ba45e7cdd2b98b1a7ae934504';var priceDisplayPrecision=2;var priceDisplayMethod=0;var roundMode=2;/* ]]> */</script>
    <link rel="stylesheet" type="text/css" href="https://www.audioactive.es/themes/leodig/css/theme-responsive.css">
    <link rel="stylesheet" type="text/css" href="https://www.audioactive.es/themes/leodig/css/font-awesome.min.css">
    <script src="https://www.audioactive.es/themes/leodig/js/custom.js" type="text/javascript">/* <![CDATA[ *//* ]]> */</script>
    <script src="https://www.audioactive.es/themes/leodig/js/jquery.cookie.js" type="text/javascript">/* <![CDATA[ *//* ]]> */</script>
    <script src="https://www.audioactive.es/themes/leodig/js/jquery.form.min.js" type="text/javascript">/* <![CDATA[ *//* ]]> */</script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/jquery/plugins/autocomplete/jquery.autocomplete.js">/* <![CDATA[ *//* ]]> */</script>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic|Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://www.audioactive.es/themes/leodig/css/nuevo.css" rel="stylesheet" type="text/css">
    <link href="https://www.audioactive.es/themes/leodig/css/nuevorwd.css" rel="stylesheet" type="text/css">

    <script>/* <![CDATA[ */(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create','UA-55324087-1','auto');ga('send','pageview');/* ]]> */</script>

    <script>/* <![CDATA[ */!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init','1491299767842621');fbq('track',"PageView");/* ]]> */</script>

    <noscript>&lt;img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1491299767842621&amp;ev=PageView&amp;noscript=1"/&gt;</noscript>
    <style type="text/css">.fancybox-margin{margin-right:15px;}</style>

    <script type="text/javascript">
        var $ = jQuery.noConflict();
    </script>
    <script type="text/javascript">
       jQuery("#header_user").each( function(){
            var content = $(".groupe-content");
            jQuery(".groupe-btn", this ).click( function(){
                content.toggleClass("show");
            }) ;
        } );
    </script>
</head>

<body <?php body_class(); ?>>
    <section class="clearfix" id="page">
    <header class="navbar-fixed-top clearfix" id="header"> <section id="topbar"><div class="container"><ul class="links hidden-xs hidden-sm pull-left" id="header_user_info"></ul><ul class="links pull-right" id="header_nav_cart"><li class="ico_cart_amount" style="display: none;">0</li><li id="shopping_cart"> <a href="https://www.audioactive.es/carrito" rel="nofollow" title="Ver mi carrito de compra"> <em class="icon-quitar-carrito"></em> <span class="ajax_cart_quantity hidden">0</span> <span class="ajax_cart_product_txt hidden">Artículo</span> <span class="ajax_cart_product_txt_s hidden">Artículos</span> <span class="ajax_cart_total hidden"> </span> <span class="ajax_cart_no_product"> 0 Artículo(s) -
0,00 € </span> </a></li></ul><div class="pull-right" id="header_user"> <a data-toggle="dropdown" class="groupe-btn dropdown hidden-sm hidden-md hidden-lg" title="Enlace rápido" href="#"><span class="fa fa-user"></span></a><ul class="links groupe-content" id="header_nav"><li><a class="login" href="https://www.audioactive.es/mi-cuenta" rel="nofollow" title="Conectarse a su cuenta de cliente"><span class="fa fa-user"></span>Entrar / Registro</a></li></ul></div><script type="text/javascript">/* <![CDATA[ */var CUSTOMIZE_TEXTFIELD=1;var img_dir='https://www.audioactive.es/themes/leodig/img/';var customizationIdMessage='Personalización n°';var removingLinkText='eliminar este producto de mi carrito';var freeShippingTranslation='Envío gratuito!';var freeProductTranslation='¡Gratis!';var delete_txt='Eliminar';var generated_date=1467035178;/* ]]> */</script><div class="block exclusive" id="cart_block"><p class="title_block"> <a href="https://www.audioactive.es/carrito" rel="nofollow" title="Ver mi carrito de compra">carrito <span class="hidden" id="block_cart_expand">&nbsp;</span> <span id="block_cart_collapse">&nbsp;</span> </a></p><div class="block_content"><div class="collapsed" id="cart_block_summary"> <span class="ajax_cart_quantity" style="display:none;">0</span> <span class="ajax_cart_product_txt_s" style="display:none">productos</span> <span class="ajax_cart_product_txt">producto</span> <span class="ajax_cart_total" style="display:none"> </span> <span class="ajax_cart_no_product">vacío</span></div><div class="expanded" id="cart_block_list"><p class="cart_block_no_products" id="cart_block_no_products">No hay productos</p><table id="vouchers" style="display:none;"><tbody><tr class="bloc_cart_voucher"><td>&nbsp;</td></tr></tbody></table><p id="cart-prices"> <span class="price ajax_cart_shipping_cost" id="cart_block_shipping_cost">0,00 €</span> <span>Transporte</span> <br> <span class="price ajax_block_cart_total" id="cart_block_total">0,00 €</span> <span>Total</span></p><p id="cart-buttons"> <a class="button exclusive" href="https://www.audioactive.es/carrito" id="button_order_cart" rel="nofollow" title="Confirmar"><span class="fa fa-check"></span>Confirmar</a></p></div></div></div><div class="col-lg-2 col-xs-5 col-sm-3 col-md-2 pull-right" id="search_block_top"> <a class="groupe-btn dropdown hidden-md hidden-lg" href="#" title="Search"><span class="fa fa-search"></span></a><form action="https://www.audioactive.es/buscar" class="row groupe" id="searchbox" method="get"> <input type="hidden" name="controller" value="search"> <input type="hidden" name="orderby" value="position"> <input type="hidden" name="orderway" value="desc"> <input class="search_query form-control ac_input" type="text" id="search_query_top" name="search_query" value="" placeholder="Accesorios para audífonos, teléfonos, tratamientos..." autocomplete="off"> <input type="submit" name="submit_search" value="Accesorios para audífonos, teléfonos, tratamientos..." class="button"></form></div><script type="text/javascript">/* <![CDATA[ */jQuery(document).ready(function($){$("#search_query_top").autocomplete('https://www.audioactive.es/buscar',{minChars:3,max:10,width:500,selectFirst:false,scroll:false,dataType:"json",formatItem:function(data,i,max,value,term){return value;},parse:function(data){var mytab=new Array();for(var i=0;i<data.length;i++)
mytab[mytab.length]={data:data[i],value:data[i].cname+' > '+data[i].pname};return mytab;},extraParams:{ajaxSearch:1,id_lang:1}}).result(function(event,data,formatted){$('#search_query_top').val(data.pname);document.location.href=data.product_link;})});/* ]]> */</script></div> </section> <section id="header-main"><div class="container"><div class="header-wrap"><div class="pull-left"> <a href="https://www.audioactive.es/" id="header_logo" title="audioactive"> <img class="logo" src="https://www.audioactive.es/img/logo.jpg?1449308701" alt="audioactive"> </a></div><div class="pull-right hiderwd"> <hgroup><h2>Centro de Atención al Cliente <span><a href="tel:+966115079">966 115 079</a></span></h2><h3 class="cac-time">Lu. a Vi. de 10:00 a 13:30 y de 16:30 a 19:00</h3> </hgroup></div></div></div> <nav class="clearfix" id="topnavigation"><div class="container"><div class="row"><div class="navbar"> <nav class="navbar-default  pull-left" id="cavas_menu" role="navigation"><div class="navbar-header"> <button class="navbar-toggle" type="button"> <span class="sr-only">Navegación Toggle</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button></div><div class="collapse navbar-collapse navbar-ex1-collapse divtogglemenu" id="leo-top-menu" style="max-height:none;overflow:hidden;"><div class="pull-right showrwd" id="header_user"><ul><li><a class="login" href="https://www.audioactive.es/mi-cuenta" rel="nofollow" title="Login to your customer account"><span class="fa fa-user"></span> ENTRAR / REGISTRO</a></li></ul></div><ul class="nav megamenu"><li> <a href="https://www.audioactive.es/soluciones-auditivas-_6" target="_self"><span class="menu-title">Soluciones Auditivas</span></a></li><li> <a href="https://www.audioactive.es/auriculares-tv_8" target="_self"><span class="menu-title">Auriculares TV</span></a></li><li> <a href="https://www.audioactive.es/telefonos-mayores_7" target="_self"><span class="menu-title">Teléfonos</span></a></li><li> <a href="https://www.audioactive.es/despertador-sordos_9" target="_self"><span class="menu-title">Despertadores</span></a></li><li> <a href="https://www.audioactive.es/limpieza-audifonos_12" target="_self"><span class="menu-title">Limpieza Audífonos</span></a></li><li> <a href="https://www.audioactive.es/pilas-audifonos_11" target="_self"><span class="menu-title">Pilas Audífonos</span></a></li></ul> <hgroup class="showrwd"><h2>Centro de Atención al Cliente <span><a href="tel:+966115079" style="font-size:30px;">966 115 079</a></span></h2><h3 class="cac-time">Lu. a Vi. de 10:00 a 13:30 y de 16:30 a 19:00</h3> </hgroup><div class="white_background"></div></div> </nav></div><script>/* <![CDATA[ */$(document).ready(function()
{$("button.navbar-toggle").click(function(e)
{e.preventDefault();$(".divtogglemenu").slideToggle("fast");});});/* ]]> */</script></div></div> </nav> </section> </header>


    <script>
    jQuery(document).ready(function() {
        jQuery('.carousel').each(function(){
            $(this).carousel({
                pause: true,
                interval: false
            });
        });
        jQuery(".blockleoproducttabs").each( function(){
            $(".nav-tabs li", this).first().addClass("active");
            $(".tab-content .tab-pane", this).first().addClass("active");
        } );
        
        jQuery('#myModal') 
        jQuery('show');
        jQuery('#myModal').on('hide.bs.modal', function (e) {
          //console.log("capturado");
          url = "index.php";
          $(location).attr('href',url);
        })
        
    });
    </script>



<div id="page" class="site">
    <div class="site-inner">
    	<header id="masthead" class="site-header" role="banner">
    		
    		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentysixteen' ); ?></a>

    			<div class="site-header-main" >
    				<div class="site-branding">
    					<?php twentysixteen_the_custom_logo(); ?>

    					<?php if ( is_front_page() && is_home() ) : ?>
    						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
    					<?php else : ?>
    						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
    					<?php endif;

    					$description = get_bloginfo( 'description', 'display' );
    					if ( $description || is_customize_preview() ) : ?>
    						<p class="site-description"><?php echo $description; ?></p>
    					<?php endif; ?>
    				</div><!-- .site-branding -->

    				<?php if ( has_nav_menu( 'primary' ) || has_nav_menu( 'social' ) ) : ?>
    					<button id="menu-toggle" class="menu-toggle"><?php _e( 'Menu', 'twentysixteen' ); ?></button>

    					<div id="site-header-menu" class="site-header-menu">
    						<?php if ( has_nav_menu( 'primary' ) ) : ?>
    							<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
    								<?php
    									wp_nav_menu( array(
    										'theme_location' => 'primary',
    										'menu_class'     => 'primary-menu',
    									 ) );
    								?>
    							</nav><!-- .main-navigation -->
    						<?php endif; ?>

    						<?php if ( has_nav_menu( 'social' ) ) : ?>
    							<nav id="social-navigation" class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'twentysixteen' ); ?>">
    								<?php
    									wp_nav_menu( array(
    										'theme_location' => 'social',
    										'menu_class'     => 'social-links-menu',
    										'depth'          => 1,
    										'link_before'    => '<span class="screen-reader-text">',
    										'link_after'     => '</span>',
    									) );
    								?>
    							</nav><!-- .social-navigation -->
    						<?php endif; ?>
    					</div><!-- .site-header-menu -->
    				<?php endif; ?>
    			</div><!-- .site-header-main -->

    			<?php if ( get_header_image() ) : ?>
    				<?php
    					/**
    					 * Filter the default twentysixteen custom header sizes attribute.
    					 *
    					 * @since Twenty Sixteen 1.0
    					 *
    					 * @param string $custom_header_sizes sizes attribute
    					 * for Custom Header. Default '(max-width: 709px) 85vw,
    					 * (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px'.
    					 */
    					$custom_header_sizes = apply_filters( 'twentysixteen_custom_header_sizes', '(max-width: 709px) 85vw, (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px' );
    				?>
    				<div class="header-image">
    					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
    						<img src="<?php header_image(); ?>" srcset="<?php echo esc_attr( wp_get_attachment_image_srcset( get_custom_header()->attachment_id ) ); ?>" sizes="<?php echo esc_attr( $custom_header_sizes ); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
    					</a>
    				</div><!-- .header-image -->
    			<?php endif; // End header image check. ?>
    		</header><!-- .site-header -->

    		<div id="content" class="site-content container">

            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                <?php if(function_exists('bcn_display'))
                {
                    bcn_display();
                }?>
            </div>

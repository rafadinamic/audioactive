$(document).ready(function () {
    $('.question_image').each(function () {
        $(this).qtip({
            content: $(this).parent().children('.tooltip_val')[0].innerHTML,
            show: 'mouseover',
            hide: 'mouseout',
            position: {
                corner: {
                    target: 'topLeft',
                    tooltip: 'bottomRight'
                }
            }
        });
    });

    $.ajax({
        type: 'POST',
        url: baseDir + 'index.php?fc=module&module=featurecategories&controller=compare',
        async: true,
        cache: false,
        data: 'action=show',
        success: function (jsonData, textStatus, jqXHR) {
            if (jsonData == 'hide') {
                $('#fc_comparison').css('display', 'none');
            }
            else {
                $('#fc_comparison').empty().html(jsonData);
                $('#fc_comparison').css('display', 'block');
            }
        }
    });

    $('.fc_btn_compare').click(function (e) {
        e.preventDefault();
        var productId = $(this).attr('name');
        $.ajax({
            type: 'POST',
            url: baseDir + 'index.php?fc=module&module=featurecategories&controller=compare',
            async: true,
            cache: false,
            data: 'action=add&productId=' + productId,
            success: function (jsonData, textStatus, jqXHR) {
                if (jsonData == 'hide') {
                    $('#fc_comparison').css('display', 'none');
                }
                else {
                    $('#fc_comparison').empty().html(jsonData);
                    $('#fc_comparison').css('display', 'block');
                }
                $('html, body').animate({
                    scrollTop: $("#fc_comparison").offset().top - 100}, 1000);
            }
        });
        return false;
    });

});

function deleteItem(data) {
    var productId = data.attr('href').split('&productId=')[1];
    $.ajax({
        type: 'POST',
        url: baseDir + 'index.php?fc=module&module=featurecategories&controller=compare',
        async: true,
        cache: false,
        data: 'action=delete&productId=' + productId,
        success: function (jsonData, textStatus, jqXHR) {
            if (jsonData == 'hide') {
                $('#fc_comparison').css('display', 'none');
            }
            else {
                $('#fc_comparison').empty().html(jsonData);
                $('#fc_comparison').css('display', 'block');
            }
        }
    });
}

function deleteAllItems() {
    $.ajax({
        type: 'POST',
        url: baseDir + 'index.php?fc=module&module=featurecategories&controller=compare',
        async: true,
        cache: false,
        data: 'action=deleteall',
        success: function (jsonData, textStatus, jqXHR) {
            $('#fc_comparison').css('display', 'none');
        }
    });
}
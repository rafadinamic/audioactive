<?php

if (!defined('_PS_VERSION_'))
    exit;

class FeatureCategories extends Module {

    private $html = '';

    public function __construct() {
        $this->name = 'featurecategories';
        $this->tab = 'smart_shopping';
        $this->version = 1.5;
        $this->author = 'Ssoft';
        $this->module_key = 'a2b7cee1897e09a7783e7d1fa5738873';
        $this->need_instance = 0;
        parent::__construct();
        $this->displayName = $this->l('Group Features and Compare Products');
        $this->description = $this->l('Enables you to create groups for your product features and creates new products comparison page');
    }

    public function install() {
        if (parent::install() == false OR !$this->registerHook('productTab') OR !$this->registerHook('productTabContent')
                OR !$this->registerHook('leftColumn') OR !$this->registerHook('extraLeft') OR !$this->registerHook('header'))
            return false;
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . '_fc_categories ( `category_id` int NOT NULL AUTO_INCREMENT, `name` varchar(250) UNIQUE, `priority` int NOT NULL DEFAULT 1, `description` varchar(512) NULL, PRIMARY KEY (`category_id`))	default CHARSET=utf8');
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . '_fc_categories_features ( `category_id` int not null, `feature_id` int not null)	default CHARSET=utf8');
        Configuration::updateValue('FEATURECATEGORIES_DISP_MODE', 'separate');
        return true;
    }

    public function uninstall() {
        if (parent::uninstall() == false)
            return false;
        return true;
    }

    public function hookProductTab($params) {
        $product_id = intval($_GET['id_product']);

        $product = new Product($product_id);
        if (count($product->getFeatures()) > 0)
        {
            $this->productosComparativa = Db::getInstance()->getRow('SELECT products FROM '. _DB_PREFIX_ .'_fc_categories where products like \'%"'. $product_id .'"%\'');
            if($this->productosComparativa)
                return '<li><a href="#idTabFeatures" id="more_info_tab_features_by_category">' . $this->l('Comparativa') . '</a></li>';
        }
    }

    public function hookProductTabContent($params) {
        /** cambio */
        //Estamos obteniendo todos los registros de la base de datos en donde esté el producto que le pasamos


        //Si no existe el id en el grupo salimos para no dibujar la tabla
        if(empty($this->productosComparativa))
            return;

      //llamamos a la funcion displayComparison para que dibuje la tabla comparativa
       $txt = '<div id="idTabFeatures">';
       $txt .= $this->displayComparison($this->productosComparativa['products']) .'</div>';
       return $txt;
    /** cambio */
    }

    /*public function hookExtraLeft($params) {
        return "<li class='fc_btn_compare'><a href=\"#\" class='fc_btn_compare' name=" . $_GET['id_product'] . " >" . $this->l('Add To Compare') . "</a></li>";
    }

    public function hookExtraRight($params) {
        return "<a href=\"#\" class='fc_btn_compare button' name=" . $_GET['id_product'] . " >" . $this->l('Add To Compare') . "</a>";
    }*/

    public function hookLeftColumn($params) {
        $html = '<div id="fc_comparison" class="block" style="display:none;"></div>';
        return $html;
    }

    public function hookRightColumn($params) {
        return $this->hookLeftColumn($params);
    }

    public function hookHeader($params) {
        $this->context->controller->addCSS($this->_path . 'views/css/style.css', 'all');
        $this->context->controller->addJS($this->_path . 'views/js/compare.js');
        $this->context->controller->addJS($this->_path . 'views/js/jquery.qtip-1.0.0-rc3.min.js');
    }

    public function getContent() {
        $this->html = '<h2>' . $this->displayName . '</h2>';
        $this->html .= '<link media="all" type="text/css" rel="stylesheet" href="' . $this->_path . 'views/css/style.css"/>';
        if (Tools::getValue('saveCategory') != NULL || Tools::getValue('editCategory') != NULL) {
            $this->html .= '<p style="color:red">' . $this->postProcess() . '</p>';
            $_GET['fc_tab'] = 'category'; //redirect to overview after post action
        } else if (Tools::getValue('submitSettings') != NULL) {
            $this->html .= '<p style="color:red">' . $this->postProcess() . '</p>';
            $this->html .= $this->displayConfirmation($this->l('Settings updated'));
        }
        $url = $_SERVER['REQUEST_URI'];
        if (strpos($url, '&fc_tab') > 0)
            $url = substr($url, 0, strpos($url, '&fc_tab'));

        $this->html .= '<ul><li ' . (Tools::getValue('fc_tab') == "category" || Tools::getValue('fc_tab') == NULL ? 'class="fc_admin_tab_active"' : 'class="fc_admin_tab"') . '><a style="padding:8px 8px 4px 4px;" href="' . $url . '&fc_tab=category" id = "fc_categories">' . $this->l('Groups') . '</a></li>
		<li ' . (Tools::getValue('fc_tab') == "create_category" ? 'class="fc_admin_tab_active"' : 'class="fc_admin_tab"') . '><a style="padding:8px 8px 4px 4px;" href="' . $url . '&fc_tab=create_category" id = "fc_create_category">' . $this->l('Create Group') . '</a></li>' .
                '<li ' . (Tools::getValue('fc_tab') == "settings" ? 'class="fc_admin_tab_active"' : 'class="fc_admin_tab"') . '><a style="padding:8px 8px 4px 4px;" href="' . $url . '&fc_tab=settings" id = "fc_settings">' . $this->l('Settings') . '</a></li></ul>';
        if (Tools::getValue('fc_tab') == 'create_category')
            $this->html.= $this->displayCreateCategory();
        else if (Tools::getValue('fc_tab') == 'edit_category') {
            $categoryId = Tools::getValue('id');
            $this->html.= $this->displayEditCategory($categoryId, $url);
        } else if (Tools::getValue('fc_tab') == 'delete_category') {
            $categoryId = Tools::getValue('id');
            $this->html.= $this->deleteCategory($categoryId, $url);
        } elseif (Tools::getValue('fc_tab') == 'settings') {
            $this->html .= $this->displaySettings();
        }
        else
            $this->html.= $this->displayCategories($url);

		$this->html .= '<div class="store"><!--See more modules by Ssoft at <a href="http://ssoft-dev.com">ssoft-dev.com</a>--></div>';
        return $this->html;
    }

    private function displayCategories($url) {
        /** cambio */
        //$product_id = intval($_GET['id_product']);
        //$categories = Db::getInstance()->Execute($sql = 'SELECT * FROM ' . _DB_PREFIX_ . '_fc_categories where products like \'%"'. $product_id .'"%\'');
        $categories = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . '_fc_categories');

        $txt = '<fieldset style="margin-top:80px;"><legend>' . $this->l('Groups Overview') . '</legend>';
        if (count($categories) > 0) {
            $txt .='<table border="1" class="table" style="width:100%; border-collapse:collapse;"><tr><th>' . $this->l('Group Name') .
                    '</th><th>' . $this->l('Features') . '</th><th>' . $this->l('Products') . '</th><th>' . $this->l('Priority') . '</th><th>' . $this->l('Actions') . '</th></tr>';
            foreach ($categories as $cat) {
                $features = Db::getInstance()->ExecuteS('SELECT name FROM ' . _DB_PREFIX_ . 'feature_lang WHERE id_lang = 1 AND id_feature IN
				(SELECT feature_id FROM ' . _DB_PREFIX_ . '_fc_categories_features WHERE category_id = ' . $cat['category_id'] . ')');
                $txt .= '<tr><td>' . $cat['name'] . '</td><td>';//pinta el nombre de la categoria
                foreach ($features as $feature) { //pinta las categorias
                    $txt .= $feature['name'] . '<br/>';
                }

//añadido
                $idProducts = (json_decode($cat['products'])); //pinta los nombres de los productos.
                if(count($idProducts) > 0){
                    $txt .= '</td><td>';
                    foreach($idProducts as $product){
                        $prd = new Product($product);
                        $txt .= $prd->getProductName($product) . '<br/>';
                    }
                }else{
                    $txt .='</td><td>';
                }
//añadido

                $txt .= '</td><td>' . $cat['priority'] . '</td><td><a style="text-decoration:underline; color:#0000FF;" href="' .
                        $url . '&fc_tab=edit_category&id=' . $cat['category_id'] . '">' . $this->l('Edit') . '</a> |
				<a onclick="return confirm(\'' . $this->l('Are you shure?') . '\') ? true : false;" style="text-decoration:underline; color:#0000FF;" href="' . $url . '&fc_tab=delete_category&id=' . $cat['category_id'] . '">' . $this->l('Delete') . '</a></td></tr>';
            }
            $txt .= '</table>';
        } else {
            $txt .='<p>' . $this->l('You have no groups') . '</p>';
        }
        $txt .= '</fieldset>';
        return $txt;
        /***/
    }

    private function displayCreateCategory() {
        /** cambio **/
        $features = $this->getUnassignedFeatures();
        $txt = '<fieldset style="margin-top:80px;"><legend>' . $this->l('Create Group') . '</legend>';
        $txt .= '<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">';
        $txt .= '<label>' . $this->l('Group Name') . '</label>
                <div class="margin-form"><input type="text" name="name" value="' . (Tools::getValue('name', '')) . '" style="width:60%;"/></div>';
        $txt .= '<label>' . $this->l('Features') . '</label><div class="margin-form"><select name="features[]" id="features" size="15" style="width:200px" multiple="multiple">';
        foreach ($features as $feature) {
            $txt .= '<option value="' . $feature['id_feature'] . '">' . $feature['name'] . '</option>';
        }
        $txt .= '</select><p class="clear">' . $this->l('Ctrl+click to select features for the group') . '</p></div>';
//añadido
        $txt .= '<label>' . $this->l('Products') . '</label><div class="margin-form"><select name="products[]" id="products" size="15" style="width:200px" multiple="multiple">';
        $products = Db::getInstance()->ExecuteS('SELECT id_product, name FROM ' . _DB_PREFIX_ . 'product_lang WHERE id_lang = 1');
        foreach($products as $product)
        {
            $txt.= '<option value="'. $product['id_product'] .'">' . $product['name']. '</option>';
        }
        $txt .= '</select><p class="clear">' . $this->l('Ctrl+click to select features for the group') . '</p></div>';
//añadido
        $txt .= '<label>' . $this->l('Priority') . '</label><div class="margin-form"><select name="priority" id="priority" style="width:200px" >';
        foreach (range(1, 50) as $val) {
            $txt .= '<option value="' . $val . '">' . $val . '</option>';
        }
        $txt .= '</select><p class="clear">' . $this->l('Defines the order of groups') . '</p></div>';
        $txt .= '<label>' . $this->l('Group Description') . '</label>
            <div class="margin-form"><textarea rows="5" name="description" style="width:60%;"></textarea>
			<p class="clear">' . $this->l('500 characters maximum') . '</p></div>';
        $txt .= '<input type="submit" name="saveCategory" value="' . $this->l('Save') . '" class="button" />';
        $txt .= '</form></fieldset>';
        /***/
        return $txt;
    }

    private function displayEditCategory($id, $url) {
        /** Cambio  */
        $category = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . '_fc_categories WHERE category_id =' . $id);
        $category = $category[0];
        $feature_ids_raw = Db::getInstance()->ExecuteS('SELECT feature_id FROM ' . _DB_PREFIX_ . '_fc_categories_features WHERE category_id =' . $id);
        $displayFeatures = $this->getUnassignedAndCategoryFeatures($id);
        $feature_ids = array();
        foreach ($feature_ids_raw as $f_id_raw) {
            $feature_ids[] = $f_id_raw['feature_id'];
        }
//añadido para que aparezcan seleccionados en el selector los productos seleccionados cuando se creo el grupo
          $product_ids_raw = Db::getInstance()->ExecuteS($sql = 'SELECT products FROM ' . _DB_PREFIX_ . '_fc_categories WHERE category_id =' .$id);
          $product_ids = array();
          $product_ids = json_decode($product_ids_raw[0]['products']);
//añadido
        $txt = '<fieldset style="margin-top:80px;"><legend>' . $this->l('Edit Group') . '</legend>';
        $txt .= '<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">';
        $txt .= '<label>' . $this->l('Group Name') . '</label>
                <div class="margin-form"><input type="text" name="name" value="' . $category['name'] . '" style="width:60%;"/></div>';
      //Features
        $txt .= '<label>' . $this->l('Features') . '</label><div class="margin-form"><select name="features[]" id="features" size="15" style="width:200px" multiple="multiple">';
        foreach ($displayFeatures as $feature) {
            $txt .= '<option value="' . $feature['id_feature'] . '"' . (in_array($feature['id_feature'], $feature_ids) ? 'selected="selected"' : '') . '>' . $feature['name'] . '</option>';
        }
        $txt .= '</select><p class="clear">' . $this->l('Ctrl+click to select features for the group') .
                '</p><button onclick="$(\'#features option:selected\').removeAttr(\'selected\');return false;">Clear Attributes</button></div>';
       //Features
//añadido
       //Productos
        $txt .= '<label>' . $this->l('Products') . '</label><div class="margin-form"><select name="products[]" id="products" size="15" style="width:200px" multiple="multiple">';
        $products = Db::getInstance()->ExecuteS('SELECT id_product, name FROM ' . _DB_PREFIX_ . 'product_lang WHERE id_lang = 1');
        foreach($products as $product)
        {
            //$txt.= '<option value="'. $product['id_product'] .'">' . $product['name']. '</option>';
            $txt .= '<option value="' . $product['id_product'] . '"' . (in_array($product['id_product'], $product_ids) ? 'selected="selected"' : '') . '>' . $product['name'] . '</option>';
        }
        $txt .= '</select><p class="clear">' . $this->l('Ctrl+click to select features for the group') . '</p></div>';
        //Features
//añadido

        $txt .= '<label>' . $this->l('Priority') . '</label><div class="margin-form"><select name="priority" id="priority" style="width:200px" >';
        foreach (range(1, 50) as $val) {
            $txt .= '<option value="' . $val . '" ' . ($category['priority'] == $val ? 'selected="selected"' : '') . '>' . $val . '</option>';
        }
        $txt .= '</select><p class="clear">' . $this->l('Defines the order of groups') . '</p></div>';
        $txt .= '<label>' . $this->l('Group Description') . '</label>
            <div class="margin-form"><textarea rows="5" name="description" style="width:60%;">' . $category['description'] . '</textarea>';
        $txt .= '<p class="clear">' . $this->l('500 characters maximum') . '</p></div>';
        $txt .= '<input type="hidden" name="id" value="' . $id . '"/>';
        $txt .= '<input type="submit" name="editCategory" value="' . $this->l('Save') . '" class="button" />';
        $txt .= '</form></fieldset>';
        return $txt;
        /***/
    }

    private function displaySettings() {
        $txt = '<fieldset style="margin-top:80px;"><legend>' . $this->l('Settings') . '</legend>
        <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
	<label>' . $this->l('Display Comparison Table') . '</label>
	<div class="margin-form">
	<input type="radio" name="display_mode" id="separate" value="separate" ' . (Tools::getValue('display_mode', Configuration::get('FEATURECATEGORIES_DISP_MODE')) == 'separate' ? 'checked="checked" ' : '') . '/>
	<label class="t" for="separate">' . $this->l('New Tab') . '</label><br/>
	<input type="radio" name="display_mode" id="inside" value="inside" ' . (Tools::getValue('display_mode', Configuration::get('FEATURECATEGORIES_DISP_MODE')) == 'inside' ? 'checked="checked" ' : '') . '/>
	<label class="t" for="inside"> ' . $this->l('Central Column of the Site') . '</label>
	</div><input type="submit" name="submitSettings" value="' . $this->l('Save') . '" class="button" /></form></fieldset>';
        return $txt;
    }

    private function deleteCategory($id, $url) {
        $id = intval($id);
        Db::getInstance()->Execute('DELETE FROM ' . _DB_PREFIX_ . '_fc_categories WHERE category_id = ' . $id);
        Db::getInstance()->Execute('DELETE FROM ' . _DB_PREFIX_ . '_fc_categories_features WHERE category_id = ' . $id);
        return $this->displayCategories($url);
    }

    private function getUnassignedFeatures() {
        /**cambio*/
        /**
         * Select modificada para que vuelvan a salir todas las categorias y poder crear nuevo grupos
         * con las mismas categorias ya assignadas anteriormente.
        */
        $result = Db::getInstance()->ExecuteS('SELECT id_feature, name FROM ' . _DB_PREFIX_ . 'feature_lang WHERE id_lang = ' . $this->context->language->id);
        return $result;
        /**cambio*/
    }

    private function getUnassignedAndCategoryFeatures($categoryId) {
        $result = Db::getInstance()->ExecuteS('SELECT id_feature, name FROM ' . _DB_PREFIX_ . 'feature_lang WHERE id_lang = ' . $this->context->language->id . ' AND id_feature NOT IN
(SELECT feature_id FROM ' . _DB_PREFIX_ . '_fc_categories_features WHERE category_id != ' . $categoryId . ')');
        return $result;
    }

    private function postProcess() {
        /** cambio */

//añadido
        //Procedemos a codificar los valores con json para guardar en la base de datos como un string en el campo products.
        $valores_productos = Tools::getValue('products');
        if(is_array($valores_productos)){
            $valores_productos = json_encode($valores_productos);
        }
        else{
            $valores_productos = '';
        }
//añadido

        if (Tools::getValue('saveCategory') != NULL) { //save new category
            //save category in fc_categories
            if (!Db::getInstance()->Execute('INSERT INTO ' . _DB_PREFIX_ . '_fc_categories (name, priority, description, products) VALUES ("' . Tools::getValue('name') . '", ' . Tools::getValue('priority') . ', "' . Tools::getValue('description') . '" , \'' . $valores_productos . '\')'))
                return $this->l('Cannot create group. Group with this name already exists.');
            $new_id = Db::getInstance()->Insert_ID();
            //save category-feature relations in fc_categories_features
            if (Tools::getValue('features') != NULL) {
                $query = 'INSERT INTO ' . _DB_PREFIX_ . '_fc_categories_features (category_id, feature_id) VALUES ';
                foreach (Tools::getValue('features') as $feature_id)
                    $query .= '(' . $new_id . ', ' . $feature_id . '),';
                $query = substr($query, 0, -1);
                Db::getInstance()->Execute($query);
            }
        } else if (Tools::getValue('editCategory') != NULL) { //edit category
            if (!Db::getInstance()->Execute( $sql = 'UPDATE ' . _DB_PREFIX_ . '_fc_categories SET name = "' . Tools::getValue('name') . '", priority = ' . Tools::getValue('priority') .
                            ', description = "' . Tools::getValue('description') . '", products = \''. $valores_productos .'\' WHERE category_id = ' . Tools::getValue('id')))
                return $this->l('Cannot update group. Group with this name already exists.');
            Db::getInstance()->Execute('DELETE FROM ' . _DB_PREFIX_ . '_fc_categories_features WHERE category_id = ' . Tools::getValue('id'));
            if (Tools::getValue('features') != NULL) {
                $query = 'INSERT INTO ' . _DB_PREFIX_ . '_fc_categories_features (category_id, feature_id) VALUES ';
                foreach (Tools::getValue('features') as $feature_id)
                    $query .= '(' . Tools::getValue('id') . ', ' . $feature_id . '),';
                $query = substr($query, 0, -1);
                Db::getInstance()->Execute($query);
            }
        } else if (Tools::getValue('submitSettings') != NULL) { //save settings
            Configuration::updateValue('FEATURECATEGORIES_DISP_MODE', Tools::getValue('display_mode'));
        }
        /** cambio */
    }

    /** cambio ** copiada del controlador compare.php de este mismo módulo */
    private function displayComparison($productIds)
    {
        //Decodificamos el parámetro pasado en la función para obtener el array de ids de productos.
        $productIds = json_decode($productIds);
        /** cambio */
        $features_by_categories = array();
        $distributed_features = array();
        $all_feature_ids = array();


        $query = 'SELECT DISTINCT cf.category_id, c.name FROM ' . _DB_PREFIX_ . '_fc_categories_features cf
		LEFT JOIN ' . _DB_PREFIX_ . '_fc_categories c ON (c.category_id = cf.category_id)
		WHERE cf.feature_id IN
		(SELECT id_feature FROM ' . _DB_PREFIX_ . 'feature_product WHERE 0';
        foreach ($productIds as $product_id) {
            if ($product_id != '')
                $query .= ' OR id_product=' . $product_id;
        }
        $query .= ') ORDER BY c.priority';

        $product_feature_categories = Db::getInstance()->ExecuteS($query);
        foreach ($product_feature_categories as $pfc) {
            $feature_ids = array();
            $raw_features = Db::getInstance()->ExecuteS('SELECT cf.feature_id FROM ' . _DB_PREFIX_ . '_fc_categories_features cf LEFT JOIN ' . _DB_PREFIX_ . 'feature f ON(f.id_feature = cf.feature_id) WHERE cf.category_id=' . $pfc['category_id'] . ' ORDER BY f.position');
            foreach ($raw_features as $fid) {
                $feature_ids[] = $fid['feature_id'];
                $distributed_features[] = $fid['feature_id'];
            }
            $features_by_categories[$pfc['name']] = $feature_ids;
        }

        //Incluimos el fichero Helper.php para que no se llame al Helper que no debe.
        require_once(dirname(__FILE__) . '/Helper.php');
        $helper = new Helper();

        $products = array();
        foreach ($productIds as $id) {
            if ($id != '') {
                $raw_all_ids = Db::getInstance()->ExecuteS('SELECT id_feature FROM ' . _DB_PREFIX_ . 'feature_product WHERE id_product = ' . $id);
                foreach ($raw_all_ids as $raw_id) {
                    if (!in_array($raw_id['id_feature'], $all_feature_ids))
                        $all_feature_ids[] = $raw_id['id_feature'];
                }
                $cover = Product::getCover((int)$id);
                $product = new Product($id, true, intval($this->context->cookie->id_lang));
                $product->id_image = Tools::htmlentitiesUTF8(Product::defineProductImage(array('id_image' => $cover['id_image'], 'id_product' => $id), $this->context->cookie->id_lang));
                $products[] = $product;
            }
        }
        $not_distributed_features = array_diff($all_feature_ids, $distributed_features);
        if (count($not_distributed_features) > 0)
            $features_by_categories['Otras'] = $not_distributed_features;
        $count = count($productIds);
        $this->context->smarty->assign(array(
            'features_by_categories' => $features_by_categories,
            'helper' => $helper,
            'products' => $products,
            'product_count' => $count
        ));

            return $this->context->smarty->fetch(dirname(__FILE__) . '/views/templates/front/comparison_embed_adora.tpl');
        /**cambio*/
    }
    /**/
}
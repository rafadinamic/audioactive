<script type="text/javascript">
$(document).ready( function(){	
	var currentLink = "";
	var nivoLink = "";
	$('#leo-coinslider').nivoSlider({
		effect:'<?php echo $this->getParams()->get('transition', 'random');?>',
		animSpeed:<?php echo (int)$this->getParams()->get('duration', 500);?>,
		pauseTime:<?php echo (int)$this->getParams()->get('interval', 3000);?>,
		slices:<?php echo (int) $this->getParams()->get('limit',6);?>,
		startSlide:<?php echo (int) $this->getParams()->get('dfshow',0);?>,
		//directionNavHide:<?php echo (int) $this->getParams()->get('direc_nav_hide',1);?>,
		//enableCaption :'<?php echo $this->getParams()->get('enable_caption',1);?>',		
		//captionOpacity:'<?php echo $this->getParams()->get('caption_opacity',0.8);?>',
		//captionBackground:'<?php echo $this->getParams()->get('caption_bg','#C01F25');?>',
		//group:'<?php echo $this->getParams()->get('module_group','product');?>',
		directionNav:<?php echo ($this->getParams()->get('navigation',1) ? 'true' : 'false');?>,
                controlNav:<?php echo ($this->getParams()->get('cnavigation',1) ? 'true' : 'false');?>,
		manualAdvance:<?php echo ($this->getParams()->get('autoAdvance',1) ? 'false' : 'true');?>,
                pauseOnHover:<?php echo ($this->getParams()->get('pausehover',1) ? 'true' : 'false');?>,
		boxCols: <?php echo (int)$this->getParams()->get('boxCols',8);?>, // For box animations
                boxRows: <?php echo (int)$this->getParams()->get('boxRows',4);?>, // For box animations
		afterChange:function(){
			nivoLink = $( this ).find(".nivo-caption .lof-title").find("a");
			if(nivoLink.attr("rel")){
				currentLink = nivoLink.attr("rel");
			}
		},
                afterLoad:function(){
                    <?php if($this->getParams()->get('navigationHover',1)){?>
                    $('.nivo-directionNav').hide();$('.nivoSlider').hover(function(){$('.nivo-directionNav').show();}, function(){$('.nivo-directionNav').hide();});
                    <?php }?>
                    
                    $(".lof-title").attr("class","lof-title");
                    $(".lof-title").addClass("stretchRight");

                    $(".lof-info").attr("class","lof-info");
                    $(".lof-info").addClass("stretchRight");

                    $(".lof-readmore").attr("class","lof-readmore");
                    $(".lof-readmore").addClass("stretchRight");
                }
	});
});
</script>
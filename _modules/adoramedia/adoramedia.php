<?php
/**
 * Project Audioactive.
 * User: David Moya de la Ossa <david.moya@adoramedia.com>
 * Date: 8/09/14
 * Time: 15:19
 */

/**
 * Class Adoramedia
 * @extends ModuleCore
 */
class Adoramedia extends Module
{
    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $this->name = 'adoramedia';
        $this->tab = 'front_office_features';
        $this->version = '1.1';
        $this->author = 'Adoramedia.com';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('Adoramedia');
        $this->description = $this->l('Agrega funcionalidad específica para Audioactive.');
        $this->secure_key = Tools::encrypt($this->name);
    }

    /**
     * @inheritdoc
     */
    public function install()
    {
        if( !parent::install()
            || !$this->registerHook('footer')
            || !$this->registerHook('actionCustomerAccountAdd'))
            return false;

        return true;
    }

    /**
     * @param array $params
     */
    public function hookActionCustomerAccountAdd($params)
    {
        // Agregamos cookie para que la próxima vez que se renderice el pie se pinte
        $this->context->cookie->customer_added = 1;
    }

    /**
     * @return string
     */
    public function hookFooter()
    {
        if($this->context->cookie->customer_added)
        {
            unset($this->context->cookie->customer_added);
            return $this->display(__FILE__, 'views/user_registered_conversion.tpl');
        }
    }
}